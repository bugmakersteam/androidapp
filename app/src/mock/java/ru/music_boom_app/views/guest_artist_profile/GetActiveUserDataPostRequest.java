package ru.music_boom_app.views.guest_artist_profile;

import okhttp3.RequestBody;
import retrofit2.Retrofit;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.base_requests.BasePostRequest;
import ru.music_boom_app.utils.request_observer.states.ProgressStateRequest;
import ru.music_boom_app.utils.request_observer.states.StateRequest;
import ru.music_boom_app.utils.request_observer.states.SuccessStateRequest;

/**
 * @author Markin Andrey on 17.04.2018.
 */
public class GetActiveUserDataPostRequest extends BasePostRequest {
    private BasePresenter mPresenter;
    private StateRequest mStateRequest;

    private static final String RESPONSE = "{\n" +
            "  \"status\": \"SUCCESS\",\n" +
            "  \"user\": {\n" +
            "    \"publicName\": \"Юрий Башмет\",\n" +
            "    \"creativity\": \"musician\",\n" +
            "    \"instrument\": \"Скрипка\",\n" +
            "    \"genre\": \"Классика\",\n" +
            "    \"isOrdered\": \"true\",\n" +
            "    \"phoneNumber\": \"+79111111111\",\n" +
            "    \"vk\": \"https://vk.com/startup_mb\",\n" +
            "    \"tlg\": \"@alias\",\n" +
            "    \"whatsApp\": \"+79111111111\",\n" +
            "    \"cityRating\": \"10,5\",\n" +
            "    \"countryRating\": \"10,5\",\n" +
            "    \"avatar\": \"https://vibirai.ru/image/825734.jpg\",\n" +
            "    \"photos\": [\n" +
            "      \"https://www.ridus.ru/images/2014/3/15/162027/in_article_4653dfdec7.jpg\",\n" +
            "      \"http://rblogger.ru/img2013/reports01/bashmet/bCHU_4867.jpg\",\n" +
            "      \"https://moscultura.ru/sites/default/files/photo/2018/02/bashmet6.jpg\",\n" +
            "      \"http://gosindex.ru/wp-content/uploads/2016/02/6068bashmet.jpg\",\n" +
            "      \"http://www.classicalmusicnews.ru/wp-content/uploads/2015/09/YUriy-Bashmet.jpg\"\n" +
            "    ]\n" +
            "  } \n" +
            "}";

    public GetActiveUserDataPostRequest(BasePresenter presenter, int stringErrorMessage) {
        super(presenter, stringErrorMessage);
        mPresenter = presenter;
    }

    @Override
    public StateRequest getStateRequest() {
        return mStateRequest;
    }

    @Override
    public void createAndSendRequest(String url, RequestBody params, String requestId) {
        mStateRequest = new ProgressStateRequest(mPresenter);
        setChanged();
        notifyObservers();

        mStateRequest = new SuccessStateRequest(RESPONSE, mPresenter, requestId);
        setChanged();
        notifyObservers();
    }

    @Override
    protected void createCustomRequest(Retrofit retrofit, RequestBody params) {

    }
}
