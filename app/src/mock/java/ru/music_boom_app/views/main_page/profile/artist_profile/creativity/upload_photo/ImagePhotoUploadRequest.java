package ru.music_boom_app.views.main_page.profile.artist_profile.creativity.upload_photo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Observable;

import okhttp3.RequestBody;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.request_observer.states.ProgressStateRequest;
import ru.music_boom_app.utils.request_observer.states.StateRequest;
import ru.music_boom_app.utils.request_observer.states.SuccessStateRequest;

/**
 * @author Markin Andrey on 23.04.2018.
 */
public class ImagePhotoUploadRequest extends Observable {
    private BasePresenter mPresenter;
    private int mStringErrorMessageId;
    private StateRequest mStateRequest;

    public ImagePhotoUploadRequest(BasePresenter presenter, int stringErrorMessageId) {
        mPresenter = presenter;
        mStringErrorMessageId = stringErrorMessageId;
    }

    public StateRequest getStateRequest() {
        return mStateRequest;
    }

    public void uploadFile(String url, RequestBody json, File avatarFile, String fileType, String requestId) {
        mStateRequest = new ProgressStateRequest(mPresenter);
        setChanged();
        notifyObservers();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("status", "SUCCESS");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mStateRequest = new SuccessStateRequest(jsonObject.toString(), mPresenter, requestId);
        setChanged();
        notifyObservers();
    }
}
