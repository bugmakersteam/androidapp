package ru.music_boom_app.views.main_page.perfomance;

import org.json.JSONObject;

/**
 * @author Markin Andrey on 15.04.2018.
 */
public class ActiveArtistsPostRequest {

    protected String createCustomRequest(JSONObject jsonObject) {
        String responseJson="{\n" +
                "  \"status\": \"SUCCESS\",\n" +
                "  \"artists\" :[\n" +
                "    {\n" +
                "      \"artist_id\": \"1\",\n" +
                "      \"longitude\": \"49.121066\",\n" +
                "      \"latitude\": \"55.787645\",\n" +
                "      \"icon_url\": \"https://habinfo.ru/wp-content/uploads/2017/04/kultura-1_ob-osnovnyh-sobytiyah-festivalya-iskusstv-yuriya-bashmeta_1.jpeg\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"artist_id\": \"2\",\n" +
                "      \"longitude\": \"49.117055\",\n" +
                "      \"latitude\": \"55.789612\",\n" +
                "      \"icon_url\": \"https://samsud.ru/images/photos/medium/article32562.jpg\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"artist_id\": \"3\",\n" +
                "      \"longitude\": \"49.115220\",\n" +
                "      \"latitude\": \"55.790537\",\n" +
                "      \"icon_url\": \"https://tverigrad.ru/wp-content/uploads/2015/05/72a1f264dad3d38b49439559a8c8de74.jpg\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"artist_id\": \"4\",\n" +
                "      \"longitude\": \"49.112967\",\n" +
                "      \"latitude\": \"55.791658\",\n" +
                "      \"icon_url\": \"http://www.nashideti.org/upload/medialibrary/b78/b7898e78166093d6090a980f1b780eb6.jpg\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"artist_id\": \"5\",\n" +
                "      \"longitude\": \"49.110805\",\n" +
                "      \"latitude\": \"55.792731\",\n" +
                "      \"icon_url\": \"http://www.belcanto.ru/media/images/publication/13102101.jpeg\"\n" +
                "    },\n" +
                "        {\n" +
                "      \"artist_id\": \"6\",\n" +
                "      \"longitude\": \"49.109125\",\n" +
                "      \"latitude\": \"55.793567\",\n" +
                "      \"icon_url\": \"http://lichnosti.net/photos/3518/13189558668.jpg\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"artist_id\": \"7\",\n" +
                "      \"longitude\": \"49.100557\",\n" +
                "      \"latitude\": \"55.796696\",\n" +
                "      \"icon_url\": \"http://www.classicalmusicnews.ru/wp-content/uploads/2014/05/Mstislav-Rostropovich.jpg\"\n" +
                "    },\n" +
                "        {\n" +
                "      \"artist_id\": \"8\",\n" +
                "      \"longitude\": \"49.123328\",\n" +
                "      \"latitude\": \"55.794012\",\n" +
                "      \"icon_url\": \"http://fullhdwallpapers.ru/image/music/22194/skripach.jpg\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"artist_id\": \"9\",\n" +
                "      \"longitude\": \"49.118286\",\n" +
                "      \"latitude\": \"55.794785\",\n" +
                "      \"icon_url\": \"http://www.peoples.ru/art/music/jazz/david_sanborn/news-sanborn_201003291641100.jpg\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"artist_id\": \"10\",\n" +
                "      \"longitude\": \"49.124201\",\n" +
                "      \"latitude\": \"55.783358\",\n" +
                "      \"icon_url\": \"https://4tololo.ru/files/images/20162907112659.jpg\"\n" +
                "    }\n" +
                "    ]\n" +
                "}";

        return responseJson;
    }
}
