package ru.music_boom_app.views.main_page.artist_work_area;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Retrofit;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.base_requests.BasePostRequest;
import ru.music_boom_app.utils.request_observer.states.ProgressStateRequest;
import ru.music_boom_app.utils.request_observer.states.StateRequest;
import ru.music_boom_app.utils.request_observer.states.SuccessStateRequest;

/**
 * @author Markin Andrey on 25.04.2018.
 */
public class StopPerformancePostRequest extends BasePostRequest {
    private StateRequest mStateRequest;
    private BasePresenter mPresenter;

    public StopPerformancePostRequest(BasePresenter presenter, int stringErrorMessage) {
        super(presenter, stringErrorMessage);
        mPresenter = presenter;
    }

    @Override
    protected void createCustomRequest(Retrofit retrofit, RequestBody params) {
    }

    @Override
    public void createAndSendRequest(String url, RequestBody params, String requestId) {
        mStateRequest = new ProgressStateRequest(mPresenter);
        setChanged();
        notifyObservers();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("status", "SUCCESS");
            jsonObject.put("earnedMoney", "1500");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mStateRequest = new SuccessStateRequest(jsonObject.toString(), mPresenter, requestId);
        setChanged();
        notifyObservers();
    }

    @Override
    public StateRequest getStateRequest() {
        return mStateRequest;
    }
}
