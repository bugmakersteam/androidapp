package ru.music_boom_app.views.authorization.VkAuth;

import android.os.Handler;

import java.util.Map;

import retrofit2.Retrofit;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.base_requests.BaseGetRequest;
import ru.music_boom_app.utils.request_observer.states.ProgressStateRequest;
import ru.music_boom_app.utils.request_observer.states.StateRequest;
import ru.music_boom_app.utils.request_observer.states.SuccessStateRequest;

/**
 * @author Markin Andrey on 27.02.2018.
 */
public class VkCheckRequest extends BaseGetRequest {
    private static final String RESPONSE_ACCESS_ARTIST = "{\n" +
            "  \"status\": \"SUCCESS\",\n" +
            "  \"user\": {\n" +
            "    \"sessionId\": \"123456\",\n" +
            "    \"userType\": \"ARTIST\",\n" +
            "    \"id\": \"1\",\n" +
            "    \"name\": \"Петр\",\n" +
            "    \"surname\": \"Евграфович\",\n" +
            "    \"patronymic\": \"Желткович\",\n" +
            "    \"birthday\": \"21.01.1111\",\n" +
            "    \"nickname\": \"superstar\",\n" +
            "    \"country\": \"Россия\",\n" +
            "    \"city\": \"Казань\",\n" +
            "    \"phoneNumber\": \"+79272454810\",\n" +
            "    \"email\": \"email@email.ru\",\n" +
            "    \"creativity\": \"Музыкант\",\n" +
            "    \"instrument\": \"Баян\",\n" +
            "    \"genre\": \"Рок\",\n" +
            "    \"cardNumber\": \"1234 **** **** 1234\",\n" +
            "    \"vk\": \"vk_link\",\n" +
            "    \"tlg\": \"@alias\",\n" +
            "    \"wapp\": \"+79272454810\",\n" +
            "    \"isOrdered\": \"true\",\n" +
            "    \"reg_date\": \"21.01.1111\",\n" +
            "    \"allEarnedMoney\": \"10000 руб.\",\n" +
            "    \"allDerivedMoney\": \"10000 руб.\",\n" +
            "    \"cityRating\": \"10,5\",\n" +
            "    \"countryRating\": \"10,5\",\n" +
            "    \"currentBalance\": \"3000\",\n" +
            "    \"allDonatedArtists\": \"12\",\n" +
            "    \"isLinkedCard\": \"true\",\n" +
            "    \"avatar\": \"http://wallpaperscraft.ru/image/sportkar_vid_speredi_stilnyj_117508_602x339.jpg\",\n" +
            "    \"aboutMe\": \"string\",\n" +
            "    \"photos\": [\n" +
            "      \"http://wallpaperscraft.ru/image/sportkar_vid_speredi_stilnyj_117508_602x339.jpg\",\n" +
            "      \"http://wallpaperscraft.ru/image/sportkar_vid_speredi_stilnyj_117508_602x339.jpg\"\n" +
            "    ],\n" +
            "    \"statOfPerfomance\":{\n" +
            "      \"allPerfomances\" : \"100\",\n" +
            "      \"hoursOfMonth\": \"65\",\n" +
            "      \"moneyOfMonth\": \"15000 руб.\",\n" +
            "      \"averagePerfomanceTime\": \"3\"\n" +
            "    }\n" +
            "  }\n" +
            "}";


    private BasePresenter mPresenter;
    private StateRequest mStateRequest;

    public VkCheckRequest(BasePresenter presenter, int stringErrorMessage) {
        super(presenter, stringErrorMessage);
        mPresenter = presenter;
    }

    @Override
    public StateRequest getStateRequest() {
        return mStateRequest;
    }

    @Override
    public void createAndSendRequest(String url, Map<String, String> params, String requestId) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                mStateRequest = new ProgressStateRequest(mPresenter);
                setChanged();
                notifyObservers();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        String social_id = params.get("social_id");
        String response = null;

        switch (social_id){
            case "11111":
                response = RESPONSE_ACCESS_ARTIST;
                break;
        }
        mStateRequest = new SuccessStateRequest(response, mPresenter, requestId);
        setChanged();
        notifyObservers();
    }

    @Override
    protected void createCustomRequest(Retrofit retrofit, Map<String, String> params) {

    }
}
