package ru.music_boom_app.views.authorization.VkAuth;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * @author Markin Andrey on 16.03.2018.
 */
interface IVkGetUserDataRequest {
    @GET("/method/users.get")
    Call<ResponseBody> baseGetRequest(@QueryMap Map<String, String> options);
}
