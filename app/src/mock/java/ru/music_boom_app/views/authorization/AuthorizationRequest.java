package ru.music_boom_app.views.authorization;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.RequestBody;
import okio.Buffer;
import retrofit2.Retrofit;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.base_requests.BasePostRequest;
import ru.music_boom_app.utils.request_observer.states.ProgressStateRequest;
import ru.music_boom_app.utils.request_observer.states.StateRequest;
import ru.music_boom_app.utils.request_observer.states.SuccessStateRequest;

/**
 * @author Markin Andrey on 22.02.2018.
 */
public class AuthorizationRequest extends BasePostRequest {
    private static final String RESPONSE_ACCESS_ARTIST = "{\n" +
            "  \"status\": \"SUCCESS\",\n" +
            "    \"sessionId\": \"123456\",\n" +
            "  \"user\": {\n" +
            "    \"userType\": \"ARTIST\",\n" +
            "    \"id\": \"1\",\n" +
            "    \"name\": \"Петр\",\n" +
            "    \"surname\": \"Евграфович\",\n" +
            "    \"patronymic\": \"Желткович\",\n" +
            "    \"birthday\": \"21.01.1111\",\n" +
            "    \"nickname\": \"superstar\",\n" +
            "    \"country\": \"Россия\",\n" +
            "    \"city\": \"Казань\",\n" +
            "    \"phoneNumber\": \"+79272454810\",\n" +
            "    \"email\": \"email@email.ru\",\n" +
            "    \"creativity\": \"musician\",\n" +
            "    \"instrument\": \"Баян\",\n" +
            "    \"genre\": \"Рок\",\n" +
            "    \"cardNumber\": \"1234 **** **** 1234\",\n" +
            "    \"vk\": \"vk_link\",\n" +
            "    \"tlg\": \"@alias\",\n" +
            "    \"wapp\": \"+79272454810\",\n" +
            "    \"isOrdered\": \"true\",\n" +
            "    \"regDate\": \"21.01.1111\",\n" +
            "    \"allEarnedMoney\": \"10000 руб.\",\n" +
            "    \"allDerivedMoney\": \"10000 руб.\",\n" +
            "    \"cityRating\": \"10,5\",\n" +
            "    \"countryRating\": \"10,5\",\n" +
            "    \"currentBalance\": \"3000\",\n" +
            "    \"allDonatedArtists\": \"\",\n" +
            "    \"isLinkedCard\": \"true\",\n" +
            "    \"avatar\": \"http://www.hqwallpapers.ru/wallpapers/music/skripach.jpg\",\n" +
            "    \"aboutMe\": \"Я жесткий игрун\",\n" +
            "    \"photos\": [\n" +
            "      \"https://yt3.ggpht.com/a-/AJLlDp1ZtRdTr4zt3knc0lrmQw6Dz6ktx28R4P8g9g=s900-mo-c-c0xffffffff-rj-k-no\",\n" +
            "      \"https://c.pxhere.com/photos/2f/51/street_orchestra_musicians_music_busker-857352.jpg!d\",\n" +
            "      \"https://img2.goodfon.ru/wallpaper/big/e/6e/muzykant-skripka-muzyka-5476.jpg\",\n" +
            "      \"http://i.photoblogs.ru/20141028/98599127841133_image068.jpg\"\n" +
            "    ],\n" +
            "    \"statOfPerformance\":{\n" +
            "      \"allPerformances\" : \"100\",\n" +
            "      \"hoursOfMonth\": \"65\",\n" +
            "      \"moneyOfMonth\": \"15000\",\n" +
            "      \"averagePerfomanceTime\": \"3\"\n" +
            "    }\n" +
            "  }\n" +
            "}";

    private static final String RESPONSE_ACCESS_LISTENER = "{\n" +
            "  \"status\": \"SUCCESS\",\n" +
            "    \"sessionId\": \"123456\",\n" +
            "  \"user\": {\n" +
            "    \"userType\": \"LISTENER\",\n" +
            "    \"id\": \"2\",\n" +
            "    \"name\": \"Арагорн\",\n" +
            "    \"surname\": \"Торовский\",\n" +
            "    \"patronymic\": \"Семенович\",\n" +
            "    \"birthday\": \"\",\n" +
            "    \"nickname\": \"\",\n" +
            "    \"country\": \"Россия\",\n" +
            "    \"city\": \"Казань\",\n" +
            "    \"phoneNumber\": \"+79272454810\",\n" +
            "    \"email\": \"alaska@gmail.com\",\n" +
            "    \"creativity\": \"\",\n" +
            "    \"instrument\": \"\",\n" +
            "    \"genre\": \"\",\n" +
            "    \"cardNumber\": \"\",\n" +
            "    \"vk\": \"\",\n" +
            "    \"tlg\": \"\",\n" +
            "    \"wapp\": \"\",\n" +
            "    \"isOrdered\": \"false\",\n" +
            "    \"reg_date\": \"21.01.1111\",\n" +
            "    \"allEarnedMoney\": \"\",\n" +
            "    \"allDerivedMoney\": \"\",\n" +
            "    \"cityRating\": \"\",\n" +
            "    \"countryRating\": \"\",\n" +
            "    \"currentBalance\": \"\",\n" +
            "    \"allDonatedArtists\": \"12\",\n" +
            "    \"isLinkedCard\": \"true\",\n" +
            "    \"avatar\": \"https://avatars.mds.yandex.net/get-pdb/750514/1e21688e-3641-49c1-977a-675fbf5b17dc/s1200\",\n" +
            "    \"aboutMe\": \"\",\n" +
            "    \"photos\": [\n" + "],\n" +
            "    \"statOfPerfomance\":{\n" +
            "      \"allPerfomances\" : \"\",\n" +
            "      \"hoursOfMonth\": \"\",\n" +
            "      \"moneyOfMonth\": \"\",\n" +
            "      \"averagePerfomanceTime\": \"\"\n" +
            "    }\n" +
            "  }\n" +
            "}";

    private BasePresenter mPresenter;
    private StateRequest mStateRequest;

    public AuthorizationRequest(BasePresenter presenter, int stringErrorMessage) {
        super(presenter, stringErrorMessage);
        mPresenter = presenter;
    }

    @Override
    public void createAndSendRequest(String url, RequestBody requestBody, String requestId) {
        mStateRequest = new ProgressStateRequest(mPresenter);
        setChanged();
        notifyObservers();

        String request = bodyToString(requestBody);
        String response = null;
        try {
            JSONObject jsonObject = new JSONObject(request);
            String login = jsonObject.getString("login");

            switch (login) {
                case "+79172656170":
                    response = RESPONSE_ACCESS_ARTIST;
                    break;
                case "+79172656171":
                    response = RESPONSE_ACCESS_LISTENER;
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mStateRequest = new SuccessStateRequest(response, mPresenter, requestId);
        setChanged();
        notifyObservers();
    }

    @Override
    public StateRequest getStateRequest() {
        return mStateRequest;
    }

    @Override
    protected void createCustomRequest(Retrofit retrofit, RequestBody params) {

    }

    private String bodyToString(final RequestBody request) {
        final RequestBody copy = request;
        final Buffer buffer = new Buffer();
        if (copy != null)
            try {
                copy.writeTo(buffer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        else
            return "";
        return buffer.readUtf8();
    }
}
