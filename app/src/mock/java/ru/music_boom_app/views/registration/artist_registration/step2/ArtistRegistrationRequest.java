package ru.music_boom_app.views.registration.artist_registration.step2;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.RequestBody;
import okio.Buffer;
import retrofit2.Retrofit;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.base_requests.BasePostRequest;
import ru.music_boom_app.utils.request_observer.states.ProgressStateRequest;
import ru.music_boom_app.utils.request_observer.states.StateRequest;
import ru.music_boom_app.utils.request_observer.states.SuccessStateRequest;

/**
 * @author Markin Andrey on 19.02.2018.
 */
public class ArtistRegistrationRequest extends BasePostRequest {

    private BasePresenter mPresenter;
    private StateRequest mStateRequest;

    public ArtistRegistrationRequest(BasePresenter presenter, int stringErrorMessage) {
        super(presenter, stringErrorMessage);
        mPresenter = presenter;
    }

    @Override
    public StateRequest getStateRequest() {
        return mStateRequest;
    }

    @Override
    protected void createCustomRequest(Retrofit retrofit, RequestBody params) {
    }

    @Override
    public void createAndSendRequest(String url, RequestBody requestBody, String requestId) {
        mStateRequest = new ProgressStateRequest(mPresenter);
        setChanged();
        notifyObservers();

        String request = bodyToString(requestBody);
        String response = null;

        try {
            JSONObject jsonObject = new JSONObject(request);
            JSONObject user = jsonObject.getJSONObject("user");
            user.remove("isAgreementOfPersonalData");
            user.remove("isArtistContract");
            user.put("userType", "ARTIST");
            user.put("isOrdered","false");
            jsonObject.put("status", "SUCCESS");
            jsonObject.put("sessionId","dfhdghjdghggmhhcghhj");
            response = jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mStateRequest = new SuccessStateRequest(response, mPresenter, requestId);
        setChanged();
        notifyObservers();
    }

    public String bodyToString(final RequestBody request) {
        final RequestBody copy = request;
        final Buffer buffer = new Buffer();
        if (copy != null)
            try {
                copy.writeTo(buffer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        else
            return "";
        return buffer.readUtf8();
    }
}
