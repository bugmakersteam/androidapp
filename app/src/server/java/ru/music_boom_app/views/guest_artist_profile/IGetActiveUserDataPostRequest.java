package ru.music_boom_app.views.guest_artist_profile;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Markin Andrey on 17.04.2018.
 */
public interface IGetActiveUserDataPostRequest {
    @POST("mapi/artist.get")
    Call<ResponseBody> basePostRequest(@Body RequestBody params);
}
