package ru.music_boom_app.views.authorization.FbAuth;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.base_requests.BaseGetRequest;
import ru.music_boom_app.utils.base_requests.ICheckAuthServicesRequest;

/**
 * @author Markin Andrey on 09.03.2018.
 */
public class FbCheckRequest extends BaseGetRequest {
    public FbCheckRequest(BasePresenter presenter, int stringErrorMessage) {
        super(presenter, stringErrorMessage);
    }

    @Override
    protected void createCustomRequest(Retrofit retrofit, Map<String, String> params) {
        ICheckAuthServicesRequest request = retrofit.create(ICheckAuthServicesRequest.class);
        request.baseGetRequest(params).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    setSuccessState(response);
                } else {
                    setFailState();
                }
                setChanged();
                notifyObservers();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                setFailState(call.request().toString() + t.getMessage());
                setChanged();
                notifyObservers();
            }
        });
    }
}
