package ru.music_boom_app.views.authorization;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Markin Andrey on 15.03.2018.
 */
public interface IAuthorizationPostRequest {
    @POST("authentication")
    Call<ResponseBody> basePostRequest(@Body RequestBody params);
}
