package ru.music_boom_app.views.authorization;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.base_requests.BasePostRequest;

/**
 * @author Markin Andrey on 22.02.2018.
 */
public class AuthorizationRequest extends BasePostRequest {
    public AuthorizationRequest(BasePresenter presenter, int stringErrorMessage) {
        super(presenter, stringErrorMessage);
    }

    @Override
    protected void createCustomRequest(Retrofit retrofit, RequestBody params) {
        IAuthorizationPostRequest request = retrofit.create(IAuthorizationPostRequest.class);
        request.basePostRequest(params).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    setSuccessState(response);
                } else {
                    setFailState();
                }
                setChanged();
                notifyObservers();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                setFailState(call.request().toString() + t.getMessage());
                setChanged();
                notifyObservers();
            }
        });
    }
}
