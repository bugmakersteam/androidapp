package ru.music_boom_app.views.registration.artist_registration.step2;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Markin Andrey on 15.03.2018.
 */
public interface IArtistRegistrationPostRequest {
    @POST("registration?user_type=artist")
    Call<ResponseBody> basePostRequest(@Body RequestBody params);
}
