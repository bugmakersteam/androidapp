package ru.music_boom_app.views.registration.listener_registration;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Markin Andrey on 16.03.2018.
 */
public interface IListenerRegistrationRequest {
    @POST("registration?user_type=listener")
    Call<ResponseBody> basePostRequest(@Body RequestBody params);
}
