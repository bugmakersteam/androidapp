package ru.music_boom_app.views.main_page.profile.listener_profile.change_password_dialog;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Markin Andrey on 05.04.2018.
 */
public interface IChangePasswordListenerPostRequest {
    @POST("mapi/listener/editing/password.change")
    Call<ResponseBody> basePostRequest(@Body RequestBody params);
}
