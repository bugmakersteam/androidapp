package ru.music_boom_app.views.main_page.finance.artist_finance.link_card_dialog;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Markin Andrey on 24.04.2018.
 */
public interface IChangeArtistCardNumberPostRequest {
    @POST("mapi/artist/card.update")
    Call<ResponseBody> basePostRequest(@Body RequestBody params);
}
