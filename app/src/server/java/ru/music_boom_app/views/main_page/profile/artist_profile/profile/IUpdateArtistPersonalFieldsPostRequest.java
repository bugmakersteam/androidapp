package ru.music_boom_app.views.main_page.profile.artist_profile.profile;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Markin Andrey on 21.04.2018.
 */
public interface IUpdateArtistPersonalFieldsPostRequest {
    @POST("mapi/artist/editing/personal")
    Call<ResponseBody> basePostRequest(@Body RequestBody params);
}
