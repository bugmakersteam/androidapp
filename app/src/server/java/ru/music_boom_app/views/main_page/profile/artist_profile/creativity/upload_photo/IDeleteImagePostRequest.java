package ru.music_boom_app.views.main_page.profile.artist_profile.creativity.upload_photo;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Markin Andrey on 23.04.2018.
 */
public interface IDeleteImagePostRequest {
    @POST("mapi/artist/editing/photos.delete")
    Call<ResponseBody> basePostRequest(@Body RequestBody params);
}
