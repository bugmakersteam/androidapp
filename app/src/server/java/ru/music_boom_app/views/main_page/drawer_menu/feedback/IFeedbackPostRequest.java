package ru.music_boom_app.views.main_page.drawer_menu.feedback;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Markin Andrey on 18.04.2018.
 */
public interface IFeedbackPostRequest {
    @POST("mapi/registereduser/feedback.send")
    Call<ResponseBody> basePostRequest(@Body RequestBody params);
}
