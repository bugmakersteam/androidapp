package ru.music_boom_app.views.main_page.profile.artist_profile.creativity.upload_photo;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * @author Markin Andrey on 22.04.2018.
 */
public interface IImagePhotoUploadRequest {
    @Multipart
    @POST("mapi/artist/editing/photos.upload")
    Call<ResponseBody> uploadAvatar(@Part("json") RequestBody json, @Part MultipartBody.Part file);
}
