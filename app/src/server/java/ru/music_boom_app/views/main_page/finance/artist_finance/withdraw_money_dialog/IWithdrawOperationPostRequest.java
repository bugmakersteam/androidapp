package ru.music_boom_app.views.main_page.finance.artist_finance.withdraw_money_dialog;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Markin Andrey on 24.04.2018.
 */
public interface IWithdrawOperationPostRequest {
    @POST("mapi/artist/withdraw")
    Call<ResponseBody> basePostRequest(@Body RequestBody params);
}
