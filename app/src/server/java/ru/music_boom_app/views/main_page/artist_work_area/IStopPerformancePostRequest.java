package ru.music_boom_app.views.main_page.artist_work_area;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Markin Andrey on 25.04.2018.
 */
public interface IStopPerformancePostRequest {
    @POST("mapi/artist/performance.end")
    Call<ResponseBody> basePostRequest(@Body RequestBody params);
}
