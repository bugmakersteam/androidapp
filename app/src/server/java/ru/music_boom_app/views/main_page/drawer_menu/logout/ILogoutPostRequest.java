package ru.music_boom_app.views.main_page.drawer_menu.logout;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Markin Andrey on 18.04.2018.
 */
public interface ILogoutPostRequest {
    @POST("logout")
    Call<ResponseBody> basePostRequest(@Body RequestBody params);
}
