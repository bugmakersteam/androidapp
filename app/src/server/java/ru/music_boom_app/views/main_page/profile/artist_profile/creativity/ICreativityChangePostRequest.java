package ru.music_boom_app.views.main_page.profile.artist_profile.creativity;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Markin Andrey on 22.04.2018.
 */
public interface ICreativityChangePostRequest {
    @POST("mapi/artist/editing/creativity.change")
    Call<ResponseBody> basePostRequest(@Body RequestBody params);
}
