package ru.music_boom_app.views.main_page.perfomance;

import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import ru.music_boom_app.BuildConfig;

/**
 * @author Markin Andrey on 12.04.2018.
 */
public class ActiveArtistsPostRequest {

    protected String createCustomRequest(JSONObject jsonObject) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.URL)
                .client(client)
                .build();
        RequestBody params = createRequestBodyText("registrationJson", jsonObject.toString());

        IActiveArtistsPostRequest request = retrofit.create(IActiveArtistsPostRequest.class);
        try {
            ResponseBody body = request.basePostRequest(params).execute().body();
            return body.string();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private RequestBody createRequestBodyText(String key, String value) {
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), value);
    }
}
