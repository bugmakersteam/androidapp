package ru.music_boom_app.views.main_page.profile.listener_profile;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Markin Andrey on 03.04.2018.
 */
public interface IUpdateListenerPersonalFieldsRequest {
    @POST("mapi/listener/editing/personal")
    Call<ResponseBody> basePostRequest(@Body RequestBody params);
}
