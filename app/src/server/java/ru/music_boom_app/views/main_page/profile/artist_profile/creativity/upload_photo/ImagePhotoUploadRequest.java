package ru.music_boom_app.views.main_page.profile.artist_profile.creativity.upload_photo;

import java.io.File;
import java.io.IOException;
import java.util.Observable;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.request_observer.states.FailStateRequest;
import ru.music_boom_app.utils.request_observer.states.ProgressStateRequest;
import ru.music_boom_app.utils.request_observer.states.StateRequest;
import ru.music_boom_app.utils.request_observer.states.SuccessStateRequest;

/**
 * @author Markin Andrey on 22.04.2018.
 */
public class ImagePhotoUploadRequest extends Observable {
    private BasePresenter mPresenter;
    private int mStringErrorMessageId;
    private StateRequest mStateRequest;

    public ImagePhotoUploadRequest(BasePresenter presenter, int stringErrorMessageId) {
        mPresenter = presenter;
        mStringErrorMessageId = stringErrorMessageId;
    }

    public StateRequest getStateRequest() {
        return mStateRequest;
    }

    public void uploadFile(String url, RequestBody json, File imageFile, String fileType, String requestId){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .build();

        IImagePhotoUploadRequest request = retrofit.create(IImagePhotoUploadRequest.class);
        mStateRequest = new ProgressStateRequest(mPresenter);
        setChanged();
        notifyObservers();

        RequestBody imageRequestBody = RequestBody.create(
                MediaType.parse("image/"+fileType),
                imageFile
        );
        MultipartBody.Part avatarPart = MultipartBody.Part.createFormData("image", imageFile.getName(), imageRequestBody);

        Call<ResponseBody> call = request.uploadAvatar(json, avatarPart);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        mStateRequest = new SuccessStateRequest(
                                response.body().string(),
                                mPresenter,
                                requestId
                        );
                        setChanged();
                        notifyObservers();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    mStateRequest = new FailStateRequest(mPresenter, mStringErrorMessageId, url);
                    setChanged();
                    notifyObservers();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mStateRequest = new FailStateRequest(mPresenter, mStringErrorMessageId, url);
                setChanged();
                notifyObservers();
            }
        });
    }
}
