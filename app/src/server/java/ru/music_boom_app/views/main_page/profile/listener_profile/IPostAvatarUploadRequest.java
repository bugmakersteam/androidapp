package ru.music_boom_app.views.main_page.profile.listener_profile;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * @author Markin Andrey on 02.04.2018.
 */
public interface IPostAvatarUploadRequest {
    @Multipart
    @POST("mapi/listener/editing/avatar.change")
    Call<ResponseBody> uploadAvatar(@Part("json") RequestBody json, @Part MultipartBody.Part file);
}
