package ru.music_boom_app.dagger.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * @author Markin Andrey on 26.02.2018.
 */
@Module
public class CommonModule {
    private final Context mContext;

    public CommonModule (Context context){
        mContext = context;
    }

    @Provides
    public Context provideContext(){
        return mContext;
    }

}
