package ru.music_boom_app.dagger.modules;

import dagger.Module;
import dagger.Provides;
import ru.music_boom_app.utils.PermissionsUtils;

/**
 * @author Markin Andrey on 08.12.2017.
 */
@Module
public class PermissionUtilsModule {
    @Provides
    public PermissionsUtils providePermissionUtils(){
        return new PermissionsUtils();
    }
}
