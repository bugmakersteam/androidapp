package ru.music_boom_app.dagger.modules;

import dagger.Module;
import dagger.Provides;
import ru.music_boom_app.utils.NetworkUtils;

/**
 * @author Markin Andrey on 06.12.2017.
 */
@Module
public class NetworkUtilsModule {
    @Provides
    public NetworkUtils provideNetworkUtils(){
        return  new NetworkUtils();
    }
}
