package ru.music_boom_app.dagger.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import ru.music_boom_app.utils.VkAuthorizationUtils;

/**
 * @author Markin Andrey on 26.02.2018.
 */
@Module
public class AuthorizationServicesModule {

    @Provides
    public VkAuthorizationUtils VkInit(Context context){
        VkAuthorizationUtils vkAuthorizationUtils = new VkAuthorizationUtils(context);
        return vkAuthorizationUtils;
    }
}
