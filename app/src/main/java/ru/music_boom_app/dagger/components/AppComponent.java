package ru.music_boom_app.dagger.components;

import javax.inject.Singleton;

import dagger.Component;
import ru.music_boom_app.dagger.modules.AuthorizationServicesModule;
import ru.music_boom_app.dagger.modules.CommonModule;
import ru.music_boom_app.dagger.modules.NetworkUtilsModule;
import ru.music_boom_app.dagger.modules.PermissionUtilsModule;
import ru.music_boom_app.views.authorization.AuthorizationFragment;
import ru.music_boom_app.views.guest_artist_profile.GuestArtistProfileFragment;
import ru.music_boom_app.views.main_page.MainPageActivity;
import ru.music_boom_app.views.main_page.artist_work_area.ArtistWorkAreaFragment;
import ru.music_boom_app.views.main_page.artist_work_area.MyService;
import ru.music_boom_app.views.main_page.artist_work_area.SendPerformStateIntentService;
import ru.music_boom_app.views.main_page.donate.DonateActivity;
import ru.music_boom_app.views.main_page.drawer_menu.feedback.FeedbackFragment;
import ru.music_boom_app.views.main_page.finance.artist_finance.link_card_dialog.LinkCardDialogFragment;
import ru.music_boom_app.views.main_page.finance.artist_finance.withdraw_money_dialog.WithdrawMoneyDialog;
import ru.music_boom_app.views.main_page.perfomance.PerformanceFragment;
import ru.music_boom_app.views.main_page.profile.artist_profile.creativity.ArtistCreativityFragment;
import ru.music_boom_app.views.main_page.profile.artist_profile.creativity.upload_photo.PhotoFragmentUpl;
import ru.music_boom_app.views.main_page.profile.artist_profile.profile.ArtistProfileFragment;
import ru.music_boom_app.views.main_page.profile.listener_profile.ListenerProfileFragment;
import ru.music_boom_app.views.main_page.profile.listener_profile.change_password_dialog.ChangePasswordDialogFragment;
import ru.music_boom_app.views.main_page.profile.listener_profile.create_avatar_dialog.CreateAvatarDialogFragment;
import ru.music_boom_app.views.registration.artist_registration.step2.RegistrationArtistFragmentStep_2;
import ru.music_boom_app.views.registration.listener_registration.RegistrationListenerFragmentStep_1;
import ru.music_boom_app.views.registration.step1.RegistrationFragmentStep_1;

/**
 * @author Markin Andrey on 06.12.2017.
 */
@Component(modules = {
        CommonModule.class,
        NetworkUtilsModule.class,
        PermissionUtilsModule.class,
        AuthorizationServicesModule.class
})
@Singleton
public interface AppComponent {
    void inject (RegistrationFragmentStep_1 fragment);
    void inject (RegistrationListenerFragmentStep_1 fragment);
    void inject (RegistrationArtistFragmentStep_2 fragment);
    void inject (AuthorizationFragment fragment);
    void inject (ListenerProfileFragment fragment);
    void inject (CreateAvatarDialogFragment fragment);
    void inject (ChangePasswordDialogFragment fragment);
    void inject (DonateActivity activity);
    void inject (PerformanceFragment fragment);
    void inject (GuestArtistProfileFragment fragment);
    void inject (FeedbackFragment fragment);
    void inject (MainPageActivity activity);
    void inject (ArtistProfileFragment fragment);
    void inject (ArtistCreativityFragment fragment);
    void inject (PhotoFragmentUpl fragment);
    void inject (LinkCardDialogFragment fragment);
    void inject (WithdrawMoneyDialog dialog);
    void inject (ArtistWorkAreaFragment fragment);
    void inject (SendPerformStateIntentService intentService);
    void inject (MyService service);
}
