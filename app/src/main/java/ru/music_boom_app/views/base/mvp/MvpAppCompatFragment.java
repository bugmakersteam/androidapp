package ru.music_boom_app.views.base.mvp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.arellomobile.mvp.MvpDelegate;

/**
 * @author Markin Andrey on 21.11.2017.
 */
public class MvpAppCompatFragment extends Fragment {

    private boolean mIsStateSaved;
    private MvpDelegate<? extends MvpAppCompatFragment> mMvpDelegate;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getMvpDelegate().onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        mIsStateSaved = false;
        getMvpDelegate().onAttach();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIsStateSaved = false;
        getMvpDelegate().onAttach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mIsStateSaved = true;
        mMvpDelegate.onSaveInstanceState(outState);
        mMvpDelegate.onDetach();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMvpDelegate.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getMvpDelegate().onDetach();
        getMvpDelegate().onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (getActivity().isFinishing()) {
            getMvpDelegate().onDestroy();
            return;
        }
        if (mIsStateSaved) {
            mIsStateSaved = false;
            return;
        }
        boolean anyParentIsRemoving = false;
        Fragment parent = getParentFragment();

        while (!anyParentIsRemoving && parent != null) {
            anyParentIsRemoving = parent.isRemoving();
            parent = getParentFragment();
        }
        if (isRemoving() || anyParentIsRemoving){
            getMvpDelegate().onDestroy();
        }
    }

    private MvpDelegate getMvpDelegate() {
        if (mMvpDelegate == null) {
            mMvpDelegate = new MvpDelegate<>(this);
        }
        return mMvpDelegate;
    }
}
