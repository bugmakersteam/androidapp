package ru.music_boom_app.views.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

import ru.music_boom_app.R;
import ru.music_boom_app.views.base.mvp.MvpAppCompatActivity;

/**
 * @author Markin Andrey on 22.11.2017.
 */
public class BaseApplicationActivity extends MvpAppCompatActivity
{
    protected static final String FAIL_DIALOG = "fail_dialog";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
