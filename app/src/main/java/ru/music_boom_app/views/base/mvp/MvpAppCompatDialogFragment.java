package ru.music_boom_app.views.base.mvp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialogFragment;

import com.arellomobile.mvp.MvpDelegate;

/**
 * @author Markin Andrey on 22.11.2017.
 */
public class MvpAppCompatDialogFragment extends AppCompatDialogFragment {
    private boolean mIsStateSaved;
    private MvpDelegate<? extends MvpAppCompatDialogFragment> mMvpDelegate;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getMvpDelegate().onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mIsStateSaved = false;
        getMvpDelegate().onAttach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mIsStateSaved = true;
        getMvpDelegate().onSaveInstanceState(outState);
        getMvpDelegate().onDetach();
    }

    @Override
    public void onStop() {
        super.onStop();
        getMvpDelegate().onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getMvpDelegate().onDetach();
        getMvpDelegate().onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getActivity().isFinishing()) {
            getMvpDelegate().onDestroy();
        }
        if (mIsStateSaved) {
            mIsStateSaved = false;
            return;
        }
        boolean anyParentIsRemoving = false;
        Fragment parent = getParentFragment();

        while (!anyParentIsRemoving && parent != null) {
            anyParentIsRemoving = parent.isRemoving();
            parent = getParentFragment();
        }
        if (isRemoving() && anyParentIsRemoving) {
            getMvpDelegate().onDestroy();
        }
    }

    public MvpDelegate getMvpDelegate() {
        if (mMvpDelegate == null) {
            mMvpDelegate = new MvpDelegate<>(this);
        }
        return mMvpDelegate;
    }
}

