package ru.music_boom_app.views.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

import ru.music_boom_app.views.base.mvp.MvpAppCompatFragment;

/**
 * @author Markin Andrey on 22.11.2017.
 */
public class BaseApplicationFragment extends MvpAppCompatFragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
}
