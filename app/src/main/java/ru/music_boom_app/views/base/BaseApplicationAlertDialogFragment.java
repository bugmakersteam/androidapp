package ru.music_boom_app.views.base;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import ru.music_boom_app.R;
import ru.music_boom_app.views.base.mvp.MvpAppCompatDialogFragment;

/**
 * @author Markin Andrey on 22.11.2017.
 */
public class BaseApplicationAlertDialogFragment extends MvpAppCompatDialogFragment {

    public static BaseApplicationAlertDialogFragment newDialog(
            String title,
            String message,
            String possitiveButtonText){
        BaseApplicationAlertDialogFragment dialogFragment = new BaseApplicationAlertDialogFragment();
        dialogFragment.setTitle(title);
        dialogFragment.setMessage(message);
        dialogFragment.setPositiveButtonText(possitiveButtonText);
        return dialogFragment;
    }

    protected String mTitle;
    protected String mMessage;
    protected String mPositiveButtonText;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setTitle(mTitle);
        builder.setMessage(mMessage);
        builder.setPositiveButton(mPositiveButtonText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        return builder.create();
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getPositiveButtonText() {
        return mPositiveButtonText;
    }

    public void setPositiveButtonText(String positiveButtonText) {
        mPositiveButtonText = positiveButtonText;
    }
}
