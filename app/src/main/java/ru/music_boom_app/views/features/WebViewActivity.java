package ru.music_boom_app.views.features;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.webkit.WebView;

import ru.music_boom_app.R;
import ru.music_boom_app.views.authorization.AuthorizationFragment;
import ru.music_boom_app.views.base.BaseApplicationActivity;

/**
 * @author Markin Andrey on 10.03.2018.
 */
public class WebViewActivity extends BaseApplicationActivity {

    private WebView mWebView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        mWebView = (WebView) findViewById(R.id.web_view);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        String url = getIntent().getStringExtra(AuthorizationFragment.URL_WEB_VIEW_EXTRA);

        if (URLUtil.isValidUrl(url)){
            mWebView.loadUrl(url);
        } else {
            finish();
        }
    }
}
