package ru.music_boom_app.views.guest_artist_profile;

import android.graphics.Bitmap;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import ru.music_boom_app.models.beans.GuestProfileArtistBean;

/**
 * @author Markin Andrey on 16.04.2018.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface GuestArtistProfileView extends MvpView {
    void onShowFailDialog(int dialogErrorMessageId);
    void onShowProgress();
    void onHideProgress();
    void onAvatarDownloaded(Bitmap bitmap, String userName);
    void onSetFields (GuestProfileArtistBean bean);
}
