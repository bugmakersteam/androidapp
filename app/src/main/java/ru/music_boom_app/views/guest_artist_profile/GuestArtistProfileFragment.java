package ru.music_boom_app.views.guest_artist_profile;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.GuestProfileArtistBean;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;
import ru.music_boom_app.views.base.BaseApplicationFragment;
import ru.music_boom_app.views.main_page.MainPageActivity;
import ru.music_boom_app.views.registration.artist_registration.step1.RegistrationArtistFragmentPresenterStep_1;

/**
 * @author Markin Andrey on 16.04.2018.
 */
public class GuestArtistProfileFragment extends BaseApplicationFragment implements GuestArtistProfileView {
    private static final String ERROR_OPERATION = "error_operation";
    private static final String ACTIVE_ARTIST_BEAN_EXTRA = "active_artist_bean_extra";
    private static final String USER_BEAN_EXTRA = "guest_user_bean_extra";


    @Inject
    NetworkUtils mNetworkUtils;
    @InjectPresenter
    GuestArtistProfilePresenter mPresenter;

    @BindView(R.id.avatar_container)
    CollapsingToolbarLayout mToolbarLayout;
    @BindView(R.id.app_bar)
    AppBarLayout mAppBarLayout;
    @BindView(R.id.user_avatar)
    ImageView mUserAvatarImageView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.creativity_title)
    TextView mCreativityTitle;
    @BindView(R.id.creativity)
    TextView mCreativity;
    @BindView(R.id.genre_title)
    TextView mGenreTitle;
    @BindView(R.id.genre)
    TextView mGenre;
    @BindView(R.id.musician_instrument_title)
    TextView mMusicianinstrumentTitle;
    @BindView(R.id.musician_instrument)
    TextView mMusicianinstrument;
    @BindView(R.id.ordered_message)
    TextView mOrderedMessage;
    @BindView(R.id.phone_number_title)
    TextView mPhoneNumberTitle;
    @BindView(R.id.phone_number)
    TextView mPhoneNumber;
    @BindView(R.id.vk_link_title)
    TextView mVkLinkTitle;
    @BindView(R.id.vk_link)
    TextView mVkLink;
    @BindView(R.id.wapp_num_title)
    TextView mWappNumTitle;
    @BindView(R.id.wapp_link)
    TextView mWappNum;
    @BindView(R.id.photo_pager)
    ViewPager mPhotoPager;
    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    private long mArtistId;
    private UserBean mUserBean;

    public static GuestArtistProfileFragment newInstance(long artistId, UserBean userBean) {
        Bundle args = new Bundle();
        args.putLong(ACTIVE_ARTIST_BEAN_EXTRA, artistId);
        args.putParcelable(USER_BEAN_EXTRA, userBean);
        GuestArtistProfileFragment fragment = new GuestArtistProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(getContext()).inject(this);
        Bundle bundle = getArguments();
        mArtistId = bundle.getLong(ACTIVE_ARTIST_BEAN_EXTRA);
        mUserBean = (UserBean) bundle.getParcelable(USER_BEAN_EXTRA);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.guest_artist_profile_fragment, container, false);
        ButterKnife.bind(this, view);
        MainPageActivity activity = (MainPageActivity) getActivity();
        activity.showDonateButton(mUserBean, String.valueOf(mArtistId));

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.startDownloadArtistData(getContext(), mUserBean.getSessionId(), mArtistId, mNetworkUtils);
    }

    @Override
    public void onResume() {
        super.onResume();
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(mToolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MainPageActivity activity = (MainPageActivity) getActivity();
        activity.hideDonateButton();
    }

    @Override
    public void onShowFailDialog(int dialogErrorMessageId) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(dialogErrorMessageId),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), ERROR_OPERATION);
    }

    @Override
    public void onShowProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAvatarDownloaded(Bitmap bitmap, String userName) {
        mUserAvatarImageView.setImageBitmap(bitmap);
        mToolbarLayout.setExpandedTitleTextAppearance(R.style.TextAppearance_AppCompat_Title);
        mToolbarLayout.setTitle(userName);
        mToolbar.setTitle(userName);
    }

    @Override
    public void onSetFields(GuestProfileArtistBean bean) {
        if (!TextUtils.isEmpty(bean.getCreativity())) {
            mCreativityTitle.setVisibility(View.VISIBLE);
            mCreativity.setVisibility(View.VISIBLE);
            String creativity = bean.getCreativity();
            mCreativity.setText(creativity);

            if (RegistrationArtistFragmentPresenterStep_1.MUSICIAN_CREATIVITY.equals(creativity)) {
                mMusicianinstrumentTitle.setVisibility(View.VISIBLE);
                mMusicianinstrument.setVisibility(View.VISIBLE);
                mMusicianinstrument.setText(bean.getInstrument());
            }
        }

        if (!TextUtils.isEmpty(bean.getGenre())) {
            mGenreTitle.setVisibility(View.VISIBLE);
            mGenre.setVisibility(View.VISIBLE);
            mGenre.setText(bean.getGenre());
        }

        if (bean.isOrdered()) {
            mOrderedMessage.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(bean.getPhoneNumber())) {
                mPhoneNumberTitle.setVisibility(View.VISIBLE);
                mPhoneNumber.setVisibility(View.VISIBLE);
                mPhoneNumber.setText(bean.getPhoneNumber());
            }

            if (!TextUtils.isEmpty(bean.getVkLink())) {
                mVkLinkTitle.setVisibility(View.VISIBLE);
                mVkLink.setVisibility(View.VISIBLE);
                mVkLink.setText(bean.getVkLink());
            }

            if (!TextUtils.isEmpty(bean.getWappNumber())) {
                mWappNumTitle.setVisibility(View.VISIBLE);
                mWappNum.setVisibility(View.VISIBLE);
                mWappNum.setText(bean.getWappNumber());
            }
        }
        mToolbar.setTitle(bean.getArtistName());
        mToolbarLayout.setTitle(bean.getArtistName());
        initViewPager(bean);
    }

    private void initViewPager(GuestProfileArtistBean bean) {
        FragmentPagerAdapter adapter = new FragmentPagerAdapter(getActivity().getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {

                return PhotoFragment.newInstance(bean.getPhotosURL().get(position));
            }

            @Override
            public int getCount() {
                return bean.getPhotosURL().size();
            }
        };
        mPhotoPager.setAdapter(adapter);
        mPhotoPager.setVisibility(View.VISIBLE);
        mPhotoPager.setCurrentItem(0);
        if (bean.getPhotosURL().size() > 0) {
            int size = bean.getPhotosURL().size();
            for (int i = 0; i < size; i++) {
                mPhotoPager.setCurrentItem(i);
            }
        }
        mPhotoPager.setCurrentItem(0);
    }
}
