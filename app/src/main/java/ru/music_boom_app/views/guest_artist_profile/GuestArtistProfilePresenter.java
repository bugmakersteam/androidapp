package ru.music_boom_app.views.guest_artist_profile;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Observable;
import java.util.Observer;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.GuestProfileArtistBean;
import ru.music_boom_app.models.parsers.BaseResponseParser;
import ru.music_boom_app.models.parsers.GuestProfileArtistParser;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.request_observer.states.StateRequest;

/**
 * @author Markin Andrey on 16.04.2018.
 */
@InjectViewState
public class GuestArtistProfilePresenter extends BasePresenter<GuestArtistProfileView>{
    private static final String GET_ACTIVE_ARTIST_DATA_REQUEST = "get_active_artist_data_request";
    private GuestProfileArtistBean mArtistBean;

    @Override
    public void setResponse(String response, String requestId) {
        if (GET_ACTIVE_ARTIST_DATA_REQUEST.equals(requestId)){
            if (BaseResponseParser.checkSuccessResponse(response)) {
                mArtistBean = GuestProfileArtistParser.parseArtistBean(response);
                getViewState().onSetFields(mArtistBean);
                downloadAvatar(mArtistBean.getAvatarURL(), mArtistBean.getArtistName());
            } else {
                getViewState().onShowFailDialog(R.string.request_fail_message);
            }
        }
    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        getViewState().onShowFailDialog(dialogErrorMessageId);
    }

    @Override
    public void showProgressBar() {
        getViewState().onShowProgress();
    }

    @Override
    public void hideProgressBar() {
        getViewState().onHideProgress();
    }

    public void startDownloadArtistData(Context context, String sessionId, long artistId, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)){
            GetActiveUserDataPostRequest postRequest = new GetActiveUserDataPostRequest(this, R.string.request_fail_message);
            postRequest.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    GetActiveUserDataPostRequest request = (GetActiveUserDataPostRequest) o;
                    StateRequest stateRequest = request.getStateRequest();
                    stateRequest.shareData();
                }
            });

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", sessionId);
                jsonObject.put("user_id", artistId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            RequestBody requestBody = createRequestBodyText("registrationJson", jsonObject.toString());
            postRequest.createAndSendRequest(BuildConfig.URL, requestBody,GET_ACTIVE_ARTIST_DATA_REQUEST);
        } else {
            getViewState().onShowFailDialog(R.string.network_error);
        }
    }

    private RequestBody createRequestBodyText(String key, String value) {
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), value);
    }

    private void downloadAvatar(String avatarUrl, String userName){
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                getViewState().onAvatarDownloaded(bitmap, userName);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        if (!TextUtils.isEmpty(avatarUrl)) {
            Picasso.get().load(avatarUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(target);
        }
    }
}
