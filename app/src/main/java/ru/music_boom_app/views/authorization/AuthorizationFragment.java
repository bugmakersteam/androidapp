package ru.music_boom_app.views.authorization;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.vision.barcode.Barcode;

import net.rimoto.intlphoneinput.IntlPhoneInput;

import javax.inject.Inject;

import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.PermissionsUtils;
import ru.music_boom_app.utils.VkAuthorizationUtils;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;
import ru.music_boom_app.views.base.BaseApplicationFragment;

import static android.app.Activity.RESULT_OK;

/**
 * @author Markin Andrey on 05.01.2018.
 */
public class AuthorizationFragment extends BaseApplicationFragment implements AuthorizationFragmentView {

    public static final String QR_CODE_SCANNER_RESULT_EXTRA = "qr_result";
    public static final int QR_SCANNER_REQUEST_CODE = 1923;
    public static final int GOOGLE_AUTH_REQUEST_CODE = 1924;
    public static final String URL_WEB_VIEW_EXTRA = "web_view_extra";
    private static final String ERROR_OPERATION = "error_operation";

    @Inject
    VkAuthorizationUtils mVkAuthorizationUtils;
    @Inject
    PermissionsUtils mPermissionsUtils;
    @Inject
    NetworkUtils mNetworkUtils;

    @InjectPresenter
    AuthorizationFragmentPresenter mPresenter;

    public static AuthorizationFragment newInstance() {
        Bundle args = new Bundle();
        AuthorizationFragment fragment = new AuthorizationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private IntlPhoneInput mLogin;
    private EditText mPassword;
    private TextInputLayout mLoginInputLayout;
    private TextInputLayout mPasswordInputLayout;
    private Button mLoginButton;
    private ImageView mGoogleLogin;
    private ImageView mVkLogin;
    private ImageView mFbLogin;
    private LoginButton mFacebookLoginButton;
    private ImageView mQrCodeScan;
    private Button mRegistrationButton;
    private ProgressBar mProgressBar;
    private CallbackManager mCallbackManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(getContext()).inject(this);
        mCallbackManager = CallbackManager.Factory.create();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.authorization_fragment, container, false);

        mLoginInputLayout = (TextInputLayout) view.findViewById(R.id.login_layout);
        mPassword = (EditText) view.findViewById(R.id.password);
        mPasswordInputLayout = (TextInputLayout) view.findViewById(R.id.password_layout);
        mLoginButton = (Button) view.findViewById(R.id.log_in_button);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.getAuthorization(getActivity(),
                        mLogin,
                        mPassword.getText().toString(),
                        mNetworkUtils);
            }
        });

        mVkLogin = (ImageView) view.findViewById(R.id.sign_in_vk);
        mVkLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mVkAuthorizationUtils.getLogin(getActivity());
            }
        });
        mFacebookLoginButton = (LoginButton) view.findViewById(R.id.facebook_login_original);
        mFacebookLoginButton.setReadPermissions("email", "user_birthday", "public_profile", "user_location");
        mFacebookLoginButton.setFragment(this);
        mFacebookLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mPresenter.checkFb(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
                mPresenter.getViewState()
                        .showFailDialog(R.string.request_fail_message);
            }
        });
        mFbLogin = (ImageView) view.findViewById(R.id.sing_in_fb);
        mFbLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFacebookLoginButton.performClick();
            }
        });
        mRegistrationButton = (Button) view.findViewById(R.id.registration);
        mQrCodeScan = (ImageView) view.findViewById(R.id.qr_code_scan);
        mQrCodeScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.startQR_codeScanner(getActivity(), AuthorizationFragment.this, mPermissionsUtils);
            }
        });

        mGoogleLogin = (ImageView) view.findViewById(R.id.sign_in_google);
        mGoogleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.startGoogleAuth(getContext(), AuthorizationFragment.this, mNetworkUtils);
            }
        });

        mLogin = (IntlPhoneInput) view.findViewById(R.id.login_phone_number);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        mRegistrationButton.setOnClickListener(mPresenter.registrationButtonHandler(fragmentManager));
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == QR_SCANNER_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Barcode result = data.getParcelableExtra(QR_CODE_SCANNER_RESULT_EXTRA);
                mPresenter.showWebPage(getActivity(), result.displayValue, mNetworkUtils);
            } else {
                mPresenter.getViewState()
                        .showFailDialog(R.string.qr_code_scanner_fail_result);
            }
        }

        if (requestCode == GOOGLE_AUTH_REQUEST_CODE) {
            Task<GoogleSignInAccount> signedInAccountFromIntent = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = signedInAccountFromIntent.getResult(ApiException.class);
                mPresenter.checkGoogle(account);
            } catch (ApiException e) {
                e.printStackTrace();
                mPresenter.getViewState()
                        .showFailDialog(R.string.request_fail_message);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case AuthorizationFragmentPresenter.QR_CODE_SCANNER_PERMISSION_REQUEST:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    mPresenter.startQR_codeScanner(getActivity(), this, mPermissionsUtils);
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Override
    public void authorizationEmptyLoginException(int messageId) {
        mLoginInputLayout.setErrorEnabled(true);
        mLoginInputLayout.setError(getString(messageId));
    }

    @Override
    public void authorizationEmptyPasswordException(int messageId) {
        mPasswordInputLayout.setErrorEnabled(true);
        mPasswordInputLayout.setError(getString(messageId));
    }

    @Override
    public void authorizationIncorrectLoginException(int messageId) {
        mLoginInputLayout.setErrorEnabled(true);
        mLoginInputLayout.setError(getString(messageId));
    }

    @Override
    public void authorizationIncorrectPasswordException(int messageId) {
        mPasswordInputLayout.setErrorEnabled(true);
        mPasswordInputLayout.setError(getString(messageId));
    }

    @Override
    public void showFailDialog(int messageId) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(messageId),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), ERROR_OPERATION);
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void startRegistrationAfterService(String provider) {
        mPresenter.startRegistration(getActivity().getSupportFragmentManager(), provider);
    }

    @Override
    public void openMainPage(UserBean bean) {
        mPresenter.startOpenMainPage(getContext(), bean);
    }

    public void checkVkData(String userId, String accessToken, String email) {
        mPresenter.checkVK(getActivity(),
                userId,
                accessToken,
                email,
                mNetworkUtils);
    }
}
