package ru.music_boom_app.views.authorization;

import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import ru.music_boom_app.models.beans.UserBean;

/**
 * @author Markin Andrey on 05.01.2018.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface AuthorizationFragmentView extends MvpView {
    void authorizationEmptyLoginException(@StringRes int messageId);
    void authorizationEmptyPasswordException(@StringRes int messageId);
    void authorizationIncorrectLoginException(@StringRes int messageId);
    void authorizationIncorrectPasswordException(@StringRes int messageId);
    void showFailDialog(@StringRes int messageId);
    void showProgress();
    void hideProgress();
    void startRegistrationAfterService(String provider);
    void openMainPage(UserBean bean);
}
