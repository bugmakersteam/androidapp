package ru.music_boom_app.views.authorization;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;

import com.arellomobile.mvp.InjectViewState;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import net.rimoto.intlphoneinput.IntlPhoneInput;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.models.beans.UserListenerBean;
import ru.music_boom_app.models.parsers.AuthorizationResponseUserArtistBeanParser;
import ru.music_boom_app.models.parsers.AuthorizationResponseUserListenerBeanParser;
import ru.music_boom_app.models.parsers.BaseResponseParser;
import ru.music_boom_app.models.parsers.FbDataResponseParser;
import ru.music_boom_app.models.parsers.VkDataResponseParser;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.PermissionsUtils;
import ru.music_boom_app.utils.base_requests.BasePostRequest;
import ru.music_boom_app.utils.request_observer.states.StateRequest;
import ru.music_boom_app.views.authorization.FbAuth.FbCheckRequest;
import ru.music_boom_app.views.authorization.GoogleAuth.GoogleCheckRequest;
import ru.music_boom_app.views.authorization.VkAuth.VkCheckRequest;
import ru.music_boom_app.views.authorization.VkAuth.VkGetUserDataRequest;
import ru.music_boom_app.views.features.QrCodeScannerActivity;
import ru.music_boom_app.views.features.WebViewActivity;
import ru.music_boom_app.views.main_page.MainPageActivity;
import ru.music_boom_app.views.registration.step1.RegistrationFragmentStep_1;

/**
 * @author Markin Andrey on 05.01.2018.
 */
@InjectViewState
public class AuthorizationFragmentPresenter extends BasePresenter<AuthorizationFragmentView> {
    public static final int QR_CODE_SCANNER_PERMISSION_REQUEST = 103;

    private static final String AUTHORIZATION_REQUEST_ID = "authorization";
    private static final String CHECK_VK_REQUEST_ID = "check_vk";
    private static final String CHECK_Fb_REQUEST_ID = "check_Fb";
    private static final String CHECK_GOOGLE_REQUEST_ID = "check_Google";
    private static final String CHECK_Fb_RESULT_ID = "check_result_Fb";
    private static final int MINIMAL_PASSWORD_SIZE = 6;
    private static final String URL_VK_GET_USER_DATA = "https://api.vk.com/";
    private static final String VK_GET_USER_DATA_ID = "vk_get_user_data";
    private static final String USER_BEAN = "userBean";


    private RegistrationFragmentStep_1 mRegistrationFragmentStep1;
    private String mAuthUserServiceId;
    private String mAuthUserEmail;
    private String mAuthUserName;
    private String mAuthUserSurname;
    private String mAuthUserBirthday;
    private String mAuthUserCountry;
    private String mAuthUserCity;
    private String mFacebookAuthToken;

    private String mRegisteredId;

    @Override
    public void setResponse(String response, String requestId) {

        if (CHECK_VK_REQUEST_ID.equals(requestId)) {
            if (BaseResponseParser.checkSuccessResponse(response)) {
                mRegisteredId = BaseResponseParser.checkRegisteredIdResponse(response);
                if (BaseResponseParser.checkRegisteredFieldResponse(response)) {
                    switch (BaseResponseParser.checkUserTypeResponse(response)) {
                        case BaseResponseParser.ARTIST_TYPE:
                            UserArtistBean userArtistBean = AuthorizationResponseUserArtistBeanParser.parseUserArtist(response);
                            getViewState().openMainPage(userArtistBean);
                            break;
                        case BaseResponseParser.LISTENER_TYPE:
                            UserListenerBean userListenerBean = AuthorizationResponseUserListenerBeanParser.paresUserListener(response);
                            getViewState().openMainPage(userListenerBean);
                            break;
                    }
                } else {
                    //ToDo проверка наличия сети
                    sendGetUserDataVkRequest();
                }
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }

        if (VK_GET_USER_DATA_ID.equals(requestId)) {
            mAuthUserName = VkDataResponseParser.getName(response);
            mAuthUserSurname = VkDataResponseParser.getSurname(response);
            mAuthUserBirthday = VkDataResponseParser.getBirthday(response);
            mAuthUserCountry = VkDataResponseParser.getCountry(response);
            mAuthUserCity = VkDataResponseParser.getCity(response);
            getViewState().startRegistrationAfterService("vk");
        }

        if (AUTHORIZATION_REQUEST_ID.equals(requestId)) {
            if (BaseResponseParser.checkSuccessResponse(response)) {
                switch (BaseResponseParser.checkUserTypeResponse(response)) {
                    case BaseResponseParser.ARTIST_TYPE:
                        UserArtistBean userArtistBean = AuthorizationResponseUserArtistBeanParser.parseUserArtist(response);
                        getViewState().openMainPage(userArtistBean);
                        break;
                    case BaseResponseParser.LISTENER_TYPE:
                        UserListenerBean userListenerBean = AuthorizationResponseUserListenerBeanParser.paresUserListener(response);
                        getViewState().openMainPage(userListenerBean);
                        break;
                }
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }

        if (CHECK_Fb_REQUEST_ID.equals(requestId)) {
            mAuthUserServiceId = FbDataResponseParser.getId(response);
            mAuthUserName = FbDataResponseParser.getName(response);
            mAuthUserSurname = FbDataResponseParser.getSurname(response);
            mAuthUserBirthday = FbDataResponseParser.getBirthday(response);
            mAuthUserCountry = FbDataResponseParser.getCountry(response);
            mAuthUserCity = FbDataResponseParser.getCity(response);
            mAuthUserEmail = FbDataResponseParser.getEmail(response);

            //ToDo проверка наличия сети
            sendCheckFbRequest(mAuthUserServiceId);
        }

        if (CHECK_Fb_RESULT_ID.equals(requestId)) {
            if (BaseResponseParser.checkSuccessResponse(response)) {
                mRegisteredId = BaseResponseParser.checkRegisteredIdResponse(response);
                if (BaseResponseParser.checkRegisteredFieldResponse(response)) {
                    switch (BaseResponseParser.checkUserTypeResponse(response)) {
                        case BaseResponseParser.ARTIST_TYPE:
                            UserArtistBean userArtistBean = AuthorizationResponseUserArtistBeanParser.parseUserArtist(response);
                            getViewState().openMainPage(userArtistBean);
                            break;
                        case BaseResponseParser.LISTENER_TYPE:
                            UserListenerBean userListenerBean = AuthorizationResponseUserListenerBeanParser.paresUserListener(response);
                            getViewState().openMainPage(userListenerBean);
                            break;
                    }
                } else {
                    getViewState().startRegistrationAfterService("fb");
                }
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }

        if (CHECK_GOOGLE_REQUEST_ID.equals(requestId)) {
            if (BaseResponseParser.checkSuccessResponse(response)) {
                mRegisteredId = BaseResponseParser.checkRegisteredIdResponse(response);
                if (BaseResponseParser.checkRegisteredFieldResponse(response)) {
                    switch (BaseResponseParser.checkUserTypeResponse(response)) {
                        case BaseResponseParser.ARTIST_TYPE:
                            UserArtistBean userArtistBean = AuthorizationResponseUserArtistBeanParser.parseUserArtist(response);
                            getViewState().openMainPage(userArtistBean);
                            break;
                        case BaseResponseParser.LISTENER_TYPE:
                            UserListenerBean userListenerBean = AuthorizationResponseUserListenerBeanParser.paresUserListener(response);
                            getViewState().openMainPage(userListenerBean);
                            break;
                    }
                } else {
                    getViewState().startRegistrationAfterService("google");
                }
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }
    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        getViewState().showFailDialog(dialogErrorMessageId);
    }

    @Override
    public void showProgressBar() {
        getViewState().showProgress();
    }

    @Override
    public void hideProgressBar() {
        getViewState().hideProgress();
    }

    public View.OnClickListener registrationButtonHandler(android.support.v4.app.FragmentManager fragmentManager) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRegistrationFragmentStep1 == null) {
                    mRegistrationFragmentStep1 = RegistrationFragmentStep_1.newInstance();
                }
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.fragment_container, mRegistrationFragmentStep1)
                        .addToBackStack(null)
                        .commit();
            }
        };
        return listener;
    }

    public void getAuthorization(Activity activity, IntlPhoneInput login, String password, NetworkUtils networkUtils) {
        if (TextUtils.isEmpty(login.getText())) {
            getViewState().authorizationEmptyLoginException(R.string.authorization_enter_phone_number);
            return;
        }
        if (!login.isValid()) {
            getViewState().authorizationIncorrectLoginException(R.string.registration_invalid_phone_number);
            return;
        }
        if (TextUtils.isEmpty(password)) {
            getViewState().authorizationEmptyPasswordException(R.string.authorization_enter_password_number);
            return;
        }
        if (password.length() < MINIMAL_PASSWORD_SIZE) {
            getViewState().authorizationIncorrectPasswordException(R.string.authorization_short_password);
            return;
        }

        if (networkUtils.checkConnection(activity)) {
            sendAuthorizationRequest(login.getNumber(), password);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public void checkVK(FragmentActivity activity, String userId, String accessToken, String email, NetworkUtils networkUtils) {
        mAuthUserServiceId = userId;
        mAuthUserEmail = email;

        if (networkUtils.checkConnection(activity)) {
            sendCheckVkRequest(userId, accessToken);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public void checkFb(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken,
                (object, response) -> setResponse(object.toString(), CHECK_Fb_REQUEST_ID));
        mFacebookAuthToken = accessToken.getToken();
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,address");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public void startRegistration(FragmentManager fragmentManager, String provider) {
        if (mRegistrationFragmentStep1 == null) {
            mRegistrationFragmentStep1 = RegistrationFragmentStep_1.newInstance(mAuthUserEmail,
                    mAuthUserName,
                    mAuthUserSurname,
                    mAuthUserBirthday,
                    mAuthUserCountry,
                    mAuthUserCity,
                    provider,
                    mRegisteredId);
        }
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, mRegistrationFragmentStep1)
                .addToBackStack(null)
                .commit();
    }

    public void startOpenMainPage(Context context, UserBean bean) {
        Intent intent = new Intent(context, MainPageActivity.class);
        intent.putExtra(USER_BEAN, bean);
        context.startActivity(intent);
    }

    public void sendCheckFbRequest(String FbId) {
        FbCheckRequest request = new FbCheckRequest(this, R.string.request_fail_message);
        request.addObserver((observable, o) -> {
            BasePostRequest basePostRequest = (BasePostRequest) observable;
            StateRequest stateRequest = basePostRequest.getStateRequest();
            stateRequest.shareData();
        });
        Map<String, String> params = new HashMap<>();
        params.put("token", mFacebookAuthToken);
        params.put("social_id", FbId);
        params.put("provider", "fb");
        request.createAndSendRequest(BuildConfig.URL, params, CHECK_Fb_RESULT_ID);
    }


    public void startQR_codeScanner(FragmentActivity activity, AuthorizationFragment authorizationFragment, PermissionsUtils permissionsUtils) {
        if (permissionsUtils.checkPermission(activity, Manifest.permission.CAMERA)) {
            Intent intent = new Intent(activity, QrCodeScannerActivity.class);
            authorizationFragment.startActivityForResult(intent, AuthorizationFragment.QR_SCANNER_REQUEST_CODE);
        } else {
            permissionsUtils.requestPermission(activity, Manifest.permission.CAMERA, QR_CODE_SCANNER_PERMISSION_REQUEST);
        }
    }

    public void showWebPage(FragmentActivity activity,
                            String displayValue,
                            NetworkUtils networkUtils) {

        if (networkUtils.checkConnection(activity)) {
            Intent intent = new Intent(activity, WebViewActivity.class);
            intent.putExtra(AuthorizationFragment.URL_WEB_VIEW_EXTRA, displayValue);
            activity.startActivity(intent);
        }
    }

    public void startGoogleAuth(Context context, AuthorizationFragment authorizationFragment, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)) {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .requestIdToken("623567506856-ig59ljhs5ck9a0kfo5irteg8fpfk7vfa.apps.googleusercontent.com")
                    .build();
            GoogleSignInClient client = GoogleSignIn.getClient(context, gso);
            Intent signInIntent = client.getSignInIntent();
            authorizationFragment.startActivityForResult(signInIntent, AuthorizationFragment.GOOGLE_AUTH_REQUEST_CODE);
        }
    }

    public void checkGoogle(GoogleSignInAccount account) {
        mAuthUserServiceId = account.getId();
        mAuthUserEmail = account.getEmail();
        mAuthUserName = account.getGivenName();
        mAuthUserSurname = account.getFamilyName();
        mAuthUserCity = "";
        mAuthUserCountry = "";
        mAuthUserBirthday = "";

        GoogleCheckRequest request = new GoogleCheckRequest(this, R.string.request_fail_message);
        request.addObserver((o, arg) -> {
            GoogleCheckRequest basePostRequest = (GoogleCheckRequest) o;
            StateRequest stateRequest = basePostRequest.getStateRequest();
            stateRequest.shareData();
        });
        Map<String, String> params = new HashMap<>();
        params.put("token", account.getIdToken());
        params.put("social_id", mAuthUserServiceId);
        params.put("provider", "google");

        request.createAndSendRequest(BuildConfig.URL, params, CHECK_GOOGLE_REQUEST_ID);
    }

    private void sendAuthorizationRequest(String login, String password) {
        AuthorizationRequest request = new AuthorizationRequest(this, R.string.request_fail_message);
        request.addObserver((observable, o) -> {
            AuthorizationRequest basePostRequest = (AuthorizationRequest) observable;
            StateRequest stateRequest = basePostRequest.getStateRequest();
            stateRequest.shareData();
        });

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("login", login);
            jsonObject.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = createRequestBodyText("registrationJson", jsonObject.toString());
        request.createAndSendRequest(BuildConfig.URL, requestBody, AUTHORIZATION_REQUEST_ID);
    }

    private RequestBody createRequestBodyText(String key, String value) {
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), value);
    }

    private void sendCheckVkRequest(String userId, String accessToken) {
        VkCheckRequest request = new VkCheckRequest(this, R.string.request_fail_message);
        request.addObserver((observable, o) -> {
            VkCheckRequest vkCheckRequest = (VkCheckRequest) observable;
            StateRequest stateRequest = vkCheckRequest.getStateRequest();
            stateRequest.shareData();
        });
        Map<String, String> params = new HashMap<>();
        params.put("token", accessToken);
        params.put("social_id", userId);
        params.put("provider", "vk");
        request.createAndSendRequest(BuildConfig.URL, params, CHECK_VK_REQUEST_ID);
    }

    private void sendGetUserDataVkRequest() {
        VkGetUserDataRequest request = new VkGetUserDataRequest(this, R.string.request_fail_message);
        request.addObserver((observable, o) -> {
            VkGetUserDataRequest basePostRequest = (VkGetUserDataRequest) observable;
            StateRequest stateRequest = basePostRequest.getStateRequest();
            stateRequest.shareData();
        });
        Map<String, String> params = new HashMap<>();
        params.put("user_ids", mAuthUserServiceId);
        params.put("fields", "bdate,city,country");
        params.put("v", "5.76");
        request.createAndSendRequest(URL_VK_GET_USER_DATA, params, VK_GET_USER_DATA_ID);
    }
}

