package ru.music_boom_app.views.main_page.profile.artist_profile.creativity;

import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Markin Andrey on 22.04.2018.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface ArtistCreativityView extends MvpView {
    void showProgress();
    void hideProgress();
    void showFailDialog(int dialogErrorMessageId);
    void onSuccessOperation(int dialogSuccessMessageID);
    void showInstrumentLayoutException(@StringRes int stringExceptionId);
    void showGenreLayoutException (@StringRes int stringExceptionId);
}
