package ru.music_boom_app.views.main_page.profile.listener_profile.create_avatar_dialog;

import com.arellomobile.mvp.InjectViewState;

import java.io.File;

import ru.music_boom_app.presenters.BasePresenter;

/**
 * @author Markin Andrey on 30.03.2018.
 */
@InjectViewState
public class CreateAvatarDialogPresenter extends BasePresenter<CreateAvatarDialogView> {


    @Override
    public void setResponse(String response, String requestId) {

    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {

    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }
}
