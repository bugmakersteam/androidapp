package ru.music_boom_app.views.main_page.profile.listener_profile;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserListenerBean;
import ru.music_boom_app.models.parsers.BaseResponseParser;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.PermissionsUtils;
import ru.music_boom_app.utils.request_observer.states.StateRequest;
import ru.music_boom_app.views.main_page.profile.listener_profile.change_password_dialog.ChangePasswordDialogFragment;
import ru.music_boom_app.views.main_page.profile.listener_profile.create_avatar_dialog.CreateAvatarDialogFragment;

/**
 * Created by markin_a on 22.03.2018.
 */
@InjectViewState
public class ListenerProfilePresenter extends BasePresenter<ListenerProfileView> {
    public static final int CHOICE_IMAGE_PERMISSION = 105;
    public static final int CREATE_PHOTO_PERMISSION = 106;
    public static final int CHOOSE_IMAGE_ACTIVITY_RESULT = 33;
    public static final int CREATE_PHOTO_ACTIVITY_RESULT = 44;
    private final static int TARGET_FRAGMENT_REQUEST = 10;
    private final static String AVATAR_REQUEST_ID = "avatar_request_id";
    private final static String AVATAR_IMAGE_NAME = "avatar.jpg";
    private final static String CREATE_AVATAR_DIALOG = "create_avatar_dialog";
    private final static String CHANGE_AVATAR_DIALOG = "change_avatar_dialog";
    private final static String UPLOAD_AVATAR_REQUEST_ID = "upload_avatar_request_id";
    private final static String UPDATE_PERSONAL_FIELDS_REQUEST_ID = "update_personal_fields_request_id";

    private boolean mIsAvatarDownloaded = false;
    private String mPhotoAvatarAbsolutePath;

    @Override
    public void setResponse(String response, String requestId) {
        if (UPLOAD_AVATAR_REQUEST_ID.equals(requestId)) {
            if (BaseResponseParser.checkSuccessResponse(response)) {
                getViewState().onSuccessAvatarUploaded();
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }

        if (UPDATE_PERSONAL_FIELDS_REQUEST_ID.equals(requestId)) {
            if (BaseResponseParser.checkSuccessResponse(response)) {
                getViewState().onSuccessPersonalFieldsUpdated();
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }
    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        getViewState().showFailDialog(dialogErrorMessageId);
    }

    @Override
    public void showProgressBar() {
        getViewState().showProgress();
    }

    @Override
    public void hideProgressBar() {
        getViewState().hideProgress();
    }

    public void downloadAvatar(Context context, String url, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)) {
            Target target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    getViewState().onDownloadedAvatar(bitmap);
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };
            if (!TextUtils.isEmpty(url)) {
                Picasso.get().load(url).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(target);
            }
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public void setAvatarDownloaded(boolean isDownloaded) {
        mIsAvatarDownloaded = isDownloaded;
    }

    public boolean isAvatarDownloaded() {
        return mIsAvatarDownloaded;
    }

    public void createNewAvatar(FragmentActivity activity, ListenerProfileFragment fragment) {
        CreateAvatarDialogFragment dialogFragment = new CreateAvatarDialogFragment();
        dialogFragment.setTargetFragment(fragment, TARGET_FRAGMENT_REQUEST);
        dialogFragment.show(activity.getSupportFragmentManager(), CREATE_AVATAR_DIALOG);
    }

    public void choiceImage(FragmentActivity activity, ListenerProfileFragment fragment, PermissionsUtils permissionsUtils) {
        if (permissionsUtils.checkPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            Intent chooserIntent = Intent.createChooser(intent, "Select Image");
            fragment.startActivityForResult(chooserIntent, CHOOSE_IMAGE_ACTIVITY_RESULT);
        } else {
            permissionsUtils.requestPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE, CHOICE_IMAGE_PERMISSION);
        }
    }

    public void createPhoto(FragmentActivity activity, Fragment targetFragment, PermissionsUtils permissionsUtils) {
        if (permissionsUtils.checkPermissions(activity, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photoFile = null;

            try {
                photoFile = createImageFile(activity);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(activity,
                        "ru.music_boom_app.android.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                mPhotoAvatarAbsolutePath = photoFile.getAbsolutePath();
            }
            targetFragment.startActivityForResult(intent, CREATE_PHOTO_ACTIVITY_RESULT);
        } else {
            permissionsUtils.requestPermissions(activity, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CREATE_PHOTO_PERMISSION);
        }
    }

    public String getPhotoAvatarAbsolutePath() {
        return mPhotoAvatarAbsolutePath;
    }

    private File createImageFile(FragmentActivity activity) throws IOException {
        String imageFileName = "MusicBoom_avatar";
        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    public void uploadAvatarToServer(Context context, String newAvatarPath, String sessionId, NetworkUtils networkUtils) {
        AvatarUploadRequest avatarUploadRequest = new AvatarUploadRequest(this, R.string.request_fail_message);
        avatarUploadRequest.addObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                AvatarUploadRequest request = (AvatarUploadRequest) o;
                StateRequest state = request.getStateRequest();
                state.shareData();
            }
        });
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sessionId", sessionId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Uri uri = Uri.parse(newAvatarPath);

        String type = "jpeg";
        File avatarFile = new File(newAvatarPath);

        if (networkUtils.checkConnection(context)) {
            RequestBody requestBody = createRequestBodyText(jsonObject.toString());
            avatarUploadRequest.uploadFile(BuildConfig.URL, requestBody, avatarFile, type, UPLOAD_AVATAR_REQUEST_ID);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public void editFields() {
        getViewState().onEditFields();
    }

    public void saveFields() {
        getViewState().onSaveFields();
    }

    public boolean checkCountryText(String s) {
        if (checkText(s)) {
            getViewState().showCountryLayoutException(R.string.registration_invalid_country);
            return false;
        } else {
            return true;
        }
    }

    public boolean checkCityText(String s) {
        if (checkText(s)) {
            getViewState().showCityLayoutException(R.string.registration_invalid_city);
            return false;
        } else {
            return true;
        }
    }

    public boolean checkEmailField(String emailText) {
        if (!checkEmailText(emailText)) {
            getViewState().showEmailLayoutException(R.string.registration_invalid_email);
            return false;
        } else {
            return true;
        }
    }

    private boolean checkEmailText(String emailText) {
        String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(emailText);
        return matcher.matches();
    }

    private boolean checkText(String text) {
        return TextUtils.isEmpty(text);
    }

    public void updatePersonal(Context context, String sessionId, String email, String country, String city, NetworkUtils networkUtils) {
        if (!checkCountryText(country)) {
            return;
        }

        if (!checkCityText(city)) {
            return;
        }

//        if (!checkEmailField(email)) {
//            return;
//        }

        if (networkUtils.checkConnection(context)) {
            UpdateListenerPersonalFieldsRequest fieldsRequest = new UpdateListenerPersonalFieldsRequest(this, R.string.request_fail_message);
            fieldsRequest.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    UpdateListenerPersonalFieldsRequest request = (UpdateListenerPersonalFieldsRequest) o;
                    StateRequest state = request.getStateRequest();
                    state.shareData();
                }
            });

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", sessionId);
                jsonObject.put("country", country);
                jsonObject.put("city", city);
                jsonObject.put("email", email);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = createRequestBodyText(jsonObject.toString());
            fieldsRequest.createAndSendRequest(BuildConfig.URL, requestBody, UPDATE_PERSONAL_FIELDS_REQUEST_ID);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    private RequestBody createRequestBodyText(String value) {
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), value);
    }

    public void showChangePasswordDialog(FragmentActivity activity, UserListenerBean bean) {
        ChangePasswordDialogFragment dialogFragment = ChangePasswordDialogFragment.newInstance(bean);
        dialogFragment.show(activity.getSupportFragmentManager(), CHANGE_AVATAR_DIALOG);
    }
}
