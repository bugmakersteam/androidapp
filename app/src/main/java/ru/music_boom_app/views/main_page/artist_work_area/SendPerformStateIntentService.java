package ru.music_boom_app.views.main_page.artist_work_area;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.PermissionsUtils;

/**
 * @author Markin Andrey on 29.04.2018.
 */
public class SendPerformStateIntentService extends IntentService {
    private static final String USER_BEAN = "user_bean";

    private GoogleApiClient mApiClient;
    private LocationListener mLocationListener;
    private boolean mIsFirstRequest = true;
    private UserArtistBean mArtistBean;
    private Handler mSendPerformStateHandler;
    private Runnable mRunnable;

    @Inject
    PermissionsUtils mPermissionsUtils;
    @Inject
    NetworkUtils mNetworkUtils;


    public SendPerformStateIntentService() {
        super("sendPerformState");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MusicBoomApplication.getComponent(getApplicationContext()).inject(this);
        mApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addApi(LocationServices.API)
                .build();
        mApiClient.connect();
        Log.d("perform", "onCreate");
    }



    @SuppressLint("MissingPermission")
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        mArtistBean = (UserArtistBean) intent.getParcelableExtra(USER_BEAN);
        mSendPerformStateHandler = new Handler();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30000);
        if (mApiClient != null) {
            if (mPermissionsUtils.checkPermissions(getApplicationContext(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})) {
                if (mApiClient.isConnected()) {
                    LocationServices.FusedLocationApi.requestLocationUpdates(
                            mApiClient, locationRequest, new LocationListener() {
                                @Override
                                public void onLocationChanged(Location location) {
                                    if (mIsFirstRequest) {
                                        Log.d("perform", "first");
                                        startPerformanceRequest(getApplicationContext(),
                                                mArtistBean,
                                                location.getLongitude(),
                                                location.getLatitude(),
                                                mNetworkUtils
                                        );
                                        mIsFirstRequest = false;
                                    } else {
                                        Log.d("perform", "update");
                                        updatePerformanceRequest(getApplicationContext(),
                                                mArtistBean,
                                                location.getLongitude(),
                                                location.getLatitude(),
                                                mNetworkUtils
                                        );
                                    }
                                    mLocationListener = this;
                                }
                            }
                    );
                }
            } else {

            }
        }
    }

    public void startPerformanceRequest(Context context, UserArtistBean artistBean, double longitude, double latitude, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)) {

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .connectTimeout(2, TimeUnit.MINUTES)
                    .readTimeout(2, TimeUnit.MINUTES)
                    .writeTimeout(2, TimeUnit.MINUTES)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.URL)
                    .client(client)
                    .build();

            IStartPerformancePostRequest request = retrofit.create(IStartPerformancePostRequest.class);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", artistBean.getSessionId());
                jsonObject.put("longitude", longitude);
                jsonObject.put("latitude", latitude);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = createRequestBodyText("jsonBody", jsonObject.toString());
            request.basePostRequest(requestBody).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        } else {

        }
    }


    public void updatePerformanceRequest(Context context, UserArtistBean artistBean, double longitude, double latitude, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)) {

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .connectTimeout(2, TimeUnit.MINUTES)
                    .readTimeout(2, TimeUnit.MINUTES)
                    .writeTimeout(2, TimeUnit.MINUTES)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.URL)
                    .client(client)
                    .build();

            IUpdatePerformancePostRequest request = retrofit.create(IUpdatePerformancePostRequest.class);

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", artistBean.getSessionId());
                jsonObject.put("longitude", longitude);
                jsonObject.put("latitude", latitude);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = createRequestBodyText("jsonBody", jsonObject.toString());
            request.basePostRequest(requestBody).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });

        } else {

        }
    }

    private RequestBody createRequestBodyText(String key, String value) {
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), value);
    }
}
