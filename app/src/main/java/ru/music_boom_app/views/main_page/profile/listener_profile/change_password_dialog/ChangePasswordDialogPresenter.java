package ru.music_boom_app.views.main_page.profile.listener_profile.change_password_dialog;

import android.content.Context;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Observable;
import java.util.Observer;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.R;
import ru.music_boom_app.models.parsers.BaseResponseParser;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.request_observer.states.StateRequest;

/**
 * @author Markin Andrey on 04.04.2018.
 */
@InjectViewState
public class ChangePasswordDialogPresenter extends BasePresenter<ChangePasswordDialogView> {
    private static final int MINIMAL_PASSWORD_SIZE = 6;
    private static final String CHANGE_PASSWORD_REQUEST_ID = "change_password_request_id";

    @Override
    public void setResponse(String response, String requestId) {
        if (CHANGE_PASSWORD_REQUEST_ID.equals(requestId)){
            if (BaseResponseParser.checkSuccessResponse(response)){
                getViewState().onSuccessChangePassword();
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }
    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        getViewState().showFailDialog(dialogErrorMessageId);
    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

    public void checkCurrentPasswordField(String passwordText) {
        if (!checkPasswordText(passwordText)) {
            getViewState().showCurrentPasswordLayoutException(R.string.registration_invalid_password);
        }
    }

    public void checkNewPasswordField(String passwordText) {
        if (!checkPasswordText(passwordText)) {
            getViewState().showNewPasswordLayoutException(R.string.registration_invalid_password);
        }
    }

    public void checkConfirmedNewPasswordField(String newConfirmPassword, String newPassword) {
        if (!checkConfirmPasswordText(newConfirmPassword, newPassword)) {
            getViewState().showConfirmedNewPasswordLayoutException(R.string.registration_invalid_confirm_password);
        }
    }


    private boolean checkPasswordText(String passwordText) {
        if (passwordText.length() < MINIMAL_PASSWORD_SIZE) {
            return false;
        }
        return true;
    }

    private boolean checkConfirmPasswordText(String confirmPasswordText, String passwordText) {
        if (TextUtils.isEmpty(passwordText) ||
                !passwordText.equals(confirmPasswordText)) {
            return false;
        }
        return true;
    }

    public void changePassword(Context context,
                               String sessionId,
                               String currentPassword,
                               String newPassword,
                               String newConfirmPassword,
                               NetworkUtils networkUtils) {
        if (!checkPasswordText(currentPassword)) {
            getViewState().showCurrentPasswordLayoutException(R.string.registration_invalid_password);
            return;
        }

        if (!checkPasswordText(newConfirmPassword)) {
            getViewState().showNewPasswordLayoutException(R.string.registration_invalid_password);
            return;
        }

        if (!checkConfirmPasswordText(newConfirmPassword, newPassword)) {
            getViewState().showConfirmedNewPasswordLayoutException(R.string.registration_invalid_confirm_password);
            return;
        }

        if (networkUtils.checkConnection(context)) {
            ChangePasswordListenerPostRequest request = new ChangePasswordListenerPostRequest(this, R.string.request_fail_message);
            request.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    ChangePasswordListenerPostRequest requestObject = (ChangePasswordListenerPostRequest) o;
                    StateRequest stateRequest = requestObject.getStateRequest();
                    stateRequest.shareData();
                }
            });

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", sessionId);
                jsonObject.put("old_password", currentPassword);
                jsonObject.put("new_password", newPassword);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            RequestBody requestBody = createRequestBodyText("registrationJson", jsonObject.toString());
            request.createAndSendRequest(BuildConfig.URL, requestBody, CHANGE_PASSWORD_REQUEST_ID);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    private RequestBody createRequestBodyText(String key, String value) {
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), value);
    }
}
