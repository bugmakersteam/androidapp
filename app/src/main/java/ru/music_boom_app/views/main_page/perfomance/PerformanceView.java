package ru.music_boom_app.views.main_page.perfomance;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.concurrent.CopyOnWriteArrayList;

import ru.music_boom_app.models.beans.ActiveArtistMapBean;

/**
 * @author Markin Andrey on 08.04.2018.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface PerformanceView extends MvpView {
    void onCloseFragment();

    void onRequestLocationUpdates();

    void onChangeMyPositionOnMap();

    void onGoogleClientConnect();

    void onUpdateMarkersPosition(CopyOnWriteArrayList<ActiveArtistMapBean> activeArtistMapBeans);

    void onDeleteMarkersPosition(CopyOnWriteArrayList<ActiveArtistMapBean> notActiveArtistMapBeans);

    void onDownloadIcons(ActiveArtistMapBean bean);
}
