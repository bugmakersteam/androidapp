package ru.music_boom_app.views.main_page.profile.artist_profile.creativity;

import android.content.Context;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Observable;
import java.util.Observer;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.R;
import ru.music_boom_app.models.parsers.BaseResponseParser;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.request_observer.states.StateRequest;

/**
 * @author Markin Andrey on 22.04.2018.
 */
@InjectViewState
public class ArtistCreativityPresenter extends BasePresenter<ArtistCreativityView> {
    private static final String CHANGE_CREATIVITY_REQUEST_ID = "change_creativity_request_id";
    private static final String CHANGE_INSTRUMENT_REQUEST_ID = "change_instrument_request_id";
    private static final String CHANGE_GENRE_REQUEST_ID = "change_genre_request_id";
    private static final String CHANGE_ORDERED_REQUEST_ID = "change_ordered_request_id";

    @Override
    public void setResponse(String response, String requestId) {
        if (CHANGE_CREATIVITY_REQUEST_ID.equals(requestId)){
            if (BaseResponseParser.checkSuccessResponse(response)) {
                getViewState().onSuccessOperation(R.string.change_creativity_success);
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }

        if (CHANGE_INSTRUMENT_REQUEST_ID.equals(requestId)){
            if (BaseResponseParser.checkSuccessResponse(response)) {
                getViewState().onSuccessOperation(R.string.change_instrument_success);
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }

        if (CHANGE_GENRE_REQUEST_ID.equals(requestId)){
            if (BaseResponseParser.checkSuccessResponse(response)) {
                getViewState().onSuccessOperation(R.string.change_genre_success);
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }

        if (CHANGE_ORDERED_REQUEST_ID.equals(requestId)){
            if (BaseResponseParser.checkSuccessResponse(response)) {
                getViewState().onSuccessOperation(R.string.change_ordered_success);
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }
    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        getViewState().showFailDialog(dialogErrorMessageId);
    }

    @Override
    public void showProgressBar() {
        getViewState().showProgress();
    }

    @Override
    public void hideProgressBar() {
        getViewState().hideProgress();
    }

    public void saveCreativity(Context context, String sessionId, String result, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)) {
            CreativityChangePostRequest postRequest = new CreativityChangePostRequest(this,  R.string.request_fail_message);
            postRequest.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    CreativityChangePostRequest request = (CreativityChangePostRequest) o;
                    StateRequest stateRequest = request.getStateRequest();
                    stateRequest.shareData();
                }
            });

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", sessionId);
                jsonObject.put("creativity", result);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = createRequestBodyText("jsonBody", jsonObject.toString());
            postRequest.createAndSendRequest(BuildConfig.URL, requestBody, CHANGE_CREATIVITY_REQUEST_ID);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public void saveInstrument(Context context, String sessionId, String result, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)) {

            if (!checkInstrumentText(result)){
                return;
            }
            ChangeInstrumentPostRequest postRequest = new ChangeInstrumentPostRequest(this,  R.string.request_fail_message);
            postRequest.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    ChangeInstrumentPostRequest request = (ChangeInstrumentPostRequest) o;
                    StateRequest stateRequest = request.getStateRequest();
                    stateRequest.shareData();
                }
            });

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", sessionId);
                jsonObject.put("instrument", result);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = createRequestBodyText("jsonBody", jsonObject.toString());
            postRequest.createAndSendRequest(BuildConfig.URL, requestBody, CHANGE_INSTRUMENT_REQUEST_ID);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public void saveGenre(Context context, String sessionId, String result, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)) {

            if (!checkGenreText(result)){
                return;
            }
            ChangeGenrePostRequest postRequest = new ChangeGenrePostRequest(this,  R.string.request_fail_message);
            postRequest.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    ChangeGenrePostRequest request = (ChangeGenrePostRequest) o;
                    StateRequest stateRequest = request.getStateRequest();
                    stateRequest.shareData();
                }
            });

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", sessionId);
                jsonObject.put("genre", result);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = createRequestBodyText("jsonBody", jsonObject.toString());
            postRequest.createAndSendRequest(BuildConfig.URL, requestBody, CHANGE_GENRE_REQUEST_ID);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public void saveOrdered(Context context, String sessionId, boolean resultBool, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)) {
            ChangeOrderedPostRequest postRequest = new ChangeOrderedPostRequest(this,  R.string.request_fail_message);
            postRequest.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    ChangeOrderedPostRequest request = (ChangeOrderedPostRequest) o;
                    StateRequest stateRequest = request.getStateRequest();
                    stateRequest.shareData();
                }
            });

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", sessionId);
                jsonObject.put("orderable", resultBool);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = createRequestBodyText("jsonBody", jsonObject.toString());
            postRequest.createAndSendRequest(BuildConfig.URL, requestBody, CHANGE_ORDERED_REQUEST_ID);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public boolean checkInstrumentText(String s) {
        if (checkText(s)) {
            getViewState().showInstrumentLayoutException(R.string.registration_invalid_musical_instrument);
            return false;
        } else {
            return true;
        }
    }

    public boolean checkGenreText(String s) {
        if (checkText(s)) {
            getViewState().showGenreLayoutException(R.string.registration_invalid_genre);
            return false;
        } else {
            return true;
        }
    }

    private boolean checkText(String text) {
        return TextUtils.isEmpty(text);
    }

    private RequestBody createRequestBodyText(String key, String value) {
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), value);
    }
}
