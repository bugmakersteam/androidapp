package ru.music_boom_app.views.main_page.drawer_menu.feedback;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Markin Andrey on 18.04.2018.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface FeedbackView extends MvpView {
    void showFailDialog(int dialogErrorMessageId);
    void onSuccessSentFeedback();
}
