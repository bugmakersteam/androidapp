package ru.music_boom_app.views.main_page.finance.artist_finance.withdraw_money_dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.math.BigDecimal;

import javax.inject.Inject;

import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;

/**
 * @author Markin Andrey on 24.04.2018.
 */
public class WithdrawMoneyDialog extends BaseApplicationAlertDialogFragment implements WithdrawMoneyView {

    public static final String USER_ARTIST_BEAN_EXTRA = "user_listener_bean_extra";
    private static final String ERROR_OPERATION = "error_operation";
    private static final String SUCCESS_OPERATION = "success_operation";

    private UserArtistBean mArtistBean;
    private TextInputLayout mAmountLayout;
    private EditText mAmount;
    private Button mWithdrawButton;
    @Inject
    NetworkUtils mNetworkUtils;
    @InjectPresenter
    WithdrawMoneyPresenter mPresenter;

    public static WithdrawMoneyDialog newInstance(UserArtistBean bean) {
        Bundle args = new Bundle();
        args.putParcelable(USER_ARTIST_BEAN_EXTRA, bean);
        WithdrawMoneyDialog fragment = new WithdrawMoneyDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(getContext()).inject(this);
        mArtistBean = (UserArtistBean) getArguments().getParcelable(USER_ARTIST_BEAN_EXTRA);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.withdraw_money_dialog, null);
        mAmountLayout = (TextInputLayout) view.findViewById(R.id.amount_layout);
        mAmount = (EditText) view.findViewById(R.id.amount);
        mAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkWithdrawAmount(new BigDecimal(mAmount.getText().toString()));
                } else {
                    mAmountLayout.setErrorEnabled(false);
                }
            }
        });

        mWithdrawButton = (Button) view.findViewById(R.id.withdraw_button);
        mWithdrawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.withdrawMoney(
                        getContext(),
                        mAmount.getText().toString(),
                        mArtistBean,
                        mNetworkUtils
                );
            }
        });
        builder.setView(view);
        return builder.create();
    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(dialogErrorMessageId),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), ERROR_OPERATION);
    }

    @Override
    public void showAmountLayoutException(int stringExceptionId) {
        mAmountLayout.setErrorEnabled(true);
        mAmountLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void onSuccessWithdrawOperation() {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.avatar_success_uploaded_title),
                getString(R.string.withdraw_operation_success_text),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), SUCCESS_OPERATION);
        this.dismiss();
    }
}
