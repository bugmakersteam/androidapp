package ru.music_boom_app.views.main_page.donate;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Markin Andrey on 08.04.2018.
 */
public interface IDonateTransactionPostRequest {
    @POST("mapi/transaction")
    Call<ResponseBody> basePostRequest(@Body RequestBody params);
}
