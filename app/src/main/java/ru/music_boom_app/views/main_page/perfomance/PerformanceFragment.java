package ru.music_boom_app.views.main_page.perfomance;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.util.LongSparseArray;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.concurrent.CopyOnWriteArrayList;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.ActiveArtistMapBean;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.utils.PermissionsUtils;
import ru.music_boom_app.views.base.BaseApplicationFragment;

/**
 * @author Markin Andrey on 08.04.2018.
 */
public class PerformanceFragment extends BaseApplicationFragment implements PerformanceView, OnMapReadyCallback {
    public static final String SESSION = "sessionId";
    public static final String LATITUDE = "lat";
    public static final String LONGITUDE = "long";
    public static final String RESULT_BROADCAST = "result_broadcast";
    public static final String ACTIVE_ARTISTS_BROADCAST_ACTION = "ru.music_boom_app.views.main_page.perfomance.active_artists_broadcasts_action";

    private static final String USER_BEAN_EXTRA = "user_bean_extra";

    private GoogleApiClient mGoogleApiClient;
    private LocationListener mLocationListener;
    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private LocationRequest mLocationRequest;
    private Intent mActiveArtistsServiceIntent;
    private UserBean mBean;
    private BroadcastReceiver mBroadcastReceiver;
    private Target mTarget;
    private LongSparseArray<Marker> activeMarkers = new LongSparseArray<>();
    private Toolbar mToolbar;

    @Inject
    PermissionsUtils mPermissionsUtils;
    @InjectPresenter
    PerformancePresenter mPresenter;

    public static PerformanceFragment newInstance(UserBean bean) {
        Bundle args = new Bundle();
        args.putParcelable(USER_BEAN_EXTRA, bean);
        PerformanceFragment fragment = new PerformanceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(getContext()).inject(this);
        mBean = getArguments().getParcelable(USER_BEAN_EXTRA);
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String jsonRequestResult = intent.getStringExtra(RESULT_BROADCAST);
                mPresenter.checkResultFromReceiver(jsonRequestResult);
            }
        };
        IntentFilter intentFilter = new IntentFilter(ACTIVE_ARTISTS_BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver, intentFilter);
        mPresenter.startGoogleClientConnect();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.performance_fragment, container, false);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.bottom_nav_performance_title);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(mToolbar);
        ActionBar actionbar = activity.getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
        mMapView = (MapView) view.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mMapView.onStart();
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(8000);
        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mPresenter.setCurrentLocation(location);

                if (mPresenter.isFirstShowMap()) {
                    mPresenter.changeMyPositionOnMap();
                    mActiveArtistsServiceIntent = new Intent(getContext(), ActiveArtistsIntentService.class);
                    mActiveArtistsServiceIntent.putExtra(SESSION, mBean.getSessionId());
                    mActiveArtistsServiceIntent.putExtra(LATITUDE, location.getLatitude());
                    mActiveArtistsServiceIntent.putExtra(LONGITUDE, location.getLongitude());
                    getActivity().startService(mActiveArtistsServiceIntent);
                    mPresenter.setFirstShowMap(false);
                }
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        mPresenter.checkAvailablePlayServices(getActivity());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PerformancePresenter.GOOGLE_CLIENT_CONNECT_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    mPresenter.googleClientConnect(getActivity(), mPermissionsUtils);
                } else {
                    getActivity().getSupportFragmentManager().beginTransaction().remove(this);
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onStop();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, mLocationListener);
        }
        try {
            if (mActiveArtistsServiceIntent != null) {
                getActivity().stopService(mActiveArtistsServiceIntent);
            }
        } catch (NullPointerException ignored) {

        }
        mPresenter.setFirstShowMap(true);
        mPresenter.destroyActiveList();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
        if (mTarget != null) {
            Picasso.get().cancelRequest(mTarget);
        }
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
        if (mActiveArtistsServiceIntent != null) {
            getActivity().stopService(mActiveArtistsServiceIntent);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onCloseFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    @Override
    public void onRequestLocationUpdates() {
        mGoogleApiClient.connect();
        mGoogleApiClient.registerConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(@Nullable Bundle bundle) {
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,
                        mLocationListener);
            }

            @Override
            public void onConnectionSuspended(int i) {

            }
        });

        mMapView.getMapAsync(this);
    }

    @Override
    public void onChangeMyPositionOnMap() {
        mPresenter.updateMyPosition(getContext(), mGoogleMap);
    }

    @Override
    public void onGoogleClientConnect() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .build();
        mPresenter.googleClientConnect(getActivity(), mPermissionsUtils);
    }

    @Override
    public void onUpdateMarkersPosition(CopyOnWriteArrayList<ActiveArtistMapBean> activeArtistMapBeans) {
        for (ActiveArtistMapBean bean : activeArtistMapBeans) {
            Marker marker = activeMarkers.get(bean.getArtistId());
            if (marker != null) {
                marker.setPosition(new LatLng(bean.getLatitude(), bean.getLongitude()));
            }
        }
    }

    @Override
    public void onDeleteMarkersPosition(CopyOnWriteArrayList<ActiveArtistMapBean> notActiveArtistMapBeans) {
        for (ActiveArtistMapBean bean : notActiveArtistMapBeans) {
            Marker marker = activeMarkers.get(bean.getArtistId());
            if (marker != null) {
                marker.remove();
                activeMarkers.delete(bean.getArtistId());
            }
        }
    }

    @Override
    public void onDownloadIcons(ActiveArtistMapBean bean) {
        mTarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                mPresenter.addActiveBean(bean);
                if (mGoogleMap != null) {
                    View view = getLayoutInflater().inflate(R.layout.marker_layout, null);

                    Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(bean.getLatitude(), bean.getLongitude()))
                            .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(view, bitmap)))
                            .anchor(0.5f, 1)
                    );
                    marker.setTag(bean.getArtistId());
                    activeMarkers.put(bean.getArtistId(), marker);
                }
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        if (!TextUtils.isEmpty(bean.getAvatarUrl())) {
            Picasso.get().load(bean.getAvatarUrl()).resize(80, 80).into(mTarget);
        } else {
            if (mGoogleMap != null) {
                View view = getLayoutInflater().inflate(R.layout.marker_layout, null);

                Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(bean.getLatitude(), bean.getLongitude()))
                        .anchor(0.5f, 1)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))
                );
                marker.setTag(bean.getArtistId());
                activeMarkers.put(bean.getArtistId(), marker);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker.getTag() != null) {
                    Long tag = (Long) marker.getTag();
                    mPresenter.showArtistProfileById(getActivity().getSupportFragmentManager(), tag, mBean);
                }
                return true;
            }
        });
    }

    private Bitmap getMarkerBitmapFromView(View view, Bitmap bitmap) {
        CircleImageView imageMarker = (CircleImageView) view.findViewById(R.id.marker);
        imageMarker.setImageBitmap(bitmap);
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = view.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        view.draw(canvas);
        return returnedBitmap;
    }
}
