package ru.music_boom_app.views.main_page.choose_donate_amount;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Markin Andrey on 18.04.2018.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface ChooseDonateAmountDialogView extends MvpView {
}
