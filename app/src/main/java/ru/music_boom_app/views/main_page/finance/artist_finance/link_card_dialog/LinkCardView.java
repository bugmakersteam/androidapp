package ru.music_boom_app.views.main_page.finance.artist_finance.link_card_dialog;

import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Markin Andrey on 24.04.2018.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface LinkCardView extends MvpView {
    void showFailDialog(int dialogErrorMessageId);
    void showCardNumberLayoutException (@StringRes int stringExceptionId);
    void onSuccessCardChanged(String cardNumber);
}
