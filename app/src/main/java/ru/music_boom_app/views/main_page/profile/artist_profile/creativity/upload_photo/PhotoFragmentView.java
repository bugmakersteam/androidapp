package ru.music_boom_app.views.main_page.profile.artist_profile.creativity.upload_photo;

import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Markin Andrey on 22.04.2018.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface PhotoFragmentView extends MvpView {
    void onSuccessAvatarUploaded();
    void showFailDialog(@StringRes int dialogErrorMessageId);
    void showProgress();
    void hideProgress();
    void onSuccessDeletePhoto();
}
