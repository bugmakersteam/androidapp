package ru.music_boom_app.views.main_page.profile.listener_profile.change_password_dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.arellomobile.mvp.presenter.InjectPresenter;

import javax.inject.Inject;

import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;

/**
 * @author Markin Andrey on 04.04.2018.
 */
public class ChangePasswordDialogFragment extends BaseApplicationAlertDialogFragment implements ChangePasswordDialogView {
    public static final String USER_LISTENER_BEAN_EXTRA = "user_listener_bean_extra";
    private static final String ERROR_OPERATION = "error_operation";
    private static final String SUCCESS_OPERATION = "success_operation";

    private TextInputLayout mCurrenPasswordLayout;
    private EditText mCurrenPasswordEditText;
    private TextInputLayout mNewPasswordLayout;
    private EditText mNewPasswordEditText;
    private TextInputLayout mConfirmNewPasseordLayout;
    private EditText mConfirmNewPasseordEditText;
    private UserBean mBean;


    @Inject
    NetworkUtils mNetworkUtils;
    @InjectPresenter
    ChangePasswordDialogPresenter mPresenter;

    public static ChangePasswordDialogFragment newInstance(UserBean bean) {
        Bundle args = new Bundle();
        args.putParcelable(USER_LISTENER_BEAN_EXTRA, bean);
        ChangePasswordDialogFragment fragment = new ChangePasswordDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(getContext()).inject(this);
        mBean = (UserBean) getArguments().getParcelable(USER_LISTENER_BEAN_EXTRA);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.change_password_dialog_fragment, null);
        mCurrenPasswordLayout = (TextInputLayout)view.findViewById(R.id.current_password_layout);
        mCurrenPasswordEditText = (EditText) view.findViewById(R.id.current_password);
        mCurrenPasswordEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkCurrentPasswordField(mCurrenPasswordEditText.getText().toString());
                } else {
                    mCurrenPasswordLayout.setErrorEnabled(false);
                }
            }
        });
        mNewPasswordLayout = (TextInputLayout) view.findViewById(R.id.new_password_layout);
        mNewPasswordEditText = (EditText) view.findViewById(R.id.new_password);
        mNewPasswordEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkNewPasswordField(mNewPasswordEditText.getText().toString());
                } else {
                    mNewPasswordLayout.setErrorEnabled(false);
                }
            }
        });
        mConfirmNewPasseordLayout = (TextInputLayout) view.findViewById(R.id.confirm_new_password_layout);
        mConfirmNewPasseordEditText = (EditText) view.findViewById(R.id.confirm_new_password);
        mConfirmNewPasseordEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkConfirmedNewPasswordField(mConfirmNewPasseordEditText.getText().toString(),
                            mNewPasswordEditText.getText().toString());
                } else {
                    mConfirmNewPasseordLayout.setErrorEnabled(false);
                }
            }
        });
        Button changePassword = (Button) view.findViewById(R.id.change);
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.changePassword(getContext(),
                        mBean.getSessionId(),
                        mCurrenPasswordEditText.getText().toString(),
                        mNewPasswordEditText.getText().toString(),
                        mConfirmNewPasseordEditText.getText().toString(),
                        mNetworkUtils
                        );
            }
        });
        builder.setView(view);
        return builder.create();
    }

    @Override
    public void showCurrentPasswordLayoutException(int stringExceptionId) {
        mCurrenPasswordLayout.setErrorEnabled(true);
        mCurrenPasswordLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showNewPasswordLayoutException(int stringExceptionId) {
        mNewPasswordLayout.setErrorEnabled(true);
        mNewPasswordLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showConfirmedNewPasswordLayoutException(int stringExceptionId) {
        mConfirmNewPasseordLayout.setErrorEnabled(true);
        mConfirmNewPasseordLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(dialogErrorMessageId),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), ERROR_OPERATION);
    }

    @Override
    public void onSuccessChangePassword() {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.avatar_success_uploaded_title),
                getString(R.string.change_password_success_text),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), SUCCESS_OPERATION);
        this.dismiss();
    }
}
