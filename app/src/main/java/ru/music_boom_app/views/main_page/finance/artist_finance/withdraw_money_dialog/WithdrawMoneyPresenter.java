package ru.music_boom_app.views.main_page.finance.artist_finance.withdraw_money_dialog;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.math.BigDecimal;
import java.util.Observable;
import java.util.Observer;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.models.parsers.BaseResponseParser;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.request_observer.states.StateRequest;

/**
 * @author Markin Andrey on 24.04.2018.
 */
@InjectViewState
public class WithdrawMoneyPresenter extends BasePresenter<WithdrawMoneyView> {
    private static final String WITHDRAW_OPERATION_REQUEST_ID = "withdraw_operation_request_id";

    @Override
    public void setResponse(String response, String requestId) {
        if (WITHDRAW_OPERATION_REQUEST_ID.equals(requestId)){
            if (BaseResponseParser.checkSuccessResponse(response)){
                getViewState().onSuccessWithdrawOperation();
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }
    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        getViewState().showFailDialog(dialogErrorMessageId);
    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

    public void withdrawMoney(Context context, String amount, UserArtistBean artistBean, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)) {
            BigDecimal amountBigDecimal = new BigDecimal(amount);

            if (!checkWithdrawAmount(amountBigDecimal)) {
                return;
            }

           WithdrawOperationPostRequest postRequest = new WithdrawOperationPostRequest(this, R.string.request_fail_message);
            postRequest.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    WithdrawOperationPostRequest request = (WithdrawOperationPostRequest) o;
                    StateRequest stateRequest = request.getStateRequest();
                    stateRequest.shareData();
                }
            });

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", artistBean.getSessionId());
                jsonObject.put("sum", amountBigDecimal);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = createRequestBodyText("registrationJson", jsonObject.toString());
            postRequest.createAndSendRequest(BuildConfig.URL, requestBody, WITHDRAW_OPERATION_REQUEST_ID);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public boolean checkWithdrawAmount(BigDecimal amount) {
        if (!checkAmount(amount)) {
            getViewState().showAmountLayoutException(R.string.invalid_withdraw_amount);
            return false;
        }
        return true;
    }

    private boolean checkAmount(BigDecimal amount) {
        if (amount.compareTo(new BigDecimal(500)) < 0) {
            return false;
        }
        return true;
    }

    private RequestBody createRequestBodyText(String key, String value) {
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), value);
    }
}
