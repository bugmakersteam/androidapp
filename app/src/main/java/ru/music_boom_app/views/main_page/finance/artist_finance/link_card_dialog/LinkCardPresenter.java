package ru.music_boom_app.views.main_page.finance.artist_finance.link_card_dialog;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Observable;
import java.util.Observer;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.models.parsers.BaseResponseParser;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.request_observer.states.StateRequest;

/**
 * @author Markin Andrey on 24.04.2018.
 */
@InjectViewState
public class LinkCardPresenter extends BasePresenter<LinkCardView> {
    private static final int MINIMAL_NUMBERS = 16;
    private static final String CHANGE_CARD_NUMBER_REQUEST_ID = "change_card_number_request_id";
    private String mNewCardNumber;

    @Override
    public void setResponse(String response, String requestId) {
        if (CHANGE_CARD_NUMBER_REQUEST_ID.equals(requestId)){
            if (BaseResponseParser.checkSuccessResponse(response)){
                getViewState().onSuccessCardChanged(mNewCardNumber);
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }
    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        getViewState().showFailDialog(dialogErrorMessageId);
    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

    public void updateCardNumber(Context context, String cardNumber, UserArtistBean artistBean, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)) {
            if (!checkCardNumber(cardNumber)) {
                return;
            }

            ChangeArtistCardNumberPostRequest postRequest = new ChangeArtistCardNumberPostRequest(this, R.string.request_fail_message);
            postRequest.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    ChangeArtistCardNumberPostRequest request = (ChangeArtistCardNumberPostRequest) o;
                    StateRequest stateRequest = request.getStateRequest();
                    stateRequest.shareData();
                }
            });

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", artistBean.getSessionId());
                jsonObject.put("cardNumber", cardNumber);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = createRequestBodyText("registrationJson", jsonObject.toString());
            mNewCardNumber = cardNumber;
            postRequest.createAndSendRequest(BuildConfig.URL, requestBody, CHANGE_CARD_NUMBER_REQUEST_ID);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public boolean checkCardNumber(String cardNumber) {
        if (!checkNumber(cardNumber)) {
            getViewState().showCardNumberLayoutException(R.string.change_invalid_card_number);
            return false;
        }
        return true;
    }

    private boolean checkNumber(String cardNumber) {
        if (cardNumber.length() < MINIMAL_NUMBERS) {
            return false;
        }
        return true;
    }

    private RequestBody createRequestBodyText(String key, String value) {
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), value);
    }

}
