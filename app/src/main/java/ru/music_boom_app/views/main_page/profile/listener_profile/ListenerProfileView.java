package ru.music_boom_app.views.main_page.profile.listener_profile;

import android.graphics.Bitmap;
import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * Created by markin_a on 22.03.2018.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface ListenerProfileView extends MvpView {
    void showProgress();

    void hideProgress();

    void showFailDialog(int dialogErrorMessageId);

    void onDownloadedAvatar(Bitmap bitmap);

    void onSuccessAvatarUploaded();

    void onSuccessPersonalFieldsUpdated();

    void onEditFields();

    void onSaveFields();

    void showCountryLayoutException(@StringRes int stringExceptionId);

    void showCityLayoutException(@StringRes int stringExceptionId);

    void showEmailLayoutException(@StringRes int stringExceptionId);
}
