package ru.music_boom_app.views.main_page.profile.artist_profile.profile;

import android.graphics.Bitmap;
import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Markin Andrey on 21.04.2018.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface ArtistProfileView extends MvpView {
    void showProgress();
    void hideProgress();
    void showFailDialog(@StringRes int dialogErrorMessageId);
    void onAvatarDownloaded(Bitmap bitmap);
    void onEditFields();
    void onSaveFields();
    void showNameLayoutException(@StringRes int stringExceptionId);
    void showSurnameLayoutException(@StringRes int stringExceptionId);
    void showNicknameLayoutException(@StringRes int stringExceptionId);
    void showBirthdayLayoutException(@StringRes int stringExceptionId);
    void showCityLayoutException(@StringRes int stringExceptionId);
    void showCountryLayoutException(@StringRes int stringExceptionId);
    void showBirthdayDate(String date);
    void onSuccessAvatarUploaded();
    void onSuccessPersonalFieldsUpdated();

}
