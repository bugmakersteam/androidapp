package ru.music_boom_app.views.main_page.donate;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ProgressBar;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.math.BigDecimal;

import javax.inject.Inject;

import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.views.base.BaseApplicationActivity;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;

/**
 * @author Markin Andrey on 07.04.2018.
 */
public class DonateActivity extends BaseApplicationActivity implements DonateView {

    public static final String USER_BEAN_EXTRA = "user_bean_extra";
    public static final String USER_RECEIVER_ID_EXTRA = "user_receiver_id_extra";
    public static final String SUM_EXTRA = "sum_extra";
    private static final String UNAVAILABLE_AMOUNT = "unavailable_sum";
    private static final String ERROR_OPERATION = "error_operation";

    private UserBean mBean;
    private String mUserReceiverId;
    private BigDecimal mDonateSum;
    private ProgressBar mProgressBar;

    @Inject
    NetworkUtils mNetworkUtils;
    @InjectPresenter
    DonatePresenter mPresenter;

    public static Intent createDonateActivity(Activity activity, UserBean bean, String userReceiverId, BigDecimal sum){
        Intent intent = new Intent(activity, DonateActivity.class);
        intent.putExtra(USER_BEAN_EXTRA, bean);
        intent.putExtra(USER_RECEIVER_ID_EXTRA, userReceiverId);
        intent.putExtra(SUM_EXTRA, sum);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(this).inject(this);
        Intent intent = getIntent();
        mBean = (UserBean) intent.getParcelableExtra(USER_BEAN_EXTRA);
        mUserReceiverId = intent.getStringExtra(USER_RECEIVER_ID_EXTRA);
        mDonateSum = (BigDecimal) intent.getSerializableExtra(SUM_EXTRA);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mPresenter.startPayment(this, mDonateSum);
    }

    @Override
    public void onUnavailableAmount() {
        BaseApplicationAlertDialogFragment dialogFragment = UnavailableAmountDialog.newDialog(
                getString(R.string.unavailable_amount_dialog_title),
                getString(R.string.unavailable_amount_dialog_text),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getSupportFragmentManager(), UNAVAILABLE_AMOUNT);
    }

    @Override
    public void onActivityFinish() {
        finish();
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(dialogErrorMessageId),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getSupportFragmentManager(), ERROR_OPERATION);
    }
}
