package ru.music_boom_app.views.main_page.finance.artist_finance;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.views.base.BaseApplicationFragment;

/**
 * @author Markin Andrey on 24.04.2018.
 */
public class ArtistFinanceFragment extends BaseApplicationFragment implements ArtistFinanceView {
    private static final String USER_BEAN = "user_bean";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.registration_date)
    TextView mRegistrationDate;
    @BindView(R.id.balance)
    TextView mBalance;
    @BindView(R.id.earned)
    TextView mEarnedMoney;
    @BindView(R.id.derived)
    TextView mDerivedMoney;
    @BindView(R.id.performances)
    TextView mAllPerformances;
    @BindView(R.id.hours)
    TextView mMonthHours;
    @BindView(R.id.money)
    TextView mMonthMoney;
    @BindView(R.id.average_time)
    TextView mAveragePerformanceTime;
    @BindView(R.id.linked_card)
    EditText mLinkedCard;
    @BindView(R.id.link_card_button)
    Button mLinkCardButton;
    @BindView(R.id.derive_button)
    Button mDeriveMoneyButton;


    private UserArtistBean mArtistBean;

    @InjectPresenter
    ArtistFinancePresenter mPresenter;

    public static ArtistFinanceFragment newInstance(UserArtistBean bean) {
        Bundle args = new Bundle();
        args.putParcelable(USER_BEAN, bean);
        ArtistFinanceFragment fragment = new ArtistFinanceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mArtistBean = (UserArtistBean) getArguments().getParcelable(USER_BEAN);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.artist_finance_fragment, container, false);
        ButterKnife.bind(this, view);
        mToolbar.setTitle(R.string.user_finance_title);
        mToolbar.inflateMenu(R.menu.user_creativity_menu);
        initCurrentValues();

        mLinkCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.linkCard(mArtistBean, getActivity().getSupportFragmentManager(), ArtistFinanceFragment.this);
            }
        });

        mDeriveMoneyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.withdrawMoney(mArtistBean, getActivity().getSupportFragmentManager());
            }
        });

        return view;
    }

    private void initCurrentValues() {
        mRegistrationDate.setText(mArtistBean.getDateOfRegistration());
        mBalance.setText(mArtistBean.getFinanceStatisticsBean().getCurrentBalance());
        mEarnedMoney.setText(mArtistBean.getFinanceStatisticsBean().getAllEarnedMoney());
        mDerivedMoney.setText(mArtistBean.getFinanceStatisticsBean().getAllDerivedMoney());
        mAllPerformances.setText(String.valueOf(mArtistBean.getPerformanceStatisticsBean().getAllPerformances()));
        mMonthHours.setText(String.valueOf(mArtistBean.getPerformanceStatisticsBean().getThisMonthHours()));
        mMonthMoney.setText(String.valueOf(mArtistBean.getPerformanceStatisticsBean().getThisMonthMoney()));
        mAveragePerformanceTime.setText(String.valueOf(mArtistBean.getPerformanceStatisticsBean().getAveragePerformanceTime()));
        mLinkedCard.setText(mArtistBean.getCardNumber());

        if (mArtistBean.isLinkedCard()) {
            mLinkCardButton.setText(R.string.change_card_title);
            mDeriveMoneyButton.setVisibility(View.VISIBLE);
        }
    }

    public void setNewCardNumber(String cardNumber) {
        mArtistBean.setCardNumber(cardNumber);
        mArtistBean.setLinkedCard(true);
        initCurrentValues();
    }
}
