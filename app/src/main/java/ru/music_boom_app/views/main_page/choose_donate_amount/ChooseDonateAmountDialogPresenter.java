package ru.music_boom_app.views.main_page.choose_donate_amount;

import com.arellomobile.mvp.InjectViewState;

import java.io.File;

import ru.music_boom_app.presenters.BasePresenter;

/**
 * @author Markin Andrey on 18.04.2018.
 */
@InjectViewState
public class ChooseDonateAmountDialogPresenter extends BasePresenter<ChooseDonateAmountDialogView> {
    @Override
    public void setResponse(String response, String requestId) {

    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {

    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }
}
