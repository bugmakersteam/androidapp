package ru.music_boom_app.views.main_page.drawer_menu.feedback;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Observable;
import java.util.Observer;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.models.parsers.BaseResponseParser;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.request_observer.states.StateRequest;

/**
 * @author Markin Andrey on 18.04.2018.
 */
@InjectViewState
public class FeedbackPresenter extends BasePresenter<FeedbackView> {
    private static final String FEEDBACK_REQUEST_ID = "feedback_request_id";

    @Override
    public void setResponse(String response, String requestId) {
        if (FEEDBACK_REQUEST_ID.equals(requestId)){
            if (BaseResponseParser.checkSuccessResponse(response)){
                getViewState().onSuccessSentFeedback();
            } else {
                showFailDialog(R.string.request_fail_message);
            }
        }
    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        getViewState().showFailDialog(dialogErrorMessageId);
    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

    public void sendFeedback(Context context, UserBean userBean, String currentType, String text, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)) {
            FeedbackPostRequest postRequest = new FeedbackPostRequest(this, R.string.request_fail_message);
            postRequest.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    FeedbackPostRequest request = (FeedbackPostRequest) o;
                    StateRequest stateRequest = request.getStateRequest();
                    stateRequest.shareData();
                }
            });
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", userBean.getSessionId());
                jsonObject.put("type", currentType);
                jsonObject.put("text", text);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            RequestBody requestBody = createRequestBodyText("registrationJson", jsonObject.toString());
            postRequest.createAndSendRequest(BuildConfig.URL, requestBody, FEEDBACK_REQUEST_ID);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    private RequestBody createRequestBodyText(String key, String value) {
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), value);
    }
}
