package ru.music_boom_app.views.main_page;

import android.app.Activity;

import com.arellomobile.mvp.InjectViewState;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.math.BigDecimal;
import java.util.Observable;
import java.util.Observer;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.models.parsers.BaseResponseParser;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.request_observer.states.StateRequest;
import ru.music_boom_app.views.main_page.donate.DonateTransactionPostRequest;
import ru.music_boom_app.views.main_page.drawer_menu.logout.LogoutPostRequest;

/**
 * Created by markin_a on 21.03.2018.
 */
@InjectViewState
public class MainPageActivityPresenter extends BasePresenter<MainPageActivityView> {
    private static  final String LOGOUT_REQUEST_ID = "logout_request_id";
    private static final String DONATE_TRANSACTION_REQUEST_ID = "donate_transaction_request_id";

    @Override
    public void setResponse(String response, String requestId) {
        if (LOGOUT_REQUEST_ID.equals(requestId)){
            if (BaseResponseParser.checkSuccessResponse(response)){
                getViewState().onFinishApp();
            } else {
                showFailDialog(R.string.request_fail_message);
            }
        }
        if (DONATE_TRANSACTION_REQUEST_ID.equals(requestId)) {
            if (BaseResponseParser.checkSuccessResponse(response)) {
                getViewState().onSuccessTransaction();
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }
    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {

    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

    public void showListenerProfile() {
        getViewState().onShowListenerProfile();
    }

    public void showListenerFinance() {
        getViewState().onShowListenerFinance();
    }

    public void showPerformances() {
        getViewState().onShowPerformances();
    }

    public void showLocator() {
        getViewState().onShowLocator();
    }

    public void startLogout() {
        getViewState().onStartLogout();
    }

    public void showArtistWorkArea(){
        getViewState().onShowArtistWorkArea();
    }

    public void logout(MainPageActivity activity, UserBean userBean, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(activity)) {
            LogoutPostRequest postRequest = new LogoutPostRequest(this, R.string.request_fail_message);
            postRequest.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    LogoutPostRequest request = (LogoutPostRequest) o;
                    StateRequest stateRequest = request.getStateRequest();
                    stateRequest.shareData();
                }
            });
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", userBean.getSessionId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            RequestBody requestBody = createRequestBodyText("registrationJson", jsonObject.toString());
            postRequest.createAndSendRequest(BuildConfig.URL, requestBody, LOGOUT_REQUEST_ID);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public void donateTransaction(Activity activity,
                                  String sessionId,
                                  String senderId,
                                  String recipientId,
                                  BigDecimal donateSum,
                                  String transactionId,
                                  NetworkUtils networkUtils
    ) {
        if (networkUtils.checkConnection(activity)) {
            DonateTransactionPostRequest request = new DonateTransactionPostRequest(this, R.string.request_fail_message);
            request.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    DonateTransactionPostRequest postRequest = (DonateTransactionPostRequest) o;
                    StateRequest stateRequest = postRequest.getStateRequest();
                    stateRequest.shareData();
                }
            });
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", sessionId);
                jsonObject.put("senderId", senderId);
                jsonObject.put("recipientId", recipientId);
                jsonObject.put("sum", donateSum.toString());
                jsonObject.put("numberOfTransaction", transactionId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            RequestBody requestBody = createRequestBodyText("registrationJson", jsonObject.toString());
            request.createAndSendRequest(BuildConfig.URL, requestBody, DONATE_TRANSACTION_REQUEST_ID);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }

    }



    private RequestBody createRequestBodyText(String key, String value) {
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), value);
    }

    public void showRootFragmentArtistProfile() {
        getViewState().onShowRootFragmentArtistProfile();
    }
}
