package ru.music_boom_app.views.main_page.finance.artist_finance;

import android.support.v4.app.FragmentManager;

import com.arellomobile.mvp.InjectViewState;

import java.io.File;

import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.views.main_page.finance.artist_finance.link_card_dialog.LinkCardDialogFragment;
import ru.music_boom_app.views.main_page.finance.artist_finance.withdraw_money_dialog.WithdrawMoneyDialog;

/**
 * @author Markin Andrey on 24.04.2018.
 */
@InjectViewState
public class ArtistFinancePresenter extends BasePresenter<ArtistFinanceView> {
    private static final String LINK_CARD_DIALOG_TAG = "link_card_dialog_tag";
    private static final int LINK_CARD_DIALOG_ID = 331;
    private static final String WITHDRAW_MONEY_DIALOG_TAG = "withdraw_money_dialog_tag";

    @Override
    public void setResponse(String response, String requestId) {

    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {

    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

    public void linkCard(UserArtistBean bean, FragmentManager supportFragmentManager, ArtistFinanceFragment artistFinanceFragment) {
        LinkCardDialogFragment dialogFragment = LinkCardDialogFragment.newInstance(bean);
        dialogFragment.setTargetFragment(artistFinanceFragment, LINK_CARD_DIALOG_ID);
        dialogFragment.show(supportFragmentManager, LINK_CARD_DIALOG_TAG);
    }

    public void withdrawMoney(UserArtistBean artistBean, FragmentManager supportFragmentManager) {
        WithdrawMoneyDialog withdrawDialog = WithdrawMoneyDialog.newInstance(artistBean);
        withdrawDialog.show(supportFragmentManager, WITHDRAW_MONEY_DIALOG_TAG);
    }
}
