package ru.music_boom_app.views.main_page.perfomance;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Markin Andrey on 12.04.2018.
 */
public class ActiveArtistsIntentService extends Service {
    private Handler mActiveUserRequestHandler;
    private Runnable mRunnable;
    AsyncTask task;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mActiveUserRequestHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                new CoordinationTask().execute(intent);
                mActiveUserRequestHandler.postDelayed(this, 30000);
            }
        };
        mRunnable.run();

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mActiveUserRequestHandler.removeCallbacks(mRunnable);
    }

    private class CoordinationTask extends AsyncTask<Intent, Void, Void> {

        @Override
        protected Void doInBackground(Intent... intents) {
            Intent intent = intents[0];
            String sessionId = intent.getStringExtra(PerformanceFragment.SESSION);
            double latitude = intent.getDoubleExtra(PerformanceFragment.LATITUDE, 0);
            double longitude = intent.getDoubleExtra(PerformanceFragment.LONGITUDE, 0);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", sessionId);
                jsonObject.put("radius", 35000);
                jsonObject.put("longitude", longitude);
                jsonObject.put("latitude", latitude);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ActiveArtistsPostRequest request = new ActiveArtistsPostRequest();
            if (latitude != 0 && longitude != 0) {
                final String response = request.createCustomRequest(jsonObject);
                if (response != null) {
                    Intent resultIntent = new Intent();
                    resultIntent.setAction(PerformanceFragment.ACTIVE_ARTISTS_BROADCAST_ACTION);
                    resultIntent.putExtra(PerformanceFragment.RESULT_BROADCAST, response);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(resultIntent);
                }
            }
            return null;
        }
    }
}
