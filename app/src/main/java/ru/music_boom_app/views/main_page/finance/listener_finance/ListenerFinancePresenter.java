package ru.music_boom_app.views.main_page.finance.listener_finance;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;

import com.arellomobile.mvp.InjectViewState;

import java.io.File;
import java.math.BigDecimal;

import ru.music_boom_app.models.beans.UserListenerBean;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.views.main_page.donate.DonateActivity;

/**
 * @author Markin Andrey on 07.04.2018.
 */
@InjectViewState
public class ListenerFinancePresenter extends BasePresenter<ListenerFinanceView> {
    @Override
    public void setResponse(String response, String requestId) {

    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {

    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

    public void startAttachCardActivity(FragmentActivity activity, UserListenerBean bean) {
        Intent donateActivity = DonateActivity.createDonateActivity(activity, bean, "11", new BigDecimal(0.5));
        activity.startActivity(donateActivity);
    }
}
