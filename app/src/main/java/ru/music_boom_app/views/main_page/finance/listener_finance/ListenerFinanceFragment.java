package ru.music_boom_app.views.main_page.finance.listener_finance;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;

import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserListenerBean;
import ru.music_boom_app.views.base.BaseApplicationFragment;

/**
 * @author Markin Andrey on 07.04.2018.
 */
public class ListenerFinanceFragment extends BaseApplicationFragment implements ListenerFinanceView {

    private static final String USER_BEAN_EXTRA = "user_bean_extra";

    private UserListenerBean mBean;
    private Toolbar mToolbar;

    @InjectPresenter
    ListenerFinancePresenter mPresenter;

    public static ListenerFinanceFragment newInstance(UserListenerBean bean) {
        Bundle args = new Bundle();
        args.putParcelable(USER_BEAN_EXTRA, bean);
        ListenerFinanceFragment fragment = new ListenerFinanceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBean = (UserListenerBean) getArguments().getParcelable(USER_BEAN_EXTRA);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.listener_finance_fragment, container, false);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.bottom_nav_finance_title);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(mToolbar);
        ActionBar actionbar = activity.getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
        TextView sumArtistsTextView = (TextView) view.findViewById(R.id.sum_artists);
        sumArtistsTextView.setText(String.valueOf(mBean.getAllDonatedArtist()));
        Button attachCardButton = (Button) view.findViewById(R.id.attach_card);
        attachCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.startAttachCardActivity(getActivity(), mBean);
            }
        });
        return view;
    }
}
