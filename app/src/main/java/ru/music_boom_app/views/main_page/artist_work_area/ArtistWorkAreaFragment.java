package ru.music_boom_app.views.main_page.artist_work_area;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import javax.inject.Inject;

import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.PermissionsUtils;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;
import ru.music_boom_app.views.base.BaseApplicationFragment;

/**
 * @author Markin Andrey on 25.04.2018.
 */
public class ArtistWorkAreaFragment extends BaseApplicationFragment implements ArtistWorkAreaView {
    public static final String STATE_PERFORM_BROADCAST_ACTION = "ru.music_boom_app.views.main_page.artist_work_area.ArtistWorkAreaFragment.state_perform";
    public static final String RESULT_BROADCAST = "result_broadcast";
    private static final String ERROR_OPERATION = "error_operation";
    private static final String USER_BEAN = "user_bean";
    private static final int REQUEST_ERROR = 0;
    private static final int LOCATION_REQUEST_PERMISSIONS = 101;


    private TextView mEarnedMoney;
    private Button mPerformanceButton;
    private ProgressBar mProgressBar;
    private UserArtistBean mArtistBean;
    private GoogleApiClient mApiClient;
    private LocationListener mLocationListener;
    private Toolbar mToolbar;
    private boolean startPerfButtonState = true;
    private Intent mIntentService;
    private BroadcastReceiver mBroadcastReceiver;
    private TextView mSycronizationTitle;
    private TextView mSyncTime;


    @Inject
    PermissionsUtils mPermissionsUtils;
    @Inject
    NetworkUtils mNetworkUtils;
    @InjectPresenter
    ArtistWorkAreaPresenter mPresenter;

    public static ArtistWorkAreaFragment newInstance(UserArtistBean bean) {
        Bundle args = new Bundle();
        args.putParcelable(USER_BEAN, bean);
        ArtistWorkAreaFragment fragment = new ArtistWorkAreaFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(getContext()).inject(this);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mArtistBean = (UserArtistBean) getArguments().getParcelable(USER_BEAN);
        mApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        if (mPerformanceButton != null) {
                            //
                        }
                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .build();
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String jsonRequestResult = intent.getStringExtra(RESULT_BROADCAST);
                Log.e("WorkArea", jsonRequestResult);
                startPerfButtonState = false;
                mPerformanceButton.setBackground(getResources().getDrawable(R.drawable.end_performance_button_red_color_drawable, getActivity().getTheme()));
                mPerformanceButton.setText(R.string.stop_performance_button_title);
                mPresenter.setResponse(jsonRequestResult, ArtistWorkAreaPresenter.UPDATE_PERFORMANCE_REQUEST_ID);
            }
        };
        IntentFilter intentFilter = new IntentFilter(STATE_PERFORM_BROADCAST_ACTION);
        getContext().registerReceiver(mBroadcastReceiver, intentFilter);

    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.googleApiConnect();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.artist_work_area_fragment, container, false);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.bottom_nav_start_performance_title);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(mToolbar);
        ActionBar actionbar = activity.getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
        mEarnedMoney = (TextView) view.findViewById(R.id.earned_money);
        mProgressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        mPerformanceButton = (Button) view.findViewById(R.id.performance_button);

        mPerformanceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mPresenter.isPerformanceStart()) {
//                    mPresenter.startPerformance();
//                } else {
//                    if (mLocationListener != null) {
//                        LocationServices.FusedLocationApi.removeLocationUpdates(mApiClient, mLocationListener);
//                    }
//                    mPresenter.stopPerformance(getContext(),
//                            mArtistBean,
//                            mNetworkUtils
//                    );
//                }

                if (startPerfButtonState) {
                    startPerformance();
                } else {
                    mIntentService = new Intent(getActivity(), MyService.class);
                    getActivity().stopService(mIntentService);
                    mPresenter.stopPerformance(getContext(),
                            mArtistBean,
                            mNetworkUtils
                    );
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPerformanceButton.setEnabled(false);
        mPresenter.checkActivePerformanceRequest(getContext(), mArtistBean, mNetworkUtils);
        mPerformanceButton.setBackground(getResources().getDrawable(R.drawable.start_performance_button_primary_color_drawable, getActivity().getTheme()));
        mPerformanceButton.setText(R.string.start_performance_button_title);

        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int errorCode = googleApiAvailability.isGooglePlayServicesAvailable(getContext());
        if (errorCode != ConnectionResult.SUCCESS) {
            Dialog errorDialog = googleApiAvailability
                    .getErrorDialog(getActivity(), errorCode, REQUEST_ERROR, new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    });
            errorDialog.show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case LOCATION_REQUEST_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    startPerformance();
                } else {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mIntentService = new Intent(getActivity(), MyService.class);
        getActivity().stopService(mIntentService);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onStartPerformance() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30000);
        if (mApiClient != null) {
            if (mPermissionsUtils.checkPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})) {
                if (mApiClient.isConnected()) {
                    LocationServices.FusedLocationApi.requestLocationUpdates(
                            mApiClient, locationRequest, new LocationListener() {
                                @Override
                                public void onLocationChanged(Location location) {
                                    if (mPresenter.isFirstRequest()) {
                                        mPresenter.startPerformanceRequest(getContext(),
                                                mArtistBean,
                                                location.getLongitude(),
                                                location.getLatitude(),
                                                mNetworkUtils
                                        );
                                    } else {
                                        mPresenter.updatePerformanceRequest(getContext(),
                                                mArtistBean,
                                                location.getLongitude(),
                                                location.getLatitude(),
                                                mNetworkUtils
                                        );
                                    }
                                    mLocationListener = this;
                                }
                            }
                    );
                }
            } else {
                mPermissionsUtils.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_REQUEST_PERMISSIONS);
            }
        }
    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(dialogErrorMessageId),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), ERROR_OPERATION);
    }

    @Override
    public void onSuccessStartPerformance() {
        mPerformanceButton.setBackground(getResources().getDrawable(R.drawable.end_performance_button_red_color_drawable, getActivity().getTheme()));
        mPerformanceButton.setText(R.string.stop_performance_button_title);
        mPresenter.setPerformanceStart(false);
        mPresenter.setFirstRequest(false);

    }

    @Override
    public void onSuccessUpdatePerformance(String earnedMoney) {
        mEarnedMoney.setText(earnedMoney);
    }

    @Override
    public void onSuccessStopPerformance(String earnedMoney) {
        mPerformanceButton.setBackground(getResources().getDrawable(R.drawable.start_performance_button_primary_color_drawable, getActivity().getTheme()));
        mPerformanceButton.setText(R.string.start_performance_button_title);
        startPerfButtonState = true;
        // mPresenter.setPerformanceStart(true);
        // mPresenter.setFirstRequest(true);

        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.performance_finished_dialog_title),
                getString(R.string.performance_earned_money_dialog_text) + earnedMoney,
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), ERROR_OPERATION);
    }

    @Override
    public void onGoogleApiConnect() {
        mApiClient.connect();
    }

    @Override
    public void onShowProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onPerformanceExist() {
        mPerformanceButton.setEnabled(true);
        startPerfButtonState = false;
        mPerformanceButton.setBackground(getResources().getDrawable(R.drawable.end_performance_button_red_color_drawable, getActivity().getTheme()));
        mPerformanceButton.setText(R.string.stop_performance_button_title);
    }

    @Override
    public void onPerformanceNotExist() {
        mPerformanceButton.setEnabled(true);
        mPerformanceButton.setBackground(getResources().getDrawable(R.drawable.start_performance_button_primary_color_drawable, getActivity().getTheme()));
        mPerformanceButton.setText(R.string.start_performance_button_title);
        startPerfButtonState = true;
        mIntentService = new Intent(getActivity(), MyService.class);
        getActivity().stopService(mIntentService);
    }

    private void startPerformance() {
        if (mPermissionsUtils.checkPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION})) {
            mIntentService = new Intent(getActivity(), MyService.class);
            mIntentService.putExtra(USER_BEAN, mArtistBean);
            getActivity().startService(mIntentService);
            startPerfButtonState = false;
            mPerformanceButton.setBackground(getResources().getDrawable(R.drawable.end_performance_button_red_color_drawable, getActivity().getTheme()));
            mPerformanceButton.setText(R.string.stop_performance_button_title);
        } else {
            mPermissionsUtils.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_REQUEST_PERMISSIONS);
        }
    }
}
