package ru.music_boom_app.views.main_page.drawer_menu.feedback;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.arellomobile.mvp.presenter.InjectPresenter;

import javax.inject.Inject;

import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;
import ru.music_boom_app.views.base.BaseApplicationFragment;

/**
 * @author Markin Andrey on 18.04.2018.
 */
public class FeedbackFragment extends BaseApplicationFragment implements FeedbackView {
    private static final String ERROR_OPERATION = "error_operation";
    private static final String BEAN_EXTRA = "bean_extra";
    private static final String[] FEEDBACK_TYPES = new String[]{"Проблемы в работе приложения", "Финансовые проблемы", "Предложения и пожелания"};

    private UserBean mUserBean;
    private Spinner mSpinner;
    private EditText mDescriptionProblemEditText;
    private Button mSendFeedbackButton;
    private String mCurrentType;

    @Inject
    NetworkUtils mNetworkUtils;
    @InjectPresenter
    FeedbackPresenter mPresenter;

    public static FeedbackFragment newInstance(UserBean bean) {
        Bundle args = new Bundle();
        args.putParcelable(BEAN_EXTRA, bean);
        FeedbackFragment fragment = new FeedbackFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(getContext()).inject(this);
        mUserBean = (UserBean) getArguments().getParcelable(BEAN_EXTRA);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.feedback_fragment, container, false);
        mSpinner = (Spinner) view.findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.feedback_types_list_item, FEEDBACK_TYPES);
        mSpinner.setAdapter(adapter);

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        mCurrentType = "APPLICATION";
                        break;
                    case 1:
                        mCurrentType = "FINANCE";
                        break;
                    case 2:
                        mCurrentType = "PROPOSAL";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSpinner.setSelection(0);

        mDescriptionProblemEditText = (EditText) view.findViewById(R.id.text);
        mSendFeedbackButton = (Button) view.findViewById(R.id.send_feedback);
        mSendFeedbackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.sendFeedback(getContext(), mUserBean, mCurrentType, mDescriptionProblemEditText.getText().toString(), mNetworkUtils);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.feedback_title);
        }
    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(dialogErrorMessageId),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), ERROR_OPERATION);
    }

    @Override
    public void onSuccessSentFeedback() {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.success_title),
                getString(R.string.success_sent_feedback),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), ERROR_OPERATION);
        getActivity().getSupportFragmentManager().popBackStack();
    }
}
