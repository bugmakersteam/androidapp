package ru.music_boom_app.views.main_page.donate;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Markin Andrey on 07.04.2018.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface DonateView extends MvpView {

    void onUnavailableAmount();

    void onActivityFinish();

    void showProgress();

    void hideProgress();

    void showFailDialog(int dialogErrorMessageId);

}
