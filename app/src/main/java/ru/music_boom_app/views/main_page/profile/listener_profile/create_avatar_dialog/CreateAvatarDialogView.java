package ru.music_boom_app.views.main_page.profile.listener_profile.create_avatar_dialog;

import com.arellomobile.mvp.MvpView;

/**
 * @author Markin Andrey on 30.03.2018.
 */
public interface CreateAvatarDialogView extends MvpView {
}
