package ru.music_boom_app.views.main_page.finance.artist_finance.link_card_dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.arellomobile.mvp.presenter.InjectPresenter;

import javax.inject.Inject;

import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;
import ru.music_boom_app.views.main_page.finance.artist_finance.ArtistFinanceFragment;

/**
 * @author Markin Andrey on 24.04.2018.
 */
public class LinkCardDialogFragment extends BaseApplicationAlertDialogFragment implements LinkCardView {

    private static final String USER_BEAN = "user_bean";
    private static final String ERROR_OPERATION = "error_operation";
    private static final String SUCCESS_OPERATION = "success_operation";

    private UserArtistBean mArtistBean;
    private TextInputLayout mCardNumberLayout;
    private EditText mCardNumber;
    private Button mLinkCardButton;

    @Inject
    NetworkUtils mNetworkUtils;
    @InjectPresenter
    LinkCardPresenter mPresenter;

    public static LinkCardDialogFragment newInstance(UserArtistBean bean) {
        Bundle args = new Bundle();
        args.putParcelable(USER_BEAN, bean);
        LinkCardDialogFragment fragment = new LinkCardDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(getContext()).inject(this);
        mArtistBean = (UserArtistBean) getArguments().getParcelable(USER_BEAN);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.link_card_dialog, null);
        mCardNumberLayout = (TextInputLayout) view.findViewById(R.id.card_number_layout);
        mCardNumber = (EditText) view.findViewById(R.id.card_number);
        mCardNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkCardNumber(mCardNumber.getText().toString());
                } else {
                    mCardNumberLayout.setErrorEnabled(false);
                }
            }
        });

        mLinkCardButton = (Button) view.findViewById(R.id.link_card_button);
        mLinkCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.updateCardNumber(getContext(),
                        mCardNumber.getText().toString(),
                        mArtistBean,
                        mNetworkUtils);
            }
        });

        builder.setView(view);
        return builder.create();
    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(dialogErrorMessageId),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), ERROR_OPERATION);
    }

    @Override
    public void showCardNumberLayoutException(int stringExceptionId) {
        mCardNumberLayout.setErrorEnabled(true);
        mCardNumberLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void onSuccessCardChanged(String cardNumber) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.avatar_success_uploaded_title),
                getString(R.string.change_card_number_success_text),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), SUCCESS_OPERATION);
        ArtistFinanceFragment fragment = (ArtistFinanceFragment)getTargetFragment();
        fragment.setNewCardNumber(cardNumber);
        this.dismiss();
    }
}
