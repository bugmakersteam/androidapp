package ru.music_boom_app.views.main_page.choose_donate_amount;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.yandex.money.api.methods.payment.params.P2pTransferParams;
import com.yandex.money.api.methods.payment.params.PaymentParams;

import java.math.BigDecimal;

import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;
import ru.music_boom_app.views.main_page.donate.MusicBoomPaymentActivity;

/**
 * @author Markin Andrey on 18.04.2018.
 */
public class ChooseDonateAmountDialogFragment extends BaseApplicationAlertDialogFragment
        implements ChooseDonateAmountDialogView {

    public static final int PAYMENT_REQUEST_CODE = 666;
    private static final String USER_BEAN_EXTRA = "user_bean_extra";

    private final static String ARTIST_ID = "artist_id";

    private String mArtistId;
    private TextView mErrorMessageTextView;
    private ChooseAmountDialogCallback mChooseAmountDialogCallback;
    private UserBean mUserBean;


    public static ChooseDonateAmountDialogFragment newInstance(String artistId, UserBean bean) {
        Bundle args = new Bundle();
        args.putString(ARTIST_ID, artistId);
        args.putParcelable(USER_BEAN_EXTRA, bean);
        ChooseDonateAmountDialogFragment fragment = new ChooseDonateAmountDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public interface ChooseAmountDialogCallback {
        void onChosenSum(String recipientId, BigDecimal sum);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        mArtistId = bundle.getString(ARTIST_ID);
        mUserBean = bundle.getParcelable(USER_BEAN_EXTRA);

        mChooseAmountDialogCallback = (ChooseAmountDialogCallback) getActivity();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.choose_donate_amount_dialog, null);

        Button donate20 = (Button) view.findViewById(R.id.donate_20);
        donate20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChooseAmountDialogCallback.onChosenSum(mArtistId, new BigDecimal(20));
                startDonateActivity(new BigDecimal(20), mUserBean);
            }
        });

        Button donate50 = (Button) view.findViewById(R.id.donate_50);
        donate50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChooseAmountDialogCallback.onChosenSum(mArtistId, new BigDecimal(50));
                startDonateActivity(new BigDecimal(50), mUserBean);
            }
        });

        Button donate100 = (Button) view.findViewById(R.id.donate_100);
        donate100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChooseAmountDialogCallback.onChosenSum(mArtistId, new BigDecimal(100));
                startDonateActivity(new BigDecimal(100), mUserBean);
            }
        });

        EditText customAmountEditText = (EditText) view.findViewById(R.id.custom_amount);
        mErrorMessageTextView = (TextView) view.findViewById(R.id.error_message);
        Button customAmountButton = (Button) view.findViewById(R.id.custom_amount_button);
        customAmountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(customAmountEditText.getText().toString())) {
                    mErrorMessageTextView.setVisibility(View.GONE);
                    mChooseAmountDialogCallback.onChosenSum(mArtistId, new BigDecimal(customAmountEditText.getText().toString()));
                    startDonateActivity(new BigDecimal(customAmountEditText.getText().toString()), mUserBean);
                } else {
                    mErrorMessageTextView.setText(R.string.custom_amount_error_message);
                    mErrorMessageTextView.setVisibility(View.VISIBLE);
                }
            }
        });
        builder.setView(view);

        return builder.create();
    }

    private void startDonateActivity(BigDecimal donateSum, UserBean bean) {
        if (donateSum.compareTo(new BigDecimal(2)) >= 0) {
            PaymentParams params = new P2pTransferParams.Builder(BuildConfig.YANDEX_WALLET)
                    .setAmount(donateSum)
                    .setComment("Финансовая поддержка артиста")
                    .setMessage("Финансовая поддержка артиста")
                    .setLabel(mArtistId)
                    .create();

            Intent paymentActivityIntent = MusicBoomPaymentActivity.getBuilder(getActivity())
                    .setPaymentParams(params)
                    .setClientId(BuildConfig.YANDEX_CLIENT_ID)
                    .setUserBean(bean)
                    .build();
            mErrorMessageTextView.setVisibility(View.GONE);
            getActivity().startActivityForResult(paymentActivityIntent, PAYMENT_REQUEST_CODE);
        } else {
            mErrorMessageTextView.setText(R.string.unavailable_amount_dialog_text);
            mErrorMessageTextView.setVisibility(View.VISIBLE);
        }
    }
}
