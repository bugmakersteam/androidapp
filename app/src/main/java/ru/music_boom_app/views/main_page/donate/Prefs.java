package ru.music_boom_app.views.main_page.donate;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * @author Markin Andrey on 20.04.2018.
 */
public class Prefs {

    private static final String PREFS_NAME = "ru.yandex.money.android.preferences";
    private static final String PREF_INSTANCE_ID = "ru.yandex.money.android.instanceId";

    @NonNull
    private final SharedPreferences prefs;

    /**
     * Creates an instance of {@link ru.yandex.money.android.Prefs}.
     *
     * @param context current context
     */
    public Prefs(@NonNull Context context) {
        prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    /**
     * Stores instance id to preferences.
     *
     * @param instanceId instance id
     */
    public void storeInstanceId(@Nullable String instanceId) {
        prefs.edit()
                .putString(PREF_INSTANCE_ID, instanceId)
                .apply();
    }

    /**
     * Gets instance id from preferences.
     *
     * @return instance id or {@code null} if preferences do not contain this parameter
     */
    @Nullable
    String restoreInstanceId() {
        return prefs.getString(PREF_INSTANCE_ID, null);
    }
}
