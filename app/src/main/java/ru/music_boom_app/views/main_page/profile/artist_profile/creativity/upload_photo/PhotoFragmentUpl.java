package ru.music_boom_app.views.main_page.profile.artist_profile.creativity.upload_photo;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.PermissionsUtils;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;
import ru.music_boom_app.views.base.BaseApplicationFragment;
import ru.music_boom_app.views.main_page.profile.listener_profile.ListenerProfilePresenter;

import static android.app.Activity.RESULT_OK;

/**
 * @author Markin Andrey on 22.04.2018.
 */
public class PhotoFragmentUpl extends BaseApplicationFragment implements PhotoFragmentView {
    private static final String PHOTO_URLS = "photo_urls";
    private static final String POSITION = "position";
    private static final String USER_BEAN = "user_bean";
    private static final String ERROR_OPERATION = "error_operation";
    private static final String SUCCESS_OPERATION = "success_operation";
    public static final int CHOICE_IMAGE_DIALOG_RESULT = 301;
    public static final int CREATE_PHOTO_DIALOG_RESULT = 302;

    private ArrayList<String> mPhotoUrls;
    private int mPosition;
    private UserArtistBean mArtistBean;
    private AppCompatImageButton mDeleteButton;
    private AppCompatImageView mPhotoImageView;
    private ProgressBar mProgressBar;
    private String mNewAvatarPath;

    @Inject
    PermissionsUtils mPermissionsUtils;
    @Inject
    NetworkUtils mNetworkUtils;
    @InjectPresenter
    PhotoFragmentPresenter mPresenter;


    public static PhotoFragmentUpl newInstance(UserArtistBean bean, List<String> photoUrls, int position) {
        Bundle args = new Bundle();
        args.putParcelable(USER_BEAN, bean);
        args.putStringArrayList(PHOTO_URLS, (ArrayList<String>) photoUrls);
        args.putInt(POSITION, position);
        PhotoFragmentUpl fragment = new PhotoFragmentUpl();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(getContext()).inject(this);
        Bundle bundle = getArguments();
        mArtistBean = (UserArtistBean) bundle.getParcelable(USER_BEAN);
        mPhotoUrls = bundle.getStringArrayList(PHOTO_URLS);
        mPosition = bundle.getInt(POSITION);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.upload_photo_first_fragment, container, false);
        mDeleteButton = (AppCompatImageButton) view.findViewById(R.id.delete);
        mPhotoImageView = (AppCompatImageView) view.findViewById(R.id.add);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        if (mPosition == 0) {
            mDeleteButton.setVisibility(View.GONE);
            mPhotoImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPresenter.createNewPhoto(getActivity(), PhotoFragmentUpl.this);
                }
            });
        } else {
            mDeleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int currentPhotoPosition = mPosition - 1;
                    mPresenter.deleteImage(getActivity(),
                            mArtistBean,
                            mPhotoUrls.get(currentPhotoPosition),
                            mPhotoImageView,
                            mNetworkUtils);
                }
            });
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPosition != 0) {
            int currentPhotoPosition = mPosition - 1;
            mPresenter.downloadPhoto(getActivity(),
                    mPhotoUrls.get(currentPhotoPosition),
                    mPhotoImageView,
                    mNetworkUtils);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CHOICE_IMAGE_DIALOG_RESULT:
                choiceImage();
                break;

            case CREATE_PHOTO_DIALOG_RESULT:
                createPhoto();
                break;

            case ListenerProfilePresenter.CHOOSE_IMAGE_ACTIVITY_RESULT:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    mNewAvatarPath = getRealPathFromURI(imageUri);
                    mPresenter.uploadImageToServer(getContext(), mNewAvatarPath, mArtistBean.getSessionId(), mNetworkUtils);
                }
                break;

            case ListenerProfilePresenter.CREATE_PHOTO_ACTIVITY_RESULT:
                if (resultCode == RESULT_OK) {
                    mNewAvatarPath = mPresenter.getPhotoAvatarAbsolutePath();
                    mPresenter.uploadImageToServer(getContext(), mNewAvatarPath, mArtistBean.getSessionId(), mNetworkUtils);
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PhotoFragmentPresenter.CHOICE_IMAGE_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPresenter.choiceImage(getActivity(), this, mPermissionsUtils);
                }
                break;

            case PhotoFragmentPresenter.CREATE_PHOTO_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    mPresenter.createPhoto(getActivity(), this, mPermissionsUtils);
                }
                break;


            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }

    }

    public void choiceImage() {
        mPresenter.choiceImage(getActivity(), this, mPermissionsUtils);
    }

    public void createPhoto() {
        mPresenter.createPhoto(getActivity(), this, mPermissionsUtils);
    }

    @Override
    public void onSuccessAvatarUploaded() {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.avatar_success_uploaded_title),
                getString(R.string.image_success_uploaded_text),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), SUCCESS_OPERATION);
    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(dialogErrorMessageId),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), ERROR_OPERATION);
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccessDeletePhoto() {
//        int currentPhotoPosition = mPosition - 1;
//        mPhotoUrls.remove(currentPhotoPosition);
        mPhotoImageView.setImageResource(R.drawable.ic_add_a_photo_vector);

        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.avatar_success_uploaded_title),
                getString(R.string.image_success_delete_text),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), SUCCESS_OPERATION);
    }

    private String getRealPathFromURI(Uri contentURI) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(contentURI);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = {MediaStore.Images.Media.DATA};

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = getContext().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{id}, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }
}
