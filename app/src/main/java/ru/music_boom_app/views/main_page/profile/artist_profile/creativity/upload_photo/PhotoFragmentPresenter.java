package ru.music_boom_app.views.main_page.profile.artist_profile.creativity.upload_photo;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.AppCompatImageView;

import com.arellomobile.mvp.InjectViewState;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.models.parsers.BaseResponseParser;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.PermissionsUtils;
import ru.music_boom_app.utils.request_observer.states.StateRequest;
import ru.music_boom_app.views.main_page.profile.listener_profile.create_avatar_dialog.CreateAvatarDialogFragment;

/**
 * @author Markin Andrey on 22.04.2018.
 */
@InjectViewState
public class PhotoFragmentPresenter extends BasePresenter<PhotoFragmentView> {
    public static final int CHOICE_IMAGE_PERMISSION = 110;
    public static final int CREATE_PHOTO_PERMISSION = 111;
    public static final int CHOOSE_IMAGE_ACTIVITY_RESULT = 33;
    public static final int CREATE_PHOTO_ACTIVITY_RESULT = 44;
    private static final String CREATE_PHOTO_DIALOG = "create_photo_dialog";
    private final static int TARGET_FRAGMENT_REQUEST = 10;
    private static final String UPLOAD_IMAGE_REQUEST_ID = "upload_image_request_id";
    private static final String DELETE_PHOTO_REQUEST_ID = "delete_photo_request_id";

    private String mPhotoAvatarAbsolutePath;

    @Override
    public void setResponse(String response, String requestId) {
        if (UPLOAD_IMAGE_REQUEST_ID.equals(requestId)) {
            if (BaseResponseParser.checkSuccessResponse(response)) {
                getViewState().onSuccessAvatarUploaded();
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }

        if (DELETE_PHOTO_REQUEST_ID.equals(requestId)) {
            if (BaseResponseParser.checkSuccessResponse(response)) {
                getViewState().onSuccessDeletePhoto();
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }
    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        getViewState().showFailDialog(dialogErrorMessageId);
    }

    @Override
    public void showProgressBar() {
        getViewState().showProgress();
    }

    @Override
    public void hideProgressBar() {
        getViewState().hideProgress();
    }

    public void createNewPhoto(FragmentActivity activity, PhotoFragmentUpl fragment) {
        CreateAvatarDialogFragment dialogFragment = new CreateAvatarDialogFragment();
        dialogFragment.setTargetFragment(fragment, TARGET_FRAGMENT_REQUEST);
        dialogFragment.show(activity.getSupportFragmentManager(), CREATE_PHOTO_DIALOG);
    }

    public void choiceImage(FragmentActivity activity, PhotoFragmentUpl fragment, PermissionsUtils permissionsUtils) {
        if (permissionsUtils.checkPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            Intent chooserIntent = Intent.createChooser(intent, "Select Image");
            fragment.startActivityForResult(chooserIntent, CHOOSE_IMAGE_ACTIVITY_RESULT);
        } else {
            permissionsUtils.requestPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE, CHOICE_IMAGE_PERMISSION);
        }
    }

    public String getPhotoAvatarAbsolutePath() {
        return mPhotoAvatarAbsolutePath;
    }

    public void createPhoto(FragmentActivity activity, Fragment targetFragment, PermissionsUtils permissionsUtils) {
        if (permissionsUtils.checkPermissions(activity, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photoFile = null;

            try {
                photoFile = createImageFile(activity);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(activity,
                        "ru.music_boom_app.android.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                mPhotoAvatarAbsolutePath = photoFile.getAbsolutePath();
            }
            targetFragment.startActivityForResult(intent, CREATE_PHOTO_ACTIVITY_RESULT);
        } else {
            permissionsUtils.requestPermissions(activity, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CREATE_PHOTO_PERMISSION);
        }
    }

    public void uploadImageToServer(Context context, String newAvatarPath, String sessionId, NetworkUtils networkUtils) {
        ImagePhotoUploadRequest avatarUploadRequest = new ImagePhotoUploadRequest(this, R.string.request_fail_message);
        avatarUploadRequest.addObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ImagePhotoUploadRequest request = (ImagePhotoUploadRequest) o;
                StateRequest state = request.getStateRequest();
                state.shareData();
            }
        });
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sessionId", sessionId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = createRequestBodyText(jsonObject.toString());
        String type = "jpeg";
        File avatarFile = new File(newAvatarPath);

        if (networkUtils.checkConnection(context)) {
            avatarUploadRequest.uploadFile(BuildConfig.URL, requestBody, avatarFile, type, UPLOAD_IMAGE_REQUEST_ID);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public void downloadPhoto(FragmentActivity activity, String photoUrl, AppCompatImageView photoImageView, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(activity)) {
            Picasso.get().load(photoUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(photoImageView);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public void deleteImage(FragmentActivity activity, UserArtistBean artistBean, String photoUrl, AppCompatImageView photoImageView, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(activity)) {
            DeleteImagePostRequest postRequest = new DeleteImagePostRequest(this, R.string.request_fail_message);
            postRequest.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    DeleteImagePostRequest request = (DeleteImagePostRequest) o;
                    StateRequest stateRequest = request.getStateRequest();
                    stateRequest.shareData();
                }
            });

            JSONObject jsonObject = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            try {
                jsonObject.put("sessionId", artistBean.getSessionId());
                jsonArray.put(photoUrl);
                jsonObject.put("photos_id", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = createRequestBodyText(jsonObject.toString());
            postRequest.createAndSendRequest(BuildConfig.URL, requestBody, DELETE_PHOTO_REQUEST_ID);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    private File createImageFile(FragmentActivity activity) throws IOException {
        String imageFileName = "MusicBoom_avatar";
        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    private RequestBody createRequestBodyText(String value) {
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), value);
    }

}
