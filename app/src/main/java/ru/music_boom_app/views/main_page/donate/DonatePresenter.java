package ru.music_boom_app.views.main_page.donate;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;

import com.arellomobile.mvp.InjectViewState;
import com.yandex.money.api.methods.payment.params.P2pTransferParams;
import com.yandex.money.api.methods.payment.params.PaymentParams;

import java.io.File;
import java.math.BigDecimal;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.presenters.BasePresenter;
import ru.yandex.money.android.PaymentActivity;

/**
 * @author Markin Andrey on 07.04.2018.
 */
@InjectViewState
public class DonatePresenter extends BasePresenter<DonateView> {
    public static final int PAYMENT_REQUEST_CODE = 666;


    @Override
    public void setResponse(String response, String requestId) {
    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        getViewState().showFailDialog(dialogErrorMessageId);
    }

    @Override
    public void showProgressBar() {
        getViewState().showProgress();
    }

    @Override
    public void hideProgressBar() {
        getViewState().hideProgress();
    }

    public void startPayment(FragmentActivity activity, BigDecimal donateSum) {
        if (donateSum.compareTo(new BigDecimal(1)) >= 0) {
            PaymentParams params = new P2pTransferParams.Builder(BuildConfig.YANDEX_WALLET)
                    .setAmount(donateSum)
                    .setComment("Финансовая поддержка артиста")
                    .setMessage("Финансовая поддержка артиста")
                    .create();

            Intent paymentActivityIntent = PaymentActivity.getBuilder(activity)
                    .setPaymentParams(params)
                    .setClientId(BuildConfig.YANDEX_CLIENT_ID)
                    .build();

            activity.startActivityForResult(paymentActivityIntent, PAYMENT_REQUEST_CODE);
        } else {
            getViewState().onUnavailableAmount();
        }
    }



    private RequestBody createRequestBodyText(String key, String value) {
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), value);
    }
}
