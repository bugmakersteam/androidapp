package ru.music_boom_app.views.main_page.profile.artist_profile.profile;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.models.parsers.BaseResponseParser;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.PermissionsUtils;
import ru.music_boom_app.utils.request_observer.states.StateRequest;
import ru.music_boom_app.views.main_page.profile.listener_profile.AvatarUploadRequest;
import ru.music_boom_app.views.main_page.profile.listener_profile.change_password_dialog.ChangePasswordDialogFragment;
import ru.music_boom_app.views.main_page.profile.listener_profile.create_avatar_dialog.CreateAvatarDialogFragment;

/**
 * @author Markin Andrey on 21.04.2018.
 */
@InjectViewState
public class ArtistProfilePresenter extends BasePresenter<ArtistProfileView> {
    public static final int CHOICE_IMAGE_PERMISSION = 107;
    public static final int CREATE_PHOTO_PERMISSION = 108;

    public static final int CHOOSE_IMAGE_ACTIVITY_RESULT = 33;
    public static final int CREATE_PHOTO_ACTIVITY_RESULT = 44;
    private final static int TARGET_FRAGMENT_REQUEST = 10;
    private final static String AVATAR_REQUEST_ID = "avatar_request_id";
    private final static String AVATAR_IMAGE_NAME = "avatar.jpg";
    private final static String CREATE_AVATAR_DIALOG = "create_avatar_dialog";
    private final static String CHANGE_AVATAR_DIALOG = "change_avatar_dialog";
    private final static String UPLOAD_AVATAR_REQUEST_ID = "upload_avatar_request_id";
    private final static String UPDATE_PERSONAL_FIELDS_REQUEST_ID = "update_personal_fields_request_id";

    private boolean mIsAvatarDownloaded = false;
    private String mPhotoAvatarAbsolutePath;

    @Override
    public void setResponse(String response, String requestId) {
        if (UPLOAD_AVATAR_REQUEST_ID.equals(requestId)) {
            if (BaseResponseParser.checkSuccessResponse(response)) {
                getViewState().onSuccessAvatarUploaded();
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }

        if (UPDATE_PERSONAL_FIELDS_REQUEST_ID.equals(requestId)) {
            if (BaseResponseParser.checkSuccessResponse(response)) {
                getViewState().onSuccessPersonalFieldsUpdated();
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }
    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {

    }

    @Override
    public void showProgressBar() {
        getViewState().showProgress();
    }

    @Override
    public void hideProgressBar() {
        getViewState().hideProgress();
    }

    public void setAvatarDownloaded(boolean isDownloaded) {
        mIsAvatarDownloaded = isDownloaded;
    }

    public boolean isAvatarDownloaded() {
        return mIsAvatarDownloaded;
    }

    public void downloadAvatar(UserArtistBean bean) {
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                mIsAvatarDownloaded = true;
                getViewState().onAvatarDownloaded(bitmap);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        if (!TextUtils.isEmpty(bean.getAvatar())) {
            Picasso.get().load(bean.getAvatar()).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(target);
        }
    }

    public void editFields() {
        getViewState().onEditFields();
    }

    public void saveFields() {
        getViewState().onSaveFields();
    }

    public void updatePersonal(Context context,
                               String sessionId,
                               String surname,
                               String name,
                               String nickname,
                               String patronymic,
                               String birthday,
                               String country,
                               String city,
                               String email,
                               String vkLink,
                               String whatsApp,
                               String publicNameType,
                               NetworkUtils networkUtils) {
        if (!checkNameText(name)) {
            return;
        }
        if (!checkSurnameText(surname)) {
            return;
        }
        if (!checkNicknameText(nickname)) {
            return;
        }
        if (!checkBirthdayText(birthday)) {
            return;
        }
        if (!checkCountryText(country)) {
            return;
        }
        if (!checkCityText(city)) {
            return;
        }

        if (networkUtils.checkConnection(context)) {
            UpdateArtistPersonalFieldsRequest fieldsRequest = new UpdateArtistPersonalFieldsRequest(this, R.string.request_fail_message);
            fieldsRequest.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    UpdateArtistPersonalFieldsRequest request = (UpdateArtistPersonalFieldsRequest) o;
                    StateRequest state = request.getStateRequest();
                    state.shareData();
                }
            });

            JSONObject jsonObject = new JSONObject();
            JSONObject userObject = new JSONObject();
            try {
                jsonObject.put("sessionId", sessionId);
                userObject.put("name", name);
                userObject.put("surname", surname);
                userObject.put("patronymic", patronymic);


                SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                try {
                    Date formatDate = dateFormat.parse(birthday);
                    SimpleDateFormat androidFormat = new SimpleDateFormat("yyyy-MM-dd");
                    birthday = androidFormat.format(formatDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                userObject.put("birthday", birthday);
                userObject.put("nickname", nickname);
                userObject.put("country", country);
                userObject.put("city", city);
                userObject.put("email", email);
                userObject.put("vk", vkLink);
                userObject.put("tlg", "");
                userObject.put("wapp", whatsApp);
                userObject.put("name_representation", publicNameType);
                jsonObject.put("user", userObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = createRequestBodyText(jsonObject.toString());
            fieldsRequest.createAndSendRequest(BuildConfig.URL, requestBody, UPDATE_PERSONAL_FIELDS_REQUEST_ID);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public boolean checkNameText(String s) {
        if (checkText(s)) {
            getViewState().showNameLayoutException(R.string.registration_invalid_name);
            return false;
        } else {
            return true;
        }
    }

    public boolean checkSurnameText(String s) {
        if (checkText(s)) {
            getViewState().showSurnameLayoutException(R.string.registration_invalid_surname);
            return false;
        } else {
            return true;
        }
    }

    public boolean checkNicknameText(String s) {
        if (checkText(s)) {
            getViewState().showNicknameLayoutException(R.string.registration_invalid_nickname);
            return false;
        } else {
            return true;
        }
    }

    public boolean checkBirthdayText(String s) {
        if (checkText(s)) {
            getViewState().showBirthdayLayoutException(R.string.registration_invalid_birthday);
            return false;
        } else {
            return true;
        }
    }

    public boolean checkCountryText(String s) {
        if (checkText(s)) {
            getViewState().showCountryLayoutException(R.string.registration_invalid_country);
            return false;
        } else {
            return true;
        }
    }

    public boolean checkCityText(String s) {
        if (checkText(s)) {
            getViewState().showCityLayoutException(R.string.registration_invalid_city);
            return false;
        } else {
            return true;
        }
    }

    public void showDataPickerDialog(Context context) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialogFragment = new DatePickerDialog(context, R.style.AppCompatAlertDialogStyle, (datePicker, year1, month1, day1) -> {
            Calendar calendar = Calendar.getInstance(Locale.GERMANY);
            calendar.set(Calendar.YEAR, year1);
            calendar.set(Calendar.MONTH, month1);
            calendar.set(Calendar.DATE, day1);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            String date = dateFormat.format(calendar.getTime());
            getViewState().showBirthdayDate(date);
        }, year, month, day);
        dialogFragment.show();
    }

    public void createNewAvatar(FragmentActivity activity, ArtistProfileFragment fragment) {
        CreateAvatarDialogFragment dialogFragment = new CreateAvatarDialogFragment();
        dialogFragment.setTargetFragment(fragment, TARGET_FRAGMENT_REQUEST);
        dialogFragment.show(activity.getSupportFragmentManager(), CREATE_AVATAR_DIALOG);
    }

    public String getPhotoAvatarAbsolutePath() {
        return mPhotoAvatarAbsolutePath;
    }

    public void showChangePasswordDialog(FragmentActivity activity, UserArtistBean bean) {
        ChangePasswordDialogFragment dialogFragment = ChangePasswordDialogFragment.newInstance(bean);
        dialogFragment.show(activity.getSupportFragmentManager(), CHANGE_AVATAR_DIALOG);
    }

    public void choiceImage(FragmentActivity activity, ArtistProfileFragment fragment, PermissionsUtils permissionsUtils) {
        if (permissionsUtils.checkPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            Intent chooserIntent = Intent.createChooser(intent, "Select Image");
            fragment.startActivityForResult(chooserIntent, CHOOSE_IMAGE_ACTIVITY_RESULT);
        } else {
            permissionsUtils.requestPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE, CHOICE_IMAGE_PERMISSION);
        }
    }

    public void createPhoto(FragmentActivity activity, Fragment targetFragment, PermissionsUtils permissionsUtils) {
        if (permissionsUtils.checkPermissions(activity, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photoFile = null;

            try {
                photoFile = createImageFile(activity);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(activity,
                        "ru.music_boom_app.android.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                mPhotoAvatarAbsolutePath = photoFile.getAbsolutePath();
            }
            targetFragment.startActivityForResult(intent, CREATE_PHOTO_ACTIVITY_RESULT);
        } else {
            permissionsUtils.requestPermissions(activity, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CREATE_PHOTO_PERMISSION);
        }
    }

    private File createImageFile(FragmentActivity activity) throws IOException {
        String imageFileName = "MusicBoom_avatar";
        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    public void uploadAvatarToServer(Context context, String newAvatarPath, String sessionId, NetworkUtils networkUtils) {
        AvatarUploadRequest avatarUploadRequest = new AvatarUploadRequest(this, R.string.request_fail_message);
        avatarUploadRequest.addObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                AvatarUploadRequest request = (AvatarUploadRequest) o;
                StateRequest state = request.getStateRequest();
                state.shareData();
            }
        });
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sessionId", sessionId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String type = "jpeg";
        File avatarFile = new File(newAvatarPath);

        if (networkUtils.checkConnection(context)) {
            RequestBody requestBody = createRequestBodyText(jsonObject.toString());
            avatarUploadRequest.uploadFile(BuildConfig.URL, requestBody, avatarFile, type, UPLOAD_AVATAR_REQUEST_ID);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    private RequestBody createRequestBodyText(String value) {
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), value);
    }

    private boolean checkText(String text) {
        return TextUtils.isEmpty(text);
    }
}
