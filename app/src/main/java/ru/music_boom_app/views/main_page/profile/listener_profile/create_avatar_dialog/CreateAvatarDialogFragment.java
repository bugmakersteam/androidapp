package ru.music_boom_app.views.main_page.profile.listener_profile.create_avatar_dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.arellomobile.mvp.presenter.InjectPresenter;

import javax.inject.Inject;

import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.utils.PermissionsUtils;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;
import ru.music_boom_app.views.main_page.profile.listener_profile.ListenerProfileFragment;

import static android.app.Activity.RESULT_OK;

/**
 * @author Markin Andrey on 30.03.2018.
 */
public class CreateAvatarDialogFragment extends BaseApplicationAlertDialogFragment implements CreateAvatarDialogView {

    @Inject
    PermissionsUtils mPermissionsUtils;
    @InjectPresenter
    CreateAvatarDialogPresenter mPresenter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(getContext()).inject(this);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.create_avatar_dialog_fragment, null);
        Button chooseImgFromGallery = view.findViewById(R.id.gallery);
        chooseImgFromGallery.setOnClickListener(v -> {
            getTargetFragment().onActivityResult(ListenerProfileFragment.CHOICE_IMAGE_DIALOG_RESULT,
                    RESULT_OK,
                    null);
            dismiss();
        });
        Button createPhoto = view.findViewById(R.id.camera);
        createPhoto.setOnClickListener(v ->{
            getTargetFragment().onActivityResult(ListenerProfileFragment.CREATE_PHOTO_DIALOG_RESULT,
                    RESULT_OK,
                    null);
        dismiss();
        });
        builder.setView(view);
        return builder.create();
    }
}
