package ru.music_boom_app.views.main_page.donate;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import ru.music_boom_app.R;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;

/**
 * @author Markin Andrey on 08.04.2018.
 */
public class UnavailableAmountDialog extends BaseApplicationAlertDialogFragment {

    public static UnavailableAmountDialog newDialog(
            String title,
            String message,
            String possitiveButtonText){
        UnavailableAmountDialog dialogFragment = new UnavailableAmountDialog();
        dialogFragment.setTitle(title);
        dialogFragment.setMessage(message);
        dialogFragment.setPositiveButtonText(possitiveButtonText);
        return dialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setTitle(mTitle);
        builder.setMessage(mMessage);
        builder.setPositiveButton(mPositiveButtonText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getActivity().finish();
            }
        });
        return builder.create();
    }
}
