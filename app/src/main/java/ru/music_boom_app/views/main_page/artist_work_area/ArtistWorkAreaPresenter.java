package ru.music_boom_app.views.main_page.artist_work_area;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Observable;
import java.util.Observer;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.models.parsers.BaseResponseParser;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.request_observer.states.StateRequest;

/**
 * @author Markin Andrey on 25.04.2018.
 */
@InjectViewState
public class ArtistWorkAreaPresenter extends BasePresenter<ArtistWorkAreaView> {
    private static final String START_PERFORMANCE_REQUEST_ID = "start_performance_request_id";
    public static final String UPDATE_PERFORMANCE_REQUEST_ID = "update_performance_request_id";
    private static final String STOP_PERFORMANCE_REQUEST_ID = "stop_performance_request_id";
    private static final String CHECK_PERFORMANCE_REQUEST_ID = "check_performance_request_id";

    private boolean mIsPerformanceStart = true;
    private boolean mIsFirstRequest = true;

    @Override
    public void setResponse(String response, String requestId) {
        if (UPDATE_PERFORMANCE_REQUEST_ID.equals(requestId)) {
            if (BaseResponseParser.checkSuccessResponse(response)) {
                String earnedMoney = null;
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    earnedMoney = jsonObject.optString("currentEarnedMoney", "");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                getViewState().onSuccessUpdatePerformance(earnedMoney);
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }

        if (STOP_PERFORMANCE_REQUEST_ID.equals(requestId)) {
            if (BaseResponseParser.checkSuccessResponse(response)) {
                String earnedMoney = null;
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    earnedMoney = jsonObject.optString("earnedMoney", "");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                getViewState().onSuccessStopPerformance(earnedMoney);
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }

        if (START_PERFORMANCE_REQUEST_ID.equals(requestId)) {
            if (BaseResponseParser.checkSuccessResponse(response)) {
                getViewState().onSuccessStartPerformance();
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }

        if (CHECK_PERFORMANCE_REQUEST_ID.equals(requestId)){
            if (BaseResponseParser.checkSuccessResponse(response)) {
                getViewState().onPerformanceExist();
            } else {
                getViewState().onPerformanceNotExist();
            }
        }
    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        getViewState().showFailDialog(dialogErrorMessageId);
    }

    @Override
    public void showProgressBar() {
        getViewState().onShowProgress();
    }

    @Override
    public void hideProgressBar() {
        getViewState().onHideProgress();
    }

    public void startPerformance() {
        getViewState().onStartPerformance();
    }

    public void stopPerformance(Context context, UserArtistBean artistBean, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)) {
            StopPerformancePostRequest postRequest = new StopPerformancePostRequest(this, R.string.request_fail_message);
            postRequest.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    StopPerformancePostRequest request = (StopPerformancePostRequest) o;
                    StateRequest stateRequest = request.getStateRequest();
                    stateRequest.shareData();
                }
            });

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", artistBean.getSessionId());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = createRequestBodyText("jsonBody", jsonObject.toString());
            postRequest.createAndSendRequest(BuildConfig.URL, requestBody, STOP_PERFORMANCE_REQUEST_ID);

        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public void startPerformanceRequest(Context context, UserArtistBean artistBean, double longitude, double latitude, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)) {
            StartPerformancePostRequest postRequest = new StartPerformancePostRequest(this, R.string.request_fail_message);
            postRequest.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    StartPerformancePostRequest request = (StartPerformancePostRequest) o;
                    StateRequest stateRequest = request.getStateRequest();
                    stateRequest.shareData();
                }
            });

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", artistBean.getSessionId());
                jsonObject.put("longitude", longitude);
                jsonObject.put("latitude", latitude);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = createRequestBodyText("jsonBody", jsonObject.toString());
            postRequest.createAndSendRequest(BuildConfig.URL, requestBody, START_PERFORMANCE_REQUEST_ID);

        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public void updatePerformanceRequest(Context context, UserArtistBean artistBean, double longitude, double latitude, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)) {
            UpdatePerformancePostRequest postRequest = new UpdatePerformancePostRequest(this, R.string.request_fail_message);
            postRequest.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    UpdatePerformancePostRequest request = (UpdatePerformancePostRequest) o;
                    StateRequest stateRequest = request.getStateRequest();
                    stateRequest.shareData();
                }
            });

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", artistBean.getSessionId());
                jsonObject.put("longitude", longitude);
                jsonObject.put("latitude", latitude);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = createRequestBodyText("jsonBody", jsonObject.toString());
            postRequest.createAndSendRequest(BuildConfig.URL, requestBody, UPDATE_PERFORMANCE_REQUEST_ID);

        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public void checkActivePerformanceRequest(Context context, UserArtistBean artistBean, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)) {
            UpdatePerformancePostRequest postRequest = new UpdatePerformancePostRequest(this, R.string.request_fail_message);
            postRequest.addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    UpdatePerformancePostRequest request = (UpdatePerformancePostRequest) o;
                    StateRequest stateRequest = request.getStateRequest();
                    stateRequest.shareData();
                }
            });

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", artistBean.getSessionId());
                jsonObject.put("longitude", 1);
                jsonObject.put("latitude", 1);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = createRequestBodyText("jsonBody", jsonObject.toString());
            postRequest.createAndSendRequest(BuildConfig.URL, requestBody, CHECK_PERFORMANCE_REQUEST_ID);

        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public boolean isPerformanceStart() {
        return mIsPerformanceStart;
    }

    public void setPerformanceStart(boolean performanceStart) {
        mIsPerformanceStart = performanceStart;
    }

    public boolean isFirstRequest() {
        return mIsFirstRequest;
    }

    public void setFirstRequest(boolean firstRequest) {
        mIsFirstRequest = firstRequest;
    }

    private RequestBody createRequestBodyText(String key, String value) {
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), value);
    }

    public void googleApiConnect() {
        getViewState().onGoogleApiConnect();
    }
}
