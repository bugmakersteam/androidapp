package ru.music_boom_app.views.main_page;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.models.beans.UserListenerBean;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.views.base.BaseApplicationActivity;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;
import ru.music_boom_app.views.main_page.artist_work_area.ArtistWorkAreaFragment;
import ru.music_boom_app.views.main_page.choose_donate_amount.ChooseDonateAmountDialogFragment;
import ru.music_boom_app.views.main_page.drawer_menu.about_app.AboutAppFragment;
import ru.music_boom_app.views.main_page.drawer_menu.feedback.FeedbackFragment;
import ru.music_boom_app.views.main_page.finance.artist_finance.ArtistFinanceFragment;
import ru.music_boom_app.views.main_page.finance.listener_finance.ListenerFinanceFragment;
import ru.music_boom_app.views.main_page.perfomance.PerformanceFragment;
import ru.music_boom_app.views.main_page.perfomance.PerformancePresenter;
import ru.music_boom_app.views.main_page.profile.artist_profile.creativity.ArtistCreativityFragment;
import ru.music_boom_app.views.main_page.profile.artist_profile.profile.ArtistProfileFragment;
import ru.music_boom_app.views.main_page.profile.listener_profile.ListenerProfileFragment;

/**
 * @author Markin Andrey on 02.03.2018.
 */

public class MainPageActivity extends BaseApplicationActivity
        implements MainPageActivityView, ChooseDonateAmountDialogFragment.ChooseAmountDialogCallback {

    private static final String USER_BEAN = "userBean";
    private static final String ARTIST_TYPE = "ARTIST";
    private static final String LISTENER_TYPE = "LISTENER";
    private static final String DONATE_DIALOG_TAG = "donate_dialog_tag";
    private static final String PERFORMANCE_TAG = "performance_tag";
    private static final String DIALOG_DEVELOPMENT = "development";
    private static final String ERROR_OPERATION = "error_operation";
    private static final String SUCCESS_OPERATION = "success_operation";
    private static final String ARTIST_PROFILE_TAG = "artist_profile_teg";

    private static final List<String> TABS_NAME = new ArrayList<String>() {
        {
            add("Профиль");
            add("Деятельность");
            add("Финансы");
        }
    };
    private static final String FEEDBACK_TAG = "feedback_tag";
    private static final String ABOUT_APP_TAG = "feedback_tag";

    private List<Fragment> TABS_FRAGMENTS = new ArrayList<Fragment>();

    private BottomNavigationViewEx mBottomNavigation;
    private UserArtistBean mArtistBean;
    private UserListenerBean mListenerBean;
    private Button mDonateButton;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private UserBean mUserBean;
    private String mUserRecipientId;
    private BigDecimal mDonateAmount;
    private TabLayout mTabLayout;
    private ViewPager mPager;

    @Inject
    NetworkUtils mNetworkUtils;
    @InjectPresenter
    MainPageActivityPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(this).inject(this);
        setContentView(R.layout.activity_main_page);
        mUserBean = (UserBean) getIntent().getParcelableExtra(USER_BEAN);
        initBottomNav(mUserBean);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        mNavigationView = (NavigationView) findViewById(R.id.navigation);

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.feedback:
                        if (mTabLayout != null && mPager != null) {
                            mTabLayout.setVisibility(View.GONE);
                            mPager.setVisibility(View.GONE);
                        }
                        item.setChecked(false);
                        mDrawerLayout.closeDrawers();
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_container, FeedbackFragment.newInstance(mUserBean), FEEDBACK_TAG)
                                .addToBackStack(null).commit();
                        break;
                    case R.id.evaluation:
                        if (mTabLayout != null && mPager != null) {
                            mTabLayout.setVisibility(View.GONE);
                            mPager.setVisibility(View.GONE);
                        }
                        item.setChecked(false);
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("market://details?id=ru.music_boom_app"));
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        break;

                    case R.id.about_app:
                        if (mTabLayout != null && mPager != null) {
                            mTabLayout.setVisibility(View.GONE);
                            mPager.setVisibility(View.GONE);
                        }
                        item.setChecked(false);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_container, new AboutAppFragment(), ABOUT_APP_TAG)
                                .addToBackStack(null).commit();
                        mDrawerLayout.closeDrawers();
                        break;
                    case R.id.logout:
                        item.setChecked(false);
                        mDrawerLayout.closeDrawers();
                        mPresenter.startLogout();
                        break;
                }
                return true;
            }
        });

        if (ARTIST_TYPE.equals(mUserBean.getUserType())) {
            mArtistBean = (UserArtistBean) mUserBean;
        }
        if (LISTENER_TYPE.equals(mUserBean.getUserType())) {
            mListenerBean = (UserListenerBean) mUserBean;
        }
        mDonateButton = (Button) findViewById(R.id.donateButton);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().findFragmentByTag(PerformancePresenter.GUEST_PROFILE) != null){
            getSupportFragmentManager().popBackStack();
            return;
        }

        if (mArtistBean != null) {
            int workArea = 1;
            if (workArea == mBottomNavigation.getCurrentItem()) {
                mPresenter.startLogout();
                return;
            } else {
                mBottomNavigation.setCurrentItem(1);
                return;
            }
        }

        if (mListenerBean != null) {
            int performanceItem = 2;
            if (performanceItem == mBottomNavigation.getCurrentItem()) {
                mPresenter.startLogout();
                return;
            } else {
                mBottomNavigation.setCurrentItem(2);
                return;
            }
        }

        if (getSupportFragmentManager().findFragmentByTag(FEEDBACK_TAG) != null
                || getSupportFragmentManager().findFragmentByTag(ABOUT_APP_TAG) != null) {
            mBottomNavigation.setCurrentItem(1);
            return;
        }

        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (ChooseDonateAmountDialogFragment.PAYMENT_REQUEST_CODE == requestCode && RESULT_OK == resultCode) {
//            String invoiceId = data.getStringExtra(PaymentActivity.EXTRA_INVOICE_ID);
//            mPresenter.donateTransaction(this,
//                    mUserBean.getSessionId(),
//                    String.valueOf(mUserBean.getId()),
//                    mUserRecipientId,
//                    mDonateAmount,
//                    invoiceId,
//                    mNetworkUtils);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(dialogErrorMessageId),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getSupportFragmentManager(), ERROR_OPERATION);
    }

    @Override
    public void onShowListenerProfile() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, ListenerProfileFragment.newInstance(mListenerBean))
                .commit();
    }

    @Override
    public void onShowListenerFinance() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, ListenerFinanceFragment.newInstance(mListenerBean))
                .commit();
    }

    @Override
    public void onShowPerformances() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, PerformanceFragment.newInstance(mUserBean), PERFORMANCE_TAG)
                .commit();
    }

    @Override
    public void onShowLocator() {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.functional_on_development_title),
                getString(R.string.functional_on_development_text),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getSupportFragmentManager(), DIALOG_DEVELOPMENT);
    }

    @Override
    public void onShowRootFragmentArtistProfile() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        }

        if (TABS_FRAGMENTS.size() == 0) {
            TABS_FRAGMENTS.add(ArtistProfileFragment.newInstance(mArtistBean));
            TABS_FRAGMENTS.add(ArtistCreativityFragment.newInstance(mArtistBean));
            TABS_FRAGMENTS.add(ArtistFinanceFragment.newInstance(mArtistBean));
        }

        mPager = (ViewPager) findViewById(R.id.view_pager);
        mPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return TABS_FRAGMENTS.get(position);
            }

            @Override
            public int getCount() {
                return TABS_FRAGMENTS.size();
            }

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                return TABS_NAME.get(position);
            }
        });
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mTabLayout.setupWithViewPager(mPager);

        mTabLayout.setVisibility(View.VISIBLE);
        mPager.setVisibility(View.VISIBLE);
        mPager.setCurrentItem(0);
    }

    @Override
    public void onShowArtistWorkArea() {
        if (mTabLayout != null && mPager != null) {
            mTabLayout.setVisibility(View.GONE);
            mPager.setVisibility(View.GONE);
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, ArtistWorkAreaFragment.newInstance(mArtistBean), PERFORMANCE_TAG)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onStartLogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.logout_dialog_title);
        builder.setMessage(R.string.logout_dialog_message);
        builder.setPositiveButton(R.string.dialog_positive_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mPresenter.logout(MainPageActivity.this, mUserBean, mNetworkUtils);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(R.string.dialog_negative_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onFinishApp() {
        finishAndRemoveTask();
    }

    @Override
    public void onChosenSum(String recipientId, BigDecimal sum) {
        mUserRecipientId = recipientId;
        mDonateAmount = sum;
    }

    @Override
    public void onSuccessTransaction() {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.success_title),
                getString(R.string.success_operation_message),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getSupportFragmentManager(), SUCCESS_OPERATION);
    }

    private void initBottomNav(UserBean userBean) {
        mBottomNavigation = (BottomNavigationViewEx) findViewById(R.id.bottom_navigation);

        if (ARTIST_TYPE.equals(userBean.getUserType())) {
            mBottomNavigation.inflateMenu(R.menu.artist_bottom_nav_menu);
            mBottomNavigation.enableAnimation(false);
            mBottomNavigation.enableShiftingMode(false);
            mBottomNavigation.enableItemShiftingMode(false);
            mBottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.profile_item:
                            mPresenter.showRootFragmentArtistProfile();
                            break;
                        case R.id.work_area_item:
                            if (mTabLayout != null && mPager != null) {
                                mTabLayout.setVisibility(View.GONE);
                                mPager.setVisibility(View.GONE);
                            }
                            mPresenter.showArtistWorkArea();

                            break;
                        case R.id.performance_item:
                            if (mTabLayout != null && mPager != null) {
                                mTabLayout.setVisibility(View.GONE);
                                mPager.setVisibility(View.GONE);
                            }
                            mPresenter.showPerformances();
                    }
                    return true;
                }
            });
            mBottomNavigation.setCurrentItem(0);
        }
        if (LISTENER_TYPE.equals(userBean.getUserType())) {
            mBottomNavigation.inflateMenu(R.menu.listener_bottom_nav_menu);
            mBottomNavigation.enableAnimation(false);
            mBottomNavigation.enableShiftingMode(false);
            mBottomNavigation.enableItemShiftingMode(false);
            mBottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                    switch (item.getItemId()) {
                        case R.id.profile_item:
                            mPresenter.showListenerProfile();
                            break;
                        case R.id.finance_item:
                            mPresenter.showListenerFinance();
                            break;
                        case R.id.performance_item:
                            mPresenter.showPerformances();
                            break;
                        case R.id.locator_item:
                            mPresenter.showLocator();
                            break;
                    }
                    return true;
                }
            });
            mBottomNavigation.setCurrentItem(2);
        }
    }

    public void showDonateButton(UserBean bean, String userReceiveId) {
        mDonateButton.setVisibility(View.VISIBLE);
        mDonateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseDonateAmountDialogFragment dialogFragment = ChooseDonateAmountDialogFragment.newInstance(userReceiveId, mUserBean);
                dialogFragment.show(getSupportFragmentManager(), DONATE_DIALOG_TAG);
            }
        });
    }

    public void hideDonateButton() {
        mDonateButton.setVisibility(View.GONE);
    }
}
