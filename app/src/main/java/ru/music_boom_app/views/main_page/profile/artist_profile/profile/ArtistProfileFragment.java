package ru.music_boom_app.views.main_page.profile.artist_profile.profile;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.PermissionsUtils;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;
import ru.music_boom_app.views.base.BaseApplicationFragment;
import ru.music_boom_app.views.main_page.profile.listener_profile.ListenerProfilePresenter;

import static android.app.Activity.RESULT_OK;

/**
 * @author Markin Andrey on 21.04.2018.
 */
public class ArtistProfileFragment extends BaseApplicationFragment implements ArtistProfileView {
    public static final int CHOICE_IMAGE_DIALOG_RESULT = 301;
    public static final int CREATE_PHOTO_DIALOG_RESULT = 302;
    private static final String ERROR_OPERATION = "error_operation";
    private static final String SUCCESS_OPERATION = "success_operation";
    private static final String USER_BEAN_EXTRA = "user_bean_extra";
    private static final String FULL_NAME = "FULLNAME";
    private static final String NICKNAME = "NICKNAME";

    private UserArtistBean mArtistBean;


    @Inject
    PermissionsUtils mPermissionsUtils;
    @Inject
    NetworkUtils mNetworkUtils;
    @InjectPresenter
    ArtistProfilePresenter mPresenter;
    //region UI
    @BindView(R.id.app_bar)
    AppBarLayout mAppBarLayout;
    @BindView(R.id.user_avatar)
    ImageView mAvatarView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.fab)
    FloatingActionButton mCreateAvatarFAB;

    @BindView(R.id.creativity_title)
    TextView mSurnameTextView;
    @BindView(R.id.creativity_spinner)
    TextInputLayout mSurnameLayout;
    @BindView(R.id.creativity)
    EditText mSurnameEditText;

    @BindView(R.id.name_text_view)
    TextView mNameTextView;
    @BindView(R.id.name_layout)
    TextInputLayout mNameLayout;
    @BindView(R.id.name)
    EditText mNameEditText;

    @BindView(R.id.patronymic_text_view)
    TextView mPatronymicTextView;
    @BindView(R.id.patronymic_layout)
    TextInputLayout mPatronymicLayout;
    @BindView(R.id.patronymic)
    EditText mPatronymicEditText;

    @BindView(R.id.nickname_text_view)
    TextView mNicknameTextView;
    @BindView(R.id.nickname_layout)
    TextInputLayout mNicknameLayout;
    @BindView(R.id.nickname)
    EditText mNicknameEditText;

    @BindView(R.id.birthday_text_view)
    TextView mBirthdayTextView;
    @BindView(R.id.birthday_layout)
    TextInputLayout mBirthdayLayout;
    @BindView(R.id.birthday)
    EditText mBirthdayEditText;

    @BindView(R.id.country_text_view)
    TextView mCountryTextView;
    @BindView(R.id.country_layout)
    TextInputLayout mCountryLayout;
    @BindView(R.id.country)
    AutoCompleteTextView mCountryEditText;

    @BindView(R.id.city_text_view)
    TextView mCityTextView;
    @BindView(R.id.city_layout)
    TextInputLayout mCityLayout;
    @BindView(R.id.city)
    AutoCompleteTextView mCityEditText;

    @BindView(R.id.phone_text_view)
    TextView mPhoneTitleTextView;
    @BindView(R.id.phone_number)
    TextView mPhoneTextView;

    @BindView(R.id.email_text_view)
    TextView mEmailTextView;
    @BindView(R.id.email_address_layout)
    TextInputLayout mEmailLayout;
    @BindView(R.id.email_address)
    EditText mEmailEditText;

    @BindView(R.id.vk_link_text_view)
    TextView mVkLinkTextView;
    @BindView(R.id.vk_link_layout)
    TextInputLayout mVkLinkLayout;
    @BindView(R.id.vk_link)
    EditText mVkLinkEditText;

    @BindView(R.id.wapp_text_view)
    TextView mWappTextView;
    @BindView(R.id.wapp_layout)
    TextInputLayout mWappLayout;
    @BindView(R.id.wapp)
    EditText mWappEditText;

    @BindView(R.id.radioGroup)
    RadioGroup mUserPublicNameTypeRadioGroup;

    @BindView(R.id.change_password_button)
    Button mChangePasswordButton;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;
    //endregion

    private RadioButton mFullNameRadioButton;
    private RadioButton mNickNameRadioButton;
    private String mUserPublicName;
    private String mNewAvatarPath;
    private Snackbar mSaveAvatarSnackbar;

    public static ArtistProfileFragment newInstance(UserArtistBean bean) {
        Bundle args = new Bundle();
        args.putParcelable(USER_BEAN_EXTRA, bean);
        ArtistProfileFragment fragment = new ArtistProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(getContext()).inject(this);
        mArtistBean = (UserArtistBean) getArguments().getParcelable(USER_BEAN_EXTRA);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.artist_profile_fragment, container, false);
        ButterKnife.bind(this, view);
        mToolbar.setTitle(R.string.user_profile_title);
        mToolbar.inflateMenu(R.menu.listener_profile_toolbar_menu);

        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.edit:
                        mPresenter.editFields();
                        item.setVisible(false);
                        mToolbar.getMenu().getItem(1).setVisible(true);
                        break;
                    case R.id.save:
                        mPresenter.saveFields();
                        item.setVisible(false);
                        mToolbar.getMenu().getItem(0).setVisible(true);
                        break;
                }
                return true;
            }
        });

        mUserPublicNameTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.fullname:
                        mUserPublicName = FULL_NAME;
                        break;

                    case R.id.nick:
                        mUserPublicName = NICKNAME;
                        break;
                }
            }
        });
        int idRadioButton = mUserPublicNameTypeRadioGroup.getCheckedRadioButtonId();
        if (idRadioButton == R.id.fullname) {
            mUserPublicName = FULL_NAME;
        } else {
            mUserPublicName = NICKNAME;
        }
        mFullNameRadioButton = (RadioButton) mUserPublicNameTypeRadioGroup.findViewById(R.id.fullname);
        mNickNameRadioButton = (RadioButton) mUserPublicNameTypeRadioGroup.findViewById(R.id.nick);

        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    mToolbar.setVisibility(View.VISIBLE);
                    isShow = true;
                } else if (isShow) {
                    mToolbar.setVisibility(View.GONE);
                    isShow = false;
                }
            }
        });

        mCreateAvatarFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.createNewAvatar(getActivity(), ArtistProfileFragment.this);
            }
        });

        String[] countries = getResources().getStringArray(R.array.countriesArray);
        mCountryEditText.setAdapter(
                new ArrayAdapter<String>(getContext(), R.layout.auto_complete_list_item, countries)
        );
        mCountryEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkCountryText(mCountryEditText.getText().toString());
                } else {
                    mCountryLayout.setErrorEnabled(false);
                }
            }
        });

        String[] cities = getResources().getStringArray(R.array.citiesArray);
        mCityEditText.setAdapter(
                new ArrayAdapter<String>(getContext(), R.layout.auto_complete_list_item, cities)
        );
        mCityEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkCityText(mCityEditText.getText().toString());
                } else {
                    mCityLayout.setErrorEnabled(false);
                }
            }
        });

        mNameEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkNameText(mNameEditText.getText().toString());
                } else {
                    mNameLayout.setErrorEnabled(false);
                }
            }
        });

        mSurnameEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkSurnameText(mSurnameEditText.getText().toString());
                } else {
                    mSurnameLayout.setErrorEnabled(false);
                }
            }
        });

        mNicknameEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkNicknameText(mNicknameEditText.getText().toString());
                } else {
                    mNicknameLayout.setErrorEnabled(false);
                }
            }
        });

        mBirthdayEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkBirthdayText(mBirthdayEditText.getText().toString());
                } else {
                    mBirthdayLayout.setErrorEnabled(false);
                    mPresenter.showDataPickerDialog(getContext());
                }
            }
        });

        mChangePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.showChangePasswordDialog(getActivity(), mArtistBean);
            }
        });

        CoordinatorLayout layout = (CoordinatorLayout) view.findViewById(R.id.main_container);
        mSaveAvatarSnackbar = Snackbar
                .make(layout, "Сохранить аватар", Snackbar.LENGTH_INDEFINITE)
                .setAction("СОХРАНИТЬ", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPresenter.uploadAvatarToServer(getContext(), mNewAvatarPath, mArtistBean.getSessionId(), mNetworkUtils);
                    }
                });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setCurrentValues();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CHOICE_IMAGE_DIALOG_RESULT:
                choiceImage();
                break;

            case CREATE_PHOTO_DIALOG_RESULT:
                createPhoto();
                break;

            case ListenerProfilePresenter.CHOOSE_IMAGE_ACTIVITY_RESULT:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    mNewAvatarPath = getRealPathFromURI(imageUri);
                    mAvatarView.setImageURI(imageUri);
                    mSaveAvatarSnackbar.show();
                }
                break;

            case ListenerProfilePresenter.CREATE_PHOTO_ACTIVITY_RESULT:
                if (resultCode == RESULT_OK) {
                    mNewAvatarPath = mPresenter.getPhotoAvatarAbsolutePath();
                    Bitmap photoAvatar = BitmapFactory.decodeFile(mNewAvatarPath);
                    mAvatarView.setImageBitmap(photoAvatar);
                    mSaveAvatarSnackbar.show();
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case ArtistProfilePresenter.CHOICE_IMAGE_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPresenter.choiceImage(getActivity(), this, mPermissionsUtils);
                }
                break;

            case ArtistProfilePresenter.CREATE_PHOTO_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    mPresenter.createPhoto(getActivity(), this, mPermissionsUtils);
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void setCurrentValues() {
        if (!mPresenter.isAvatarDownloaded()) {
            mPresenter.downloadAvatar(mArtistBean);
        }
        mSurnameEditText.setText(mArtistBean.getSurname());
        mNameEditText.setText(mArtistBean.getName());
        if (!TextUtils.isEmpty(mArtistBean.getPatronymic())) {
            mPatronymicEditText.setText(mArtistBean.getPatronymic());
        }
        mNicknameEditText.setText(mArtistBean.getNickname());
        mBirthdayEditText.setText(mArtistBean.getBirthday());
        mCountryEditText.setText(mArtistBean.getCountry());
        mCityEditText.setText(mArtistBean.getCity());
        mPhoneTextView.setText(mArtistBean.getPhoneNumber());
        mEmailEditText.setText(mArtistBean.getEmail());
        if (!TextUtils.isEmpty(mArtistBean.getVKLink())) {
            mVkLinkEditText.setText(mArtistBean.getVKLink());
        }
        if (!TextUtils.isEmpty(mArtistBean.getWhatsAppNumber())) {
            mWappEditText.setText(mArtistBean.getWhatsAppNumber());
        }
        //ToDo запросить у бэка то что сейчас отображается в качестве Имени в общем профиля. Необходимо для контроля состояния RadioButton;
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(dialogErrorMessageId),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), ERROR_OPERATION);
    }

    @Override
    public void onAvatarDownloaded(Bitmap bitmap) {
        mAvatarView.setImageBitmap(bitmap);
    }

    @Override
    public void onEditFields() {
        mSurnameEditText.setEnabled(true);
        mSurnameEditText.setTextColor(getResources().getColor(android.R.color.black));
        mNameEditText.setEnabled(true);
        mNameEditText.setTextColor(getResources().getColor(android.R.color.black));
        mPatronymicEditText.setEnabled(true);
        mPatronymicEditText.setTextColor(getResources().getColor(android.R.color.black));
        mNicknameEditText.setEnabled(true);
        mNicknameEditText.setTextColor(getResources().getColor(android.R.color.black));
        mBirthdayEditText.setEnabled(true);
        mBirthdayEditText.setTextColor(getResources().getColor(android.R.color.black));
        mCountryEditText.setEnabled(true);
        mCountryEditText.setTextColor(getResources().getColor(android.R.color.black));
        mCityEditText.setEnabled(true);
        mCityEditText.setTextColor(getResources().getColor(android.R.color.black));
        mEmailEditText.setEnabled(true);
        mEmailEditText.setTextColor(getResources().getColor(android.R.color.black));
        mVkLinkEditText.setEnabled(true);
        mVkLinkEditText.setTextColor(getResources().getColor(android.R.color.black));
        mWappEditText.setEnabled(true);
        mWappEditText.setTextColor(getResources().getColor(android.R.color.black));
        mFullNameRadioButton.setEnabled(true);
        mNickNameRadioButton.setEnabled(true);

    }

    @Override
    public void onSaveFields() {
        mSurnameEditText.setEnabled(false);
        mSurnameEditText.setTextColor(getResources().getColor(R.color.primary_material_music_boom));
        mNameEditText.setEnabled(false);
        mNameEditText.setTextColor(getResources().getColor(R.color.primary_material_music_boom));
        mNicknameEditText.setEnabled(false);
        mNicknameEditText.setTextColor(getResources().getColor(R.color.primary_material_music_boom));
        mPatronymicEditText.setEnabled(false);
        mPatronymicEditText.setTextColor(getResources().getColor(R.color.primary_material_music_boom));
        mBirthdayEditText.setEnabled(false);
        mBirthdayEditText.setTextColor(getResources().getColor(R.color.primary_material_music_boom));
        mCountryEditText.setEnabled(false);
        mCountryEditText.setTextColor(getResources().getColor(R.color.primary_material_music_boom));
        mCityEditText.setEnabled(false);
        mCityEditText.setTextColor(getResources().getColor(R.color.primary_material_music_boom));
        mEmailEditText.setEnabled(false);
        mEmailEditText.setTextColor(getResources().getColor(R.color.primary_material_music_boom));
        mVkLinkEditText.setEnabled(false);
        mVkLinkEditText.setTextColor(getResources().getColor(R.color.primary_material_music_boom));
        mWappEditText.setEnabled(false);
        mWappEditText.setTextColor(getResources().getColor(R.color.primary_material_music_boom));
        mFullNameRadioButton.setEnabled(false);
        mNickNameRadioButton.setEnabled(false);

        mPresenter.updatePersonal(
                getContext(),
                mArtistBean.getSessionId(),
                mSurnameEditText.getText().toString(),
                mNameEditText.getText().toString(),
                mNicknameEditText.getText().toString(),
                mPatronymicEditText.getText().toString(),
                mBirthdayEditText.getText().toString(),
                mCountryEditText.getText().toString(),
                mCityEditText.getText().toString(),
                mEmailEditText.getText().toString(),
                mVkLinkEditText.getText().toString(),
                mWappEditText.getText().toString(),
                mUserPublicName,
                mNetworkUtils
        );
    }

    @Override
    public void showNameLayoutException(int stringExceptionId) {
        mNameLayout.setErrorEnabled(true);
        mNameLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showSurnameLayoutException(int stringExceptionId) {
        mSurnameLayout.setErrorEnabled(true);
        mSurnameLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showNicknameLayoutException(int stringExceptionId) {
        mNicknameLayout.setErrorEnabled(true);
        mNicknameLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showBirthdayLayoutException(int stringExceptionId) {
        mBirthdayLayout.setErrorEnabled(true);
        mBirthdayLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showCityLayoutException(int stringExceptionId) {
        mCityLayout.setErrorEnabled(true);
        mCityLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showCountryLayoutException(int stringExceptionId) {
        mCountryLayout.setErrorEnabled(true);
        mCountryLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showBirthdayDate(String date) {
        mBirthdayEditText.setText(date);
    }

    @Override
    public void onSuccessAvatarUploaded() {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.avatar_success_uploaded_title),
                getString(R.string.avatar_success_uploaded_text),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), SUCCESS_OPERATION);
    }

    @Override
    public void onSuccessPersonalFieldsUpdated() {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.avatar_success_uploaded_title),
                getString(R.string.personal_fields_success_updated_text),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), SUCCESS_OPERATION);
    }

    public void choiceImage() {
        mPresenter.choiceImage(getActivity(), this, mPermissionsUtils);
    }

    public void createPhoto() {
        mPresenter.createPhoto(getActivity(), this, mPermissionsUtils);
    }

    private String getRealPathFromURI(Uri contentURI) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(contentURI);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = { MediaStore.Images.Media.DATA };

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = getContext().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{ id }, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }
}
