package ru.music_boom_app.views.main_page.profile.listener_profile.change_password_dialog;

import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Markin Andrey on 04.04.2018.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface ChangePasswordDialogView extends MvpView {
    void showCurrentPasswordLayoutException(@StringRes int stringExceptionId);

    void showNewPasswordLayoutException(@StringRes int stringExceptionId);

    void showConfirmedNewPasswordLayoutException(@StringRes int stringExceptionId);

    void showFailDialog(int dialogErrorMessageId);

    void onSuccessChangePassword();
}
