package ru.music_boom_app.views.main_page;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * Created by markin_a on 21.03.2018.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface MainPageActivityView extends MvpView {
    void showProgress();
    void hideProgress();
    void showFailDialog(int dialogErrorMessageId);
    void onShowListenerProfile();
    void onShowListenerFinance();
    void onShowPerformances();
    void onShowLocator();
    void onShowArtistWorkArea();
    void onStartLogout();
    void onFinishApp();
    void onShowRootFragmentArtistProfile();
    void onSuccessTransaction();
}
