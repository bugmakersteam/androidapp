package ru.music_boom_app.views.main_page.artist_work_area;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.utils.NetworkUtils;

/**
 * @author Markin Andrey on 30.04.2018.
 */
public class MyService extends Service {
    private static final String TAG = "BOOMBOOMTESTGPS";
    private static final String USER_BEAN = "user_bean";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 30000;
    private static final float LOCATION_DISTANCE = 0f;
    private UserArtistBean mArtistBean;
    private Handler mActiveUserRequestHandler;
    private Runnable mRunnable;

    private boolean mIsFirstRequest = true;
    @Inject
    NetworkUtils mNetworkUtils;

    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;

        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.e(TAG, "onLocationChanged: " + location);
            if (mIsFirstRequest) {
                Log.d("perform", "first");
                startPerformanceRequest(getApplicationContext(),
                        mArtistBean,
                        location.getLongitude(),
                        location.getLatitude(),
                        mNetworkUtils
                );
                mIsFirstRequest = false;
            } else {
                Log.d("perform", "update");
                updatePerformanceRequest(getApplicationContext(),
                        mArtistBean,
                        location.getLongitude(),
                        location.getLatitude(),
                        mNetworkUtils
                );
            }

            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            String id = "my_channel_01";
            CharSequence name = "notificate_chanel";

            NotificationChannel mChannel = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                String description = "performance_chanel";
                int importance = NotificationManager.IMPORTANCE_LOW;
                mChannel = new NotificationChannel(id, name, importance);
                mChannel.setDescription(description);
                mChannel.enableLights(true);
                mChannel.setLightColor(Color.RED);
                mNotificationManager.createNotificationChannel(mChannel);
            }

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);
            Notification notification = buildForegroundNotification();
            notificationManager.notify(121, notification);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        mArtistBean = (UserArtistBean) intent.getParcelableExtra(USER_BEAN);

        mActiveUserRequestHandler = new Handler();

        mRunnable = new Runnable() {
            @Override
            public void run() {

                initializeLocationManager();
                try {
                    mLocationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                            mLocationListeners[1]);
                } catch (java.lang.SecurityException ex) {
                    Log.i(TAG, "fail to request location update, ignore", ex);
                } catch (IllegalArgumentException ex) {
                    Log.d(TAG, "network provider does not exist, " + ex.getMessage());
                }
                try {
                    mLocationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                            mLocationListeners[0]);
                } catch (java.lang.SecurityException ex) {
                    Log.i(TAG, "fail to request location update, ignore", ex);
                } catch (IllegalArgumentException ex) {
                    Log.d(TAG, "gps provider does not exist " + ex.getMessage());
                }
                PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
                PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                        "MyWakelockTag");
                wakeLock.acquire(10*60*1000L /*10 minutes*/);
                mActiveUserRequestHandler.postDelayed(this, 30000);
            }
        };
        mRunnable.run();
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        MusicBoomApplication.getComponent(getApplicationContext()).inject(this);

    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
        mActiveUserRequestHandler.removeCallbacks(mRunnable);

        String id = "my_channel_01";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.deleteNotificationChannel(id);
        }
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    public void startPerformanceRequest(Context context, UserArtistBean artistBean, double longitude, double latitude, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)) {

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .connectTimeout(2, TimeUnit.MINUTES)
                    .readTimeout(2, TimeUnit.MINUTES)
                    .writeTimeout(2, TimeUnit.MINUTES)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.URL)
                    .client(client)
                    .build();

            IStartPerformancePostRequest request = retrofit.create(IStartPerformancePostRequest.class);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", artistBean.getSessionId());
                jsonObject.put("longitude", longitude);
                jsonObject.put("latitude", latitude);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = createRequestBodyText("jsonBody", jsonObject.toString());
            request.basePostRequest(requestBody).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        } else {

        }
    }


    public void updatePerformanceRequest(Context context, UserArtistBean artistBean, double longitude, double latitude, NetworkUtils networkUtils) {
        if (networkUtils.checkConnection(context)) {

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .connectTimeout(2, TimeUnit.MINUTES)
                    .readTimeout(2, TimeUnit.MINUTES)
                    .writeTimeout(2, TimeUnit.MINUTES)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.URL)
                    .client(client)
                    .build();

            IUpdatePerformancePostRequest request = retrofit.create(IUpdatePerformancePostRequest.class);

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sessionId", artistBean.getSessionId());
                jsonObject.put("longitude", longitude);
                jsonObject.put("latitude", latitude);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = createRequestBodyText("jsonBody", jsonObject.toString());
            request.basePostRequest(requestBody).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        Intent resultIntent = new Intent();
                        resultIntent.setAction(ArtistWorkAreaFragment.STATE_PERFORM_BROADCAST_ACTION);
                        try {
                            resultIntent.putExtra(ArtistWorkAreaFragment.RESULT_BROADCAST, response.body().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        getApplicationContext().sendBroadcast(resultIntent);
                        Log.e(TAG, "Broadcast sent");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });

        } else {

        }
    }

    private Notification buildForegroundNotification() {
        NotificationCompat.Builder b = new NotificationCompat.Builder(this);
                b.setChannelId("my_channel_01")
                .setContentTitle("Идет выступление")
                .setContentText("Вы выступаете")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker("Выступление")
                .setPriority(Notification.PRIORITY_MAX)
                .setWhen(System.currentTimeMillis());

        return b.build();
    }

    private RequestBody createRequestBodyText(String key, String value) {
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), value);
    }
}