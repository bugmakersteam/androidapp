package ru.music_boom_app.views.main_page.artist_work_area;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Markin Andrey on 25.04.2018.
 */
@StateStrategyType(SingleStateStrategy.class)
public interface ArtistWorkAreaView extends MvpView {
    void onStartPerformance();
    void showFailDialog(int dialogErrorMessageId);
    void onSuccessStartPerformance();
    void onSuccessUpdatePerformance(String earnedMoney);
    void onSuccessStopPerformance(String earnedMoney);
    void onGoogleApiConnect();
    void onShowProgress();
    void onHideProgress();
    void onPerformanceExist();
    void onPerformanceNotExist();
}
