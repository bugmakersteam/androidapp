package ru.music_boom_app.views.main_page.profile.listener_profile;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;

import javax.inject.Inject;

import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserListenerBean;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.PermissionsUtils;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;
import ru.music_boom_app.views.base.BaseApplicationFragment;

import static android.app.Activity.RESULT_OK;

/**
 * Created by markin_a on 22.03.2018.
 */

public class ListenerProfileFragment extends BaseApplicationFragment implements ListenerProfileView {

    public static final int CHOICE_IMAGE_DIALOG_RESULT = 301;
    public static final int CREATE_PHOTO_DIALOG_RESULT = 302;
    private static final String USER_BEAN_EXTRA = "user_bean_extra";
    private static final String ERROR_OPERATION = "error_operation";
    private static final String SUCCESS_OPERATION = "success_operation";

    private Toolbar mToolbar;
    private AppBarLayout mAppBarLayout;
    private UserListenerBean mBean;
    private ImageView mUserAvatarImageView;
    private TextView mPhoneNumberTextView;
    private TextInputLayout mEmailLayout;
    private EditText mEmailEditText;
    private TextInputLayout mCountryLayout;
    private AutoCompleteTextView mCountryTextView;
    private TextInputLayout mCityLayout;
    private AutoCompleteTextView mCityTextView;
    private FloatingActionButton mFAB;
    private ProgressBar mProgressBar;
    private Snackbar mSaveAvatarSnackbar;
    private Button mChangePasswordButton;
    private String mNewAvatarPath;
    private boolean mIsEdit;

    @Inject
    NetworkUtils mNetworkUtils;
    @Inject
    PermissionsUtils mPermissionsUtils;

    @InjectPresenter
    ListenerProfilePresenter mPresenter;

    public static ListenerProfileFragment newInstance(UserListenerBean bean) {
        Bundle args = new Bundle();
        args.putParcelable(USER_BEAN_EXTRA, bean);
        ListenerProfileFragment fragment = new ListenerProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(getContext()).inject(this);
        Bundle bundle = getArguments();
        mBean = bundle.getParcelable(USER_BEAN_EXTRA);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.listener_profile_fragment, container, false);
        initViews(view);
        mToolbar.setTitle(R.string.user_profile_title);
        mToolbar.inflateMenu(R.menu.listener_profile_toolbar_menu);

        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.edit:
                        mPresenter.editFields();
                        item.setVisible(false);
                        mToolbar.getMenu().getItem(1).setVisible(true);
                        break;
                    case R.id.save:
                        mPresenter.saveFields();
                        item.setVisible(false);
                        mToolbar.getMenu().getItem(0).setVisible(true);
                        break;
                }
                return true;
            }
        });

        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    mToolbar.setVisibility(View.VISIBLE);
                    isShow = true;
                } else if (isShow) {
                    mToolbar.setVisibility(View.GONE);
                    isShow = false;
                }
            }
        });

        mFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.createNewAvatar(getActivity(), ListenerProfileFragment.this);
            }
        });
        CoordinatorLayout layout = (CoordinatorLayout) view.findViewById(R.id.main_container);
        mSaveAvatarSnackbar = Snackbar
                .make(layout, "Сохранить аватар", Snackbar.LENGTH_INDEFINITE)
                .setAction("СОХРАНИТЬ", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPresenter.uploadAvatarToServer(getContext(), mNewAvatarPath, mBean.getSessionId(), mNetworkUtils);
                    }
                });

//        mEmailEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!v.hasFocus()) {
//                    mPresenter.checkEmailField(mEmailEditText.getText().toString());
//                } else {
//                    mEmailLayout.setErrorEnabled(false);
//                }
//            }
//        });

        String[] countries = getResources().getStringArray(R.array.countriesArray);
        mCountryTextView.setAdapter(
                new ArrayAdapter<String>(getContext(), R.layout.auto_complete_list_item, countries)
        );
        mCountryTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkCountryText(mCountryTextView.getText().toString());
                } else {
                    mCountryLayout.setErrorEnabled(false);
                }
            }
        });

        String[] cities = getResources().getStringArray(R.array.citiesArray);
        mCityTextView.setAdapter(
                new ArrayAdapter<String>(getContext(), R.layout.auto_complete_list_item, cities)
        );
        mCityTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkCityText(mCityTextView.getText().toString());
                } else {
                    mCityLayout.setErrorEnabled(false);
                }
            }
        });

        mChangePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.showChangePasswordDialog(getActivity(), mBean);
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setCurrentValues();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CHOICE_IMAGE_DIALOG_RESULT:
                choiceImage();
                break;

            case CREATE_PHOTO_DIALOG_RESULT:
                createPhoto();
                break;

            case ListenerProfilePresenter.CHOOSE_IMAGE_ACTIVITY_RESULT:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    mNewAvatarPath = getRealPathFromURI(imageUri);
                    mUserAvatarImageView.setImageURI(imageUri);
                    mSaveAvatarSnackbar.show();
                }
                break;

            case ListenerProfilePresenter.CREATE_PHOTO_ACTIVITY_RESULT:
                if (resultCode == RESULT_OK) {
                    mNewAvatarPath = mPresenter.getPhotoAvatarAbsolutePath();
                    Bitmap photoAvatar = BitmapFactory.decodeFile(mNewAvatarPath);
                    mUserAvatarImageView.setImageBitmap(photoAvatar);
                    mSaveAvatarSnackbar.show();
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case ListenerProfilePresenter.CHOICE_IMAGE_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPresenter.choiceImage(getActivity(), this, mPermissionsUtils);
                }
                break;

            case ListenerProfilePresenter.CREATE_PHOTO_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    mPresenter.createPhoto(getActivity(), this, mPermissionsUtils);
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(dialogErrorMessageId),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), ERROR_OPERATION);
    }

    @Override
    public void onDownloadedAvatar(Bitmap drawable) {
        mUserAvatarImageView.setImageBitmap(drawable);
        mPresenter.setAvatarDownloaded(true);
    }

    @Override
    public void onSuccessAvatarUploaded() {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.avatar_success_uploaded_title),
                getString(R.string.avatar_success_uploaded_text),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), SUCCESS_OPERATION);
    }

    @Override
    public void onSuccessPersonalFieldsUpdated() {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.avatar_success_uploaded_title),
                getString(R.string.personal_fields_success_updated_text),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), SUCCESS_OPERATION);
    }

    @Override
    public void onEditFields() {
        mEmailEditText.setTextColor(getResources().getColor(android.R.color.black));
        mEmailEditText.setEnabled(true);
        mCountryTextView.setTextColor(getResources().getColor(android.R.color.black));
        mCountryTextView.setEnabled(true);
        mCityTextView.setTextColor(getResources().getColor(android.R.color.black));
        mCityTextView.setEnabled(true);
    }

    @Override
    public void onSaveFields() {
        mEmailEditText.setTextColor(getResources().getColor(R.color.primary_material_music_boom));
        mEmailEditText.setEnabled(false);
        mCountryTextView.setTextColor(getResources().getColor(R.color.primary_material_music_boom));
        mCountryTextView.setEnabled(false);
        mCityTextView.setTextColor(getResources().getColor(R.color.primary_material_music_boom));
        mCityTextView.setEnabled(false);
        mPresenter.updatePersonal(getContext(),
                mBean.getSessionId(),
                mEmailEditText.getText().toString(),
                mCountryTextView.getText().toString(),
                mCityTextView.getText().toString(),
                mNetworkUtils
        );
    }

    @Override
    public void showCountryLayoutException(int stringExceptionId) {
        mCountryLayout.setErrorEnabled(true);
        mCountryLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showCityLayoutException(int stringExceptionId) {
        mCityLayout.setErrorEnabled(true);
        mCityLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showEmailLayoutException(int stringExceptionId) {
        mEmailLayout.setErrorEnabled(true);
        mEmailLayout.setError(getString(stringExceptionId));
    }

    private void initViews(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mAppBarLayout = (AppBarLayout) view.findViewById(R.id.app_bar);
        mUserAvatarImageView = (ImageView) view.findViewById(R.id.user_avatar);
        mPhoneNumberTextView = (TextView) view.findViewById(R.id.phone_number);
        mEmailLayout = (TextInputLayout) view.findViewById(R.id.email_address_layout);
        mEmailEditText = (EditText) view.findViewById(R.id.email_address);
        mCountryLayout = (TextInputLayout) view.findViewById(R.id.country_layout);
        mCountryTextView = (AutoCompleteTextView) view.findViewById(R.id.country);
        mCityLayout = (TextInputLayout) view.findViewById(R.id.city_layout);
        mCityTextView = (AutoCompleteTextView) view.findViewById(R.id.city);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mFAB = (FloatingActionButton) view.findViewById(R.id.fab);
        mChangePasswordButton = (Button) view.findViewById(R.id.change_password_button);
    }

    private void setCurrentValues() {
        if (!mPresenter.isAvatarDownloaded()) {
            mPresenter.downloadAvatar(getContext(), mBean.getAvatar(), mNetworkUtils);
        }
        mPhoneNumberTextView.setText(mBean.getPhoneNumber());
        mEmailEditText.setText(mBean.getEmail());
        mCountryTextView.setText(mBean.getCountry());
        mCityTextView.setText(mBean.getCity());
    }

    public void choiceImage() {
        mPresenter.choiceImage(getActivity(), this, mPermissionsUtils);
    }

    public void createPhoto() {
        mPresenter.createPhoto(getActivity(), this, mPermissionsUtils);
    }

    private String getRealPathFromURI(Uri contentURI) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(contentURI);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = {MediaStore.Images.Media.DATA};

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = getContext().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{id}, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }
}
