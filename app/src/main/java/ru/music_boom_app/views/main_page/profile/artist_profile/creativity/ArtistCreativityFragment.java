package ru.music_boom_app.views.main_page.profile.artist_profile.creativity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;
import ru.music_boom_app.views.base.BaseApplicationFragment;
import ru.music_boom_app.views.main_page.profile.artist_profile.creativity.upload_photo.PhotoFragmentUpl;
import ru.music_boom_app.views.registration.artist_registration.step1.RegistrationArtistFragmentPresenterStep_1;

/**
 * @author Markin Andrey on 22.04.2018.
 */
public class ArtistCreativityFragment extends BaseApplicationFragment implements ArtistCreativityView {

    private static final String USER_ARTIST_BEAN = "user_artist_bean";
    private static final String ARTIST = "Артист";
    private static final String MUSICIAN = "Музыкант";
    private static final String ERROR_OPERATION = "error_operation";
    private static final String SUCCESS_OPERATION = "success_operation";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.creativity_title)
    TextView mCreativityTitle;
    @BindView(R.id.creativity_spinner)
    Spinner mCreativitySpinner;
    @BindView(R.id.edit_creativity)
    ImageButton mCreativityEdit;

    @BindView(R.id.instrument_title)
    TextView mInstrumentTitle;
    @BindView(R.id.instrument_layout)
    TextInputLayout mInstrumentLayout;
    @BindView(R.id.instrument)
    EditText mInstrumentEditText;
    @BindView(R.id.edit_instrument)
    ImageButton mInstrumentEdit;

    @BindView(R.id.genre_title)
    TextView mGenreTitle;
    @BindView(R.id.genre_layout)
    TextInputLayout mGenreLayout;
    @BindView(R.id.genre)
    EditText mGenreEditText;
    @BindView(R.id.edit_genre)
    ImageButton mGenreEdit;

    @BindView(R.id.ordered_switch)
    SwitchCompat mOrderedSwitch;
    @BindView(R.id.edit_ordered)
    ImageButton mOrderedEdit;
    @BindView(R.id.photo_view_pager)
    ViewPager mPhotoViewPager;
    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    @Inject
    NetworkUtils mNetworkUtils;
    @InjectPresenter
    ArtistCreativityPresenter mPresenter;

    private UserArtistBean mArtistBean;
    private int mEditedFieldNumber;

    public static ArtistCreativityFragment newInstance(UserArtistBean bean) {
        Bundle args = new Bundle();
        args.putParcelable(USER_ARTIST_BEAN, bean);
        ArtistCreativityFragment fragment = new ArtistCreativityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(getContext()).inject(this);
        mArtistBean = (UserArtistBean) getArguments().getParcelable(USER_ARTIST_BEAN);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.artist_creativity_fragment, container, false);
        ButterKnife.bind(this, view);
        initCurrentValue();
        initPhotoViewPager();
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        mToolbar.setTitle(R.string.user_creativity_title);
        mToolbar.inflateMenu(R.menu.user_creativity_menu);
        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.save:
                        saveFieldToServer();
                        item.setVisible(false);
                        break;
                }
                return true;
            }
        });

        String[] data = new String[]{ARTIST, MUSICIAN};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.feedback_types_list_item, data);
        mCreativitySpinner.setAdapter(adapter);
        mCreativityEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mToolbar.getMenu().getItem(0).setVisible(true);
                mCreativitySpinner.setEnabled(true);
                mEditedFieldNumber = 1;
                mCreativitySpinner.setClickable(true);
                mCreativityEdit.setVisibility(View.GONE);
                mInstrumentEdit.setEnabled(false);
                mGenreEdit.setEnabled(false);
                mOrderedEdit.setEnabled(false);
            }
        });

        mInstrumentEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mToolbar.getMenu().getItem(0).setVisible(true);
                mEditedFieldNumber = 2;
                mInstrumentEdit.setVisibility(View.GONE);
                mInstrumentEditText.setEnabled(true);
                mInstrumentEditText.setTextColor(getResources().getColor(android.R.color.black));
                mCreativityEdit.setEnabled(false);
                mGenreEdit.setEnabled(false);
                mOrderedEdit.setEnabled(false);
            }
        });

        mGenreEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mToolbar.getMenu().getItem(0).setVisible(true);
                mEditedFieldNumber = 3;
                mGenreEdit.setVisibility(View.GONE);
                mGenreEditText.setEnabled(true);
                mGenreEditText.setTextColor(getResources().getColor(android.R.color.black));
                mCreativityEdit.setEnabled(false);
                mInstrumentEdit.setEnabled(false);
                mOrderedEdit.setEnabled(false);
            }
        });

        mOrderedEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mToolbar.getMenu().getItem(0).setVisible(true);
                mEditedFieldNumber = 4;
                mOrderedEdit.setVisibility(View.GONE);
                mOrderedSwitch.setEnabled(true);
                mCreativityEdit.setEnabled(false);
                mInstrumentEdit.setEnabled(false);
                mGenreEdit.setEnabled(false);
            }
        });

        mInstrumentEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkInstrumentText(mInstrumentEditText.getText().toString());
                } else {
                    mInstrumentLayout.setErrorEnabled(false);
                }
            }
        });

        mGenreEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkGenreText(mGenreEditText.getText().toString());
                } else {
                    mGenreLayout.setErrorEnabled(false);
                }
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(dialogErrorMessageId),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), ERROR_OPERATION);
    }

    @Override
    public void onSuccessOperation(int dialogSuccessMessageID) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.success_title),
                getString(dialogSuccessMessageID),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), SUCCESS_OPERATION);
    }

    @Override
    public void showInstrumentLayoutException(int stringExceptionId) {
        mInstrumentLayout.setErrorEnabled(true);
        mInstrumentLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showGenreLayoutException(int stringExceptionId) {
        mGenreLayout.setErrorEnabled(true);
        mGenreLayout.setError(getString(stringExceptionId));
    }

    private void saveFieldToServer() {
        String result = null;
        switch (mEditedFieldNumber) {
            case 1:
                int position = mCreativitySpinner.getSelectedItemPosition();
                mCreativitySpinner.setEnabled(false);
                if (position == 0) {
                    result = RegistrationArtistFragmentPresenterStep_1.ARTIST_CREATIVITY;
                }
                if (position == 1) {
                    result = RegistrationArtistFragmentPresenterStep_1.MUSICIAN_CREATIVITY;
                }
                mCreativityEdit.setVisibility(View.VISIBLE);
                mCreativitySpinner.setClickable(false);
                mInstrumentEdit.setEnabled(true);
                mGenreEdit.setEnabled(true);
                mOrderedEdit.setEnabled(true);
                mPresenter.saveCreativity(getContext(),
                        mArtistBean.getSessionId(),
                        result,
                        mNetworkUtils);
                break;
            case 2:
                result = mInstrumentEditText.getText().toString();
                mInstrumentEditText.setTextColor(getResources().getColor(R.color.primary_material_music_boom));
                mInstrumentEditText.setEnabled(false);
                mInstrumentEdit.setVisibility(View.VISIBLE);
                mCreativityEdit.setEnabled(true);
                mGenreEdit.setEnabled(true);
                mOrderedEdit.setEnabled(true);
                mPresenter.saveInstrument(getContext(),
                        mArtistBean.getSessionId(),
                        result,
                        mNetworkUtils);
                break;

            case 3:
                result = mGenreEditText.getText().toString();
                mGenreEditText.setTextColor(getResources().getColor(R.color.primary_material_music_boom));
                mGenreEditText.setEnabled(false);
                mGenreEdit.setVisibility(View.VISIBLE);
                mCreativityEdit.setEnabled(true);
                mInstrumentEdit.setEnabled(true);
                mOrderedEdit.setEnabled(true);
                mPresenter.saveGenre(getContext(),
                        mArtistBean.getSessionId(),
                        result,
                        mNetworkUtils);
                break;

            case 4:
                boolean resultBool = mOrderedSwitch.isChecked();
                mOrderedEdit.setVisibility(View.GONE);
                mOrderedSwitch.setEnabled(false);
                mCreativityEdit.setEnabled(true);
                mInstrumentEdit.setEnabled(true);
                mGenreEdit.setEnabled(true);
                mPresenter.saveOrdered(getContext(),
                        mArtistBean.getSessionId(),
                        resultBool,
                        mNetworkUtils);
                break;
        }
    }

    private void initCurrentValue() {
        mCreativitySpinner.setPrompt(mArtistBean.getCreativity());
        mCreativitySpinner.setEnabled(false);
        if (RegistrationArtistFragmentPresenterStep_1.MUSICIAN_CREATIVITY.equals(mArtistBean.getCreativity())) {
            mInstrumentTitle.setVisibility(View.VISIBLE);
            mInstrumentLayout.setVisibility(View.VISIBLE);
            mInstrumentEditText.setVisibility(View.VISIBLE);
            mInstrumentEdit.setVisibility(View.VISIBLE);
            mInstrumentEditText.setText(mArtistBean.getInstrument());
        }
        mGenreEditText.setText(mArtistBean.getGenre());

        if (mArtistBean.isOrdered()) {
            mOrderedSwitch.setChecked(true);
        } else {
            mOrderedSwitch.setChecked(false);
        }
    }

    private void initPhotoViewPager() {
        mPhotoViewPager.setAdapter(new FragmentPagerAdapter(getActivity().getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return mArtistBean.getPhotos().size() + 1;
            }

            @Override
            public Fragment getItem(int position) {
                return PhotoFragmentUpl.newInstance(mArtistBean, mArtistBean.getPhotos(), position);
            }
        });

        if (mPhotoViewPager != null) {
            mPhotoViewPager.setCurrentItem(0);
            if (mArtistBean.getPhotos().size() > 0) {
                int size = mArtistBean.getPhotos().size();
                for (int i = 0; i < size; i++) {
                    mPhotoViewPager.setCurrentItem(i);
                }
            }
            mPhotoViewPager.setCurrentItem(0);
        }
    }
}
