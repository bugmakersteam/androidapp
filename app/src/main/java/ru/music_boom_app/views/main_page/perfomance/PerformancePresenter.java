package ru.music_boom_app.views.main_page.perfomance;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.arellomobile.mvp.InjectViewState;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.ActiveArtistMapBean;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.models.parsers.ActiveUserMapBeanParser;
import ru.music_boom_app.models.parsers.BaseResponseParser;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.PermissionsUtils;
import ru.music_boom_app.views.guest_artist_profile.GuestArtistProfileFragment;

/**
 * @author Markin Andrey on 08.04.2018.
 */
@InjectViewState
public class PerformancePresenter extends BasePresenter<PerformanceView> {
    public static final String GUEST_PROFILE = "guest_profile";
    public static final int GOOGLE_CLIENT_CONNECT_PERMISSION = 108;

    private final static int REQUEST_ERROR = 0;
    private static final int DEFAULT_ZOOM = 12;

    private boolean mIsFirstShowMap = true;
    private Location mCurrentLocation;
    private CopyOnWriteArrayList<ActiveArtistMapBean> mCurrentActiveArtists;
    private CopyOnWriteArrayList<ActiveArtistMapBean> mActiveArtists;

    @Override
    public void setResponse(String response, String requestId) {

    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {

    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

    public void checkAvailablePlayServices(FragmentActivity activity) {
        int errorCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
        if (errorCode != ConnectionResult.SUCCESS) {
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(errorCode,
                    activity,
                    REQUEST_ERROR,
                    new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {

                        }
                    });
        }
    }

    public void startGoogleClientConnect() {
        getViewState().onGoogleClientConnect();
    }

    public void googleClientConnect(FragmentActivity activity, PermissionsUtils permissionsUtils) {
        if (permissionsUtils.checkPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION})) {
            getViewState().onRequestLocationUpdates();
        } else {
            permissionsUtils.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION}, GOOGLE_CLIENT_CONNECT_PERMISSION);
        }
    }

    public void updateMyPosition(Context context, GoogleMap googleMap) {
        LatLng myPosition = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(myPosition, DEFAULT_ZOOM);
        MarkerOptions myMarker = new MarkerOptions();
        myMarker.position(myPosition);
        googleMap.addMarker(myMarker);
        googleMap.animateCamera(update);

    }

    public boolean isFirstShowMap() {
        return mIsFirstShowMap;
    }

    public Location getCurrentLocation() {
        return mCurrentLocation;
    }

    public void setFirstShowMap(boolean firstShowMap) {
        mIsFirstShowMap = firstShowMap;
    }

    public void setCurrentLocation(Location currentLocation) {
        mCurrentLocation = currentLocation;
    }

    public void changeMyPositionOnMap() {
        getViewState().onChangeMyPositionOnMap();
    }

    public void checkResultFromReceiver(String jsonRequestResult) {
        if (BaseResponseParser.checkSuccessResponse(jsonRequestResult)) {
            CopyOnWriteArrayList<ActiveArtistMapBean> activeArtistMapBeans = ActiveUserMapBeanParser.parseActiveUsersMapBean(jsonRequestResult);
            if (activeArtistMapBeans != null && activeArtistMapBeans.size() > 0) {
                if (mCurrentActiveArtists == null) {
                    mCurrentActiveArtists = activeArtistMapBeans;
                    downloadAvatarsAndSendToMap(activeArtistMapBeans);
                } else {
                    //ToDo ПРОВЕРИТЬ! Возможно придется синхронизировать
                    mActiveArtists = activeArtistMapBeans;
                    new FindCurrentElementAsyncTask().execute(activeArtistMapBeans, mCurrentActiveArtists);
                }
            }
        }
    }

    public void addActiveBean(ActiveArtistMapBean bean) {
        if (mCurrentActiveArtists != null && !mCurrentActiveArtists.contains(bean)) {
            mCurrentActiveArtists.add(bean);
        }
    }

    public void showArtistProfileById(FragmentManager supportFragmentManager, long tag, UserBean bean) {
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container,
                GuestArtistProfileFragment.newInstance(tag, bean), GUEST_PROFILE).addToBackStack(null).commit();
    }

    public void destroyActiveList() {
        mCurrentActiveArtists = null;
    }


//ToDo оптимизировать все циклы в AsyncTasks

    /**
     * Ищем артистов, которые уже есть списке, но поменяли свои координаты. Обновляем в списке текущих и обновляем на карте
     */
    private class FindCurrentElementAsyncTask extends AsyncTask<CopyOnWriteArrayList<ActiveArtistMapBean>, Void, CopyOnWriteArrayList<ActiveArtistMapBean>> {

        @Override
        protected CopyOnWriteArrayList<ActiveArtistMapBean> doInBackground(CopyOnWriteArrayList<ActiveArtistMapBean>[] lists) {
            CopyOnWriteArrayList<ActiveArtistMapBean> activeList = lists[0];
            CopyOnWriteArrayList<ActiveArtistMapBean> currentList = lists[1];
            CopyOnWriteArrayList<ActiveArtistMapBean> newList = new CopyOnWriteArrayList<>();

            for (int i = 0; i < activeList.size(); i++) {
                for (int j = 0; j < currentList.size(); j++) {
                    if (activeList.get(i).getArtistId() == currentList.get(j).getArtistId()
                            && (activeList.get(i).getLatitude() != currentList.get(j).getLatitude()
                            || activeList.get(i).getLongitude() != currentList.get(j).getLongitude())) {
                        newList.add(currentList.get(j));
                    }
                }
            }
            updateCoordinatesCurrentList(newList);
            return newList;
        }

        @Override
        protected void onPostExecute(CopyOnWriteArrayList<ActiveArtistMapBean> activeArtistMapBeans) {
            getViewState().onUpdateMarkersPosition(activeArtistMapBeans);
            new FindNewArtistAsyncTask().execute(mActiveArtists, mCurrentActiveArtists);
        }
    }

    /**
     * Поиск новых артистов, которых еще нет в списке активных. Качаем аватары, закидываем в текущие и отмечаем на карте
     */
    private class FindNewArtistAsyncTask extends AsyncTask<CopyOnWriteArrayList<ActiveArtistMapBean>, Void, CopyOnWriteArrayList<ActiveArtistMapBean>> {

        @Override
        protected CopyOnWriteArrayList<ActiveArtistMapBean> doInBackground(CopyOnWriteArrayList<ActiveArtistMapBean>[] lists) {
            CopyOnWriteArrayList<ActiveArtistMapBean> activeList = lists[0];
            CopyOnWriteArrayList<ActiveArtistMapBean> currentList = lists[1];
            CopyOnWriteArrayList<ActiveArtistMapBean> newList = new CopyOnWriteArrayList<>();


            for (int i = 0; i < activeList.size(); i++) {
                boolean isItemExist = false;
                for (int j = 0; j < currentList.size(); j++) {
                    if (activeList.get(i).getArtistId() == currentList.get(j).getArtistId()) {
                        isItemExist = true;
                        break;
                    }
                }

                if (!isItemExist) {
                    newList.add(activeList.get(i));
                }
            }
            return newList;
        }

        @Override
        protected void onPostExecute(CopyOnWriteArrayList<ActiveArtistMapBean> artistBeans) {
            downloadAvatarsAndSendToMap(artistBeans);
            new FindNotActiveArtists().execute(mActiveArtists, mCurrentActiveArtists);
        }
    }

    /**
     * Ищем в списке текущих уже неактивных артистов. Удаляем из текущих, удаляем с карты
     */
    private class FindNotActiveArtists extends AsyncTask<CopyOnWriteArrayList<ActiveArtistMapBean>, Void, CopyOnWriteArrayList<ActiveArtistMapBean>> {

        @Override
        protected CopyOnWriteArrayList<ActiveArtistMapBean> doInBackground(CopyOnWriteArrayList<ActiveArtistMapBean>[] lists) {
            CopyOnWriteArrayList<ActiveArtistMapBean> activeList = lists[0];
            CopyOnWriteArrayList<ActiveArtistMapBean> currentList = lists[1];
            CopyOnWriteArrayList<ActiveArtistMapBean> notActiveList = new CopyOnWriteArrayList<>();


            for (int i = 0; i < currentList.size(); i++) {
                boolean isActive = false;
                for (int j = 0; j < activeList.size(); j++) {
                    if (currentList.get(i).getArtistId() == activeList.get(j).getArtistId()) {
                        isActive = true;
                        break;
                    }
                }

                if (!isActive) {
                    notActiveList.add(currentList.get(i));
                }

            }
            deleteFromCurrentArtists(notActiveList);
            return notActiveList;
        }

        @Override
        protected void onPostExecute(CopyOnWriteArrayList<ActiveArtistMapBean> notActiveArtistMapBeans) {
            getViewState().onDeleteMarkersPosition(notActiveArtistMapBeans);
        }
    }

    private void deleteFromCurrentArtists(List<ActiveArtistMapBean> notActiveList) {
        mCurrentActiveArtists.removeAll(notActiveList);
    }

    private void downloadAvatarsAndSendToMap(List<ActiveArtistMapBean> activeArtistMapBeans) {
        for (int i = 0; i < activeArtistMapBeans.size(); i++) {
            getViewState().onDownloadIcons(activeArtistMapBeans.get(i));
        }
    }

    private void updateCoordinatesCurrentList(List<ActiveArtistMapBean> activeArtistMapBeans) {
        for (int i = 0; i < mCurrentActiveArtists.size(); i++) {
            for (int j = 0; j < activeArtistMapBeans.size(); j++) {
                if (mCurrentActiveArtists.get(i).getArtistId() == activeArtistMapBeans.get(j).getArtistId()) {
                    ActiveArtistMapBean bean = mCurrentActiveArtists.get(i);
                    bean = activeArtistMapBeans.get(j);
                }
            }
        }
    }
}
