package ru.music_boom_app.views.registration.step1;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import net.rimoto.intlphoneinput.IntlPhoneInput;

import javax.inject.Inject;

import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.PermissionsUtils;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;
import ru.music_boom_app.views.base.BaseApplicationFragment;

/**
 * @author Markin Andrey on 09.01.2018.
 */
public class RegistrationFragmentStep_1 extends BaseApplicationFragment implements RegistrationFragmentView_1 {

    private static final String FAILED_SMS_VERIFICATION_DIALOG = "failed_sms_verification";
    private static final String ERROR_OPERATION = "error_operation";

    public static RegistrationFragmentStep_1 newInstance() {
        Bundle args = new Bundle();
        RegistrationFragmentStep_1 fragment = new RegistrationFragmentStep_1();
        fragment.setArguments(args);
        return fragment;
    }

    public static RegistrationFragmentStep_1 newInstance(String email,
                                                         String name,
                                                         String surname,
                                                         String birthday,
                                                         String country,
                                                         String city,
                                                         String provider,
                                                         String registeredId) {
        Bundle args = new Bundle();
        args.putString(UserArtistBean.NAME_EXTRA, name);
        args.putString(UserArtistBean.SURNAME_EXTRA, surname);
        args.putString(UserArtistBean.BIRTHDAY_EXTRA, birthday);
        args.putString(UserArtistBean.COUNTRY_EXTRA, country);
        args.putString(UserArtistBean.CITY_EXTRA, city);
        args.putString(UserArtistBean.EMAIL_EXTRA, email);
        args.putString(UserBean.PROVIDER, provider);
        args.putString(UserBean.REGISTERED_ID, registeredId);
        RegistrationFragmentStep_1 fragment = new RegistrationFragmentStep_1();
        fragment.setArguments(args);
        return fragment;
    }

    private TextInputLayout mPasswordLayout;
    private EditText mPasswordEditText;
    private TextInputLayout mConfirmPasswordLayout;
    private EditText mConfirmPasswordEditText;
    private Button mConfirmButton;
    private IntlPhoneInput mPhoneNumberText;
    private TextInputLayout mPhoneNumberLayout;
    private EditText mSmsCodeText;
    private ImageView mSuccessTick;
    private Button mSendSmsCode;
    private Button mCheckSmsCode;
    private TextView mChronometer;
    private TextView mResendSMSText;
    private TextView mResendSMSButton;
    private PhoneAuthProvider.ForceResendingToken mForceResendingToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private FirebaseAuth mAuth;

    @Inject
    PermissionsUtils mPermissionsUtils;
    @Inject
    NetworkUtils mNetworkUtils;
    @InjectPresenter
    RegistrationFragmentPresenter_1 mPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(getContext()).inject(this);
        mAuth = FirebaseAuth.getInstance();
        initSmsAuthCallback();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setTitle(R.string.registration_title);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.registration_step_1_fragment, container, false);
        mPasswordLayout = (TextInputLayout) view.findViewById(R.id.password_layout);
        mPasswordEditText = (EditText) view.findViewById(R.id.password);
        mConfirmPasswordLayout = (TextInputLayout) view.findViewById(R.id.confirm_password_layout);
        mConfirmPasswordEditText = (EditText) view.findViewById(R.id.confirm_password);
        mConfirmButton = (Button) view.findViewById(R.id.confirm_button);

        mPasswordEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkPasswordField(mPasswordEditText.getText().toString());
                } else {
                    mPasswordLayout.setErrorEnabled(false);
                }
            }
        });

        mConfirmPasswordEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkConfirmPasswordField(
                            mConfirmPasswordEditText.getText().toString(),
                            mPasswordEditText.getText().toString());
                } else {
                    mConfirmPasswordLayout.setErrorEnabled(false);
                }
            }
        });

        mPhoneNumberLayout = (TextInputLayout) view.findViewById(R.id.phoneNumberLayout);
        mPhoneNumberText = (IntlPhoneInput) view.findViewById(R.id.phoneNumber);
        mPhoneNumberText.setEmptyDefault("RU");

        mSmsCodeText = (EditText) view.findViewById(R.id.smsCode);
        mSuccessTick = (ImageView) view.findViewById(R.id.successTick);
        mSendSmsCode = (Button) view.findViewById(R.id.sendSMSCode);
        mSendSmsCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPresenter.checkValidNumber(mPhoneNumberText)) {
                    mPhoneNumberLayout.setErrorEnabled(false);
                    mPhoneNumberText.setEnabled(false);
                    mPresenter.sendSMSCode(
                            getActivity(),
                            mPhoneNumberText.getNumber(),
                            mCallbacks,
                            mNetworkUtils
                    );
                }
            }
        });

        mCheckSmsCode = (Button) view.findViewById(R.id.checkSMSCode);
        mCheckSmsCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(mSmsCodeText.getText().toString())) {
                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mPresenter.getVerificationId(), mSmsCodeText.getText().toString());
                    checkCredential(credential);
                }
            }
        });

        mChronometer = (TextView) view.findViewById(R.id.timer_chronometer);
        mResendSMSText = (TextView) view.findViewById(R.id.resend_sms_text);
        mResendSMSButton = (TextView) view.findViewById(R.id.resend_sms_button);
        mResendSMSButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mResendSMSButton.setVisibility(View.GONE);
                mPresenter.resendSMSCode(
                        getActivity(),
                        mPhoneNumberText.getText(),
                        mCallbacks,
                        mForceResendingToken,
                        mNetworkUtils
                );
            }
        });

        mConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.checkAllFieldsAndStartNewFragment(getActivity().getSupportFragmentManager(),
                        getArguments(),
                        mPresenter.getVerificationPhoneNumber(),
                        mPasswordEditText.getText().toString(),
                        mConfirmPasswordEditText.getText().toString()
                );
            }
        });
        return view;
    }

    @Override
    public void showPasswordLayoutException(int stringExceptionId) {
        mPasswordLayout.setErrorEnabled(true);
        mPasswordLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showConfirmPasswordLayoutException(int stringExceptionId) {
        mConfirmPasswordLayout.setErrorEnabled(true);
        mConfirmPasswordLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showPhoneNumberException(int stringExceptionId) {
        mPhoneNumberLayout.setErrorEnabled(true);
        mPhoneNumberLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showResendSMSTimer(String seconds) {
        mChronometer.setText(seconds);
    }

    @Override
    public void showResendSMSButton() {
        mResendSMSText.setVisibility(View.GONE);
        mChronometer.setVisibility(View.GONE);
        mResendSMSButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void showFailDialog(int errorTextId) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(errorTextId),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), ERROR_OPERATION);
    }

    @Override
    public void updateUIVerificationFailedFragment() {
        mSmsCodeText.setVisibility(View.GONE);
        mSendSmsCode.setVisibility(View.VISIBLE);
        mCheckSmsCode.setVisibility(View.GONE);
        showFailedVerificationAlertDialog();
    }

    @Override
    public void updateUICodeSentFragment() {
        mSmsCodeText.setVisibility(View.VISIBLE);
        mSendSmsCode.setVisibility(View.GONE);
        mCheckSmsCode.setVisibility(View.VISIBLE);
        mResendSMSText.setVisibility(View.VISIBLE);
        mChronometer.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateUIOnCompleteFragment() {
        mSuccessTick.setVisibility(View.VISIBLE);
        mSmsCodeText.setVisibility(View.GONE);
        mSendSmsCode.setVisibility(View.GONE);
        mCheckSmsCode.setVisibility(View.GONE);
        mResendSMSText.setVisibility(View.GONE);
        mResendSMSButton.setVisibility(View.GONE);
        mChronometer.setVisibility(View.GONE);
    }

    private void initSmsAuthCallback() {
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                checkCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                mPresenter.updateUIVerificationFailed();
            }

            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                mPresenter.setVerificationId(verificationId);
                mForceResendingToken = forceResendingToken;
                mPresenter.updateUICodeSent();
                mPresenter.setChronometerTimer();
            }
        };
    }

    private void showFailedSMSVerificationAlertDialog() {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.failed_sms_verification_dialog_title),
                getString(R.string.filed_sms_verification_dialog_text),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), FAILED_SMS_VERIFICATION_DIALOG);
    }

    private void showFailedVerificationAlertDialog() {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(R.string.failed_verification_dialog_text),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), FAILED_SMS_VERIFICATION_DIALOG);
    }

    private void checkCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            mPresenter.cancelChronometer();
                            mPresenter.updateUIOnComplete();
                            mPresenter.setVerificationPhoneNumber(mPhoneNumberText.getNumber());
                        } else {
                            showFailedSMSVerificationAlertDialog();
                        }
                    }
                });
    }


}
