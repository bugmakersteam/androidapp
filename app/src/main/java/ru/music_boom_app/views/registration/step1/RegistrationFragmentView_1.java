package ru.music_boom_app.views.registration.step1;

import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;

/**
 * @author Markin Andrey on 09.01.2018.
 */
public interface RegistrationFragmentView_1 extends MvpView {
    void showPasswordLayoutException(@StringRes int stringExceptionId);
    void showConfirmPasswordLayoutException(@StringRes int stringExceptionId);
    void showPhoneNumberException(@StringRes int stringExceptionId);
    void showResendSMSTimer(String seconds);
    void showResendSMSButton();
    void showFailDialog(@StringRes int errorText);
    void updateUIVerificationFailedFragment();
    void updateUICodeSentFragment();
    void updateUIOnCompleteFragment();
}
