package ru.music_boom_app.views.registration.step2;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Markin Andrey on 12.01.2018.
 */
@StateStrategyType(SkipStrategy.class)
public interface RegistrationFragmentView_2 extends MvpView {
}
