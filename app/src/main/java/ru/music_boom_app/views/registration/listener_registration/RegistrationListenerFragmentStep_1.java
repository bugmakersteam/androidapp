package ru.music_boom_app.views.registration.listener_registration;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ProgressBar;

import com.arellomobile.mvp.presenter.InjectPresenter;

import javax.inject.Inject;

import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.PermissionsUtils;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;
import ru.music_boom_app.views.base.BaseApplicationFragment;

/**
 * @author Markin Andrey on 12.01.2018.
 */
public class RegistrationListenerFragmentStep_1 extends BaseApplicationFragment implements RegistrationListenerFragmentViewStep_1  {

    private static final String CHECK_PERSONAL_DATA_DIALOG_TAG = "personal_data_dialog_tag";
    private static final String ERROR_OPERATION = "error_operation";

    private TextInputLayout mCountryLayout;
    private AutoCompleteTextView mCountryText;
    private TextInputLayout mCityLayout;
    private AutoCompleteTextView mCityText;
    private ProgressBar mProgressBar;
    private CheckedTextView mCheckedPersonalData;
    private Button mEndRegistration;

    @InjectPresenter
    RegistrationListenerFragmentPresenterStep_1 mPresenter;
    @Inject
    PermissionsUtils mPermissionsUtils;
    @Inject
    NetworkUtils mNetworkUtils;

    public static RegistrationListenerFragmentStep_1 newInstance(Bundle args) {
        RegistrationListenerFragmentStep_1 fragment = new RegistrationListenerFragmentStep_1();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(getContext()).inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.registration_title);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.registration_step_3_listener_fragment, container, false);
        mCountryLayout = (TextInputLayout) view.findViewById(R.id.country_layout);
        mCountryText = (AutoCompleteTextView) view.findViewById(R.id.country);
        String [] countries = getResources().getStringArray(R.array.countriesArray);
        mCountryText.setAdapter(
                new ArrayAdapter<String>(getContext(), R.layout.auto_complete_list_item, countries)
        );
        mCountryText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkCountryText(mCountryText.getText().toString());
                } else {
                    mCountryLayout.setErrorEnabled(false);
                }
            }
        });
        mCityLayout = (TextInputLayout) view.findViewById(R.id.city_layout);
        mCityText = (AutoCompleteTextView) view.findViewById(R.id.city);
        String [] cities = getResources().getStringArray(R.array.citiesArray);
        mCityText.setAdapter(
                new ArrayAdapter<String>(getContext(), R.layout.auto_complete_list_item, cities)
        );
        mCityText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkCityText(mCityText.getText().toString());
                } else {
                    mCityLayout.setErrorEnabled(false);
                }
            }
        });
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mCheckedPersonalData = (CheckedTextView) view.findViewById(R.id.personalDataAgreement);
        mCheckedPersonalData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!mCheckedPersonalData.isChecked()){
                    mCheckedPersonalData.setChecked(true);
                } else {
                    mCheckedPersonalData.setChecked(false);
                }
            }
        });
        mEndRegistration = (Button) view.findViewById(R.id.end_registration_button);
        mEndRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.checkAllFieldsAndStartRequestRegistration(
                        getActivity(),
                        mNetworkUtils,
                        getArguments(),
                        mCheckedPersonalData.isChecked(),
                        mCountryText.getText().toString(),
                        mCityText.getText().toString()
                );
            }
        });
        return view;
    }

    @Override
    public void showCountryLayoutException(int stringExceptionId) {
        mCountryLayout.setErrorEnabled(true);
        mCountryLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showCityLayoutException(int stringExceptionId) {
        mCityLayout.setErrorEnabled(true);
        mCityLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showCheckedPersonalDataContractLayoutException() {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.check_personal_data_dialog_title),
                getString(R.string.check_personal_data_dialog_message),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), CHECK_PERSONAL_DATA_DIALOG_TAG);
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showFailDialog(int errorTextId) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(errorTextId),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), ERROR_OPERATION);
    }

    @Override
    public void openMainPage(UserBean bean) {
        mPresenter.startOpenMainPage(getContext(), bean);
    }

}
