package ru.music_boom_app.views.registration.artist_registration.step2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.arellomobile.mvp.InjectViewState;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.models.parsers.AuthorizationResponseUserArtistBeanParser;
import ru.music_boom_app.models.parsers.BaseResponseParser;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.base_requests.BasePostRequest;
import ru.music_boom_app.utils.request_observer.states.StateRequest;
import ru.music_boom_app.views.main_page.MainPageActivity;

/**
 * @author Markin Andrey on 31.01.2018.
 */
@InjectViewState
public class RegistrationArtistFragmentPresenterStep_2 extends BasePresenter<RegistrationArtistFragmentViewStep_2> {
    private static final String ARTIST_REGISTRATION = "artist_registration";
    private static final String USER_BEAN = "userBean";

    private String mCurrentSocialNetwork;

    @Override
    public void setResponse(String response, String requestId) {
        if (ARTIST_REGISTRATION.equals(requestId)) {
            if (BaseResponseParser.checkSuccessResponse(response)) {
                switch (BaseResponseParser.checkUserTypeResponse(response)) {
                    case BaseResponseParser.ARTIST_TYPE:
                        UserArtistBean userArtistBean = AuthorizationResponseUserArtistBeanParser.parseUserArtist(response);
                        getViewState().openMainPage(userArtistBean);
                        break;
                }
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }
    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        getViewState().showFailDialog(dialogErrorMessageId);
    }

    @Override
    public void showProgressBar() {
        getViewState().showProgress();
    }

    @Override
    public void hideProgressBar() {
        getViewState().hideProgress();
    }

    public String getCurrentSocialNetwork() {
        return mCurrentSocialNetwork;
    }

    public void checkEmailField(String emailText) {
        if (!checkEmailText(emailText)) {
            getViewState().showEmailLayoutException(R.string.registration_invalid_email);
        }
    }

    private boolean checkEmailText(String emailText) {
        String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(emailText);
        return matcher.matches();
    }

    public void showSocialNetworkValueFields(String currentSocialNetwork) {
        mCurrentSocialNetwork = currentSocialNetwork;
        getViewState().showSocialNetworkValueFields();
    }

    public void hideSocialNetworkValueFields() {
        getViewState().hideSocialNetworkValueFields(mCurrentSocialNetwork);
    }

    public void checkAllFieldsAndStartRequestRegistration(Activity activity,
                                                          NetworkUtils networkUtils,
                                                          Bundle bundle,
                                                          boolean checkedAgreementOfPersonalData,
                                                          boolean checkedArtistContract,
                                                          String email,
                                                          String vk,
                                                          String tlg,
                                                          String wApp) {
        if (!checkedAgreementOfPersonalData) {
            getViewState().showCheckedAgreementPersonalData();
            return;
        }
        if (!checkedArtistContract) {
            getViewState().showCheckedArtistContract();
            return;
        }
        if(!checkEmailText(email)){
            getViewState().showEmailLayoutException(R.string.registration_invalid_email);
        }

        if (networkUtils.checkConnection(activity)){
            sendArtistRegistrationRequest(bundle, email, vk, tlg, wApp);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }

    }

    public void startOpenMainPage(Context context, UserBean bean) {
        Intent intent = new Intent(context, MainPageActivity.class);
        intent.putExtra(USER_BEAN, bean);
        context.startActivity(intent);
    }

    private void sendArtistRegistrationRequest(Bundle bundle, String email, String vk, String tlg, String wApp) {
        ArtistRegistrationRequest request = new ArtistRegistrationRequest(this, R.string.request_fail_message);
        request.addObserver((observable, o) -> {
            BasePostRequest basePostRequest = (BasePostRequest) observable;
            StateRequest stateRequest = basePostRequest.getStateRequest();
            stateRequest.shareData();
        });

        JSONObject userObj = new JSONObject();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", bundle.getString(UserArtistBean.NAME_EXTRA));
            jsonObject.put("surname", bundle.getString(UserArtistBean.SURNAME_EXTRA));
            jsonObject.put("patronymic", bundle.get(UserArtistBean.PATRONYMIC_EXTRA));

            String date = bundle.getString(UserArtistBean.BIRTHDAY_EXTRA);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            try {
                Date formatDate = dateFormat.parse(date);
                SimpleDateFormat androidFormat = new SimpleDateFormat("yyyy-MM-dd");
                date = androidFormat.format(formatDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            jsonObject.put("id", bundle.getString(UserBean.REGISTERED_ID));
            jsonObject.put("birthday", date);
            jsonObject.put("nickname", bundle.getString(UserArtistBean.NICKNAME_EXTRA));
            jsonObject.put("country", bundle.getString(UserBean.COUNTRY_EXTRA));
            jsonObject.put("city", bundle.getString(UserBean.CITY_EXTRA));
            jsonObject.put("phoneNumber", bundle.getString(UserBean.PHONE_NUMBER_EXTRA));
            jsonObject.put("password", bundle.getString(UserBean.CONFIRM_PASSWORD_EXTRA));
            jsonObject.put("email", email);
            jsonObject.put("creativity", bundle.getString(UserArtistBean.CREATIVITY_EXTRA));
            jsonObject.put("instrument", bundle.getString(UserArtistBean.MUSICAL_INSTRUMENT_EXTRA));
            jsonObject.put("genre", bundle.getString(UserArtistBean.GENRE_EXTRA));
            jsonObject.put("vk", vk);
            jsonObject.put("tlg", tlg);
            jsonObject.put("wapp", wApp);
            jsonObject.put("isAgreementOfPersonalData", "true");
            jsonObject.put("isArtistContract", "true");
            userObj.put("provider", bundle.getString(UserBean.PROVIDER));
            userObj.put("user", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = createRequestBodyText("registrationJson", userObj.toString());
        request.createAndSendRequest(BuildConfig.URL, requestBody, ARTIST_REGISTRATION);
    }

    private RequestBody createRequestBodyText(String key, String value) {
        return RequestBody.create(MediaType.parse("application/json"), value);
    }
}
