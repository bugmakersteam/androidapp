package ru.music_boom_app.views.registration.step2;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.arellomobile.mvp.InjectViewState;

import java.io.File;

import ru.music_boom_app.R;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.views.registration.artist_registration.step1.RegistrationArtistFragmentStep_1;
import ru.music_boom_app.views.registration.listener_registration.RegistrationListenerFragmentStep_1;

/**
 * @author Markin Andrey on 12.01.2018.
 */
@InjectViewState
public class RegistrationFragmentPresenter_2 extends BasePresenter<RegistrationFragmentView_2> {
    private RegistrationListenerFragmentStep_1 mRegistrationListenerFragmentStep_1;
    private RegistrationArtistFragmentStep_1 mRegistrationArtistFragmentStep_1;

    @Override
    public void setResponse(String response, String requestId) {

    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {

    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

    public void showListenerRegistrationFragment(FragmentManager fragmentManager, Bundle bundle) {
        if (mRegistrationListenerFragmentStep_1 == null) {
            mRegistrationListenerFragmentStep_1 = RegistrationListenerFragmentStep_1.newInstance(bundle);
        } else {
            mRegistrationListenerFragmentStep_1.setArguments(bundle);
        }
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, mRegistrationListenerFragmentStep_1)
                .addToBackStack(null)
                .commit();
    }

    public void showArtistRegistrationFragment(FragmentManager fragmentManager, Bundle bundle) {
        if (mRegistrationArtistFragmentStep_1 == null) {
            mRegistrationArtistFragmentStep_1 = RegistrationArtistFragmentStep_1.newInstance(bundle);
        } else {
            mRegistrationArtistFragmentStep_1.setArguments(bundle);
        }
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, mRegistrationArtistFragmentStep_1)
                .addToBackStack(null)
                .commit();
    }
}
