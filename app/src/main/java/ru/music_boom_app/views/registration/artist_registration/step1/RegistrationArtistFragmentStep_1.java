package ru.music_boom_app.views.registration.artist_registration.step1;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;

import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.views.base.BaseApplicationFragment;

/**
 * @author Markin Andrey on 25.01.2018.
 */
public class RegistrationArtistFragmentStep_1 extends BaseApplicationFragment
        implements RegistrationArtistFragmentViewStep_1 {

    private TextInputLayout mNameLayout;
    private EditText mNameText;
    private TextInputLayout mSurnameNameLayout;
    private EditText mSurnameNameText;
    private TextInputLayout mNicknameLayout;
    private EditText mNicknameText;
    private TextInputLayout mCountryLayout;
    private AutoCompleteTextView mCountryText;
    private TextInputLayout mCityLayout;
    private AutoCompleteTextView mCityText;
    private TextInputLayout mBirthdayLayout;
    private EditText mBirthdayText;
    private Button mBirthdayButton;
    private RadioGroup mUserCreativityTypeRadioGroup;
    private TextInputLayout mMusicalInstrumentLayout;
    private AutoCompleteTextView mMusicalInstrumentText;
    private TextInputLayout mMusicalGenreLayout;
    private AutoCompleteTextView mMusicalGenreText;
    private Button mConfirmButton;
    private String mCreativity;

    //Флаг означающий первичную загрузку экрана
    private boolean mIsFirstScreen = true;

    @InjectPresenter
    RegistrationArtistFragmentPresenterStep_1 mPresenter;

    public static RegistrationArtistFragmentStep_1 newInstance(Bundle args) {
        RegistrationArtistFragmentStep_1 fragment = new RegistrationArtistFragmentStep_1();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.general_information_title);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.registration_step_3_artist_fragment, container, false);
        mNameLayout = (TextInputLayout) view.findViewById(R.id.name_layout);
        mNameText = (EditText) view.findViewById(R.id.name);
        mNameText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!view.hasFocus()) {
                    mPresenter.checkNameText(mNameText.getText().toString());
                } else {
                    mNameLayout.setErrorEnabled(false);
                }
            }
        });

        mSurnameNameLayout = (TextInputLayout) view.findViewById(R.id.surname_layout);
        mSurnameNameText = (EditText) view.findViewById(R.id.surname);
        mSurnameNameText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!view.hasFocus()) {
                    mPresenter.checkSurnameText(mSurnameNameText.getText().toString());
                } else {
                    mSurnameNameLayout.setErrorEnabled(false);
                }
            }
        });

        mNicknameLayout = (TextInputLayout) view.findViewById(R.id.nickname_layout);
        mNicknameText = (EditText) view.findViewById(R.id.nickname);
        mNicknameText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!view.hasFocus()) {
                    mPresenter.checkNicknameText(mNicknameText.getText().toString());
                } else {
                    mNicknameLayout.setErrorEnabled(false);
                }
            }
        });


        mCountryLayout = (TextInputLayout) view.findViewById(R.id.country_layout);
        mCountryText = (AutoCompleteTextView) view.findViewById(R.id.country);
        mCountryText.setAdapter(new ArrayAdapter<String>(getContext(),
                R.layout.auto_complete_list_item,
                getResources().getStringArray(R.array.countriesArray)));
        mCountryText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!view.hasFocus()) {
                    mPresenter.checkCountryText(mCountryText.getText().toString());
                } else {
                    mCountryLayout.setErrorEnabled(false);
                }
            }
        });

        mCityLayout = (TextInputLayout) view.findViewById(R.id.city_layout);
        mCityText = (AutoCompleteTextView) view.findViewById(R.id.city);
        mCityText.setAdapter(new ArrayAdapter<String>(getContext(),
                R.layout.auto_complete_list_item,
                getResources().getStringArray(R.array.citiesArray)));
        mCityText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!view.hasFocus()) {
                    mPresenter.checkCityText(mCityText.getText().toString());
                } else {
                    mCityLayout.setErrorEnabled(false);
                }
            }
        });

        mBirthdayLayout = (TextInputLayout) view.findViewById(R.id.birthday_layout);
        mBirthdayText = (EditText) view.findViewById(R.id.birthday);
        mBirthdayText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!view.hasFocus()) {
                    mPresenter.checkBirthdayText(mBirthdayText.getText().toString());
                } else {
                    mBirthdayLayout.setErrorEnabled(false);
                    mPresenter.showDataPickerDialog(getContext());
                }
            }
        });

//        mBirthdayButton = (Button) view.findViewById(R.id.choiceBirthdayDate);
//        mBirthdayButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mPresenter.showDataPickerDialog(getContext());
//            }
//        });

        mUserCreativityTypeRadioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
        mUserCreativityTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId) {
                    case R.id.musician:
                        mCreativity = RegistrationArtistFragmentPresenterStep_1.MUSICIAN_CREATIVITY;
                        mMusicalInstrumentLayout.setVisibility(View.VISIBLE);
                        mMusicalInstrumentText.setVisibility(View.VISIBLE);
                        break;
                    case R.id.artist:
                        mCreativity = RegistrationArtistFragmentPresenterStep_1.ARTIST_CREATIVITY;
                        mMusicalInstrumentLayout.setVisibility(View.GONE);
                        mMusicalInstrumentText.setVisibility(View.GONE);
                        break;
                }
            }
        });
        mMusicalInstrumentLayout = (TextInputLayout) view.findViewById(R.id.chooseMusicianInstrument_layout);
        mMusicalInstrumentText = (AutoCompleteTextView) view.findViewById(R.id.chooseMusicianInstrument);
        mMusicalInstrumentText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!view.hasFocus()) {
                    mPresenter.checkMusicalInstrumentText(mMusicalInstrumentText.getText().toString());
                } else {
                    mMusicalInstrumentLayout.setErrorEnabled(false);
                }
            }
        });

        int radioButtonId = mUserCreativityTypeRadioGroup.getCheckedRadioButtonId();
        if (R.id.musician == radioButtonId) {
            mCreativity = RegistrationArtistFragmentPresenterStep_1.MUSICIAN_CREATIVITY;
            mMusicalInstrumentLayout.setVisibility(View.VISIBLE);
            mMusicalInstrumentText.setVisibility(View.VISIBLE);
        }

        mMusicalGenreLayout = (TextInputLayout) view.findViewById(R.id.chooseGenre_layout);
        mMusicalGenreText = (AutoCompleteTextView) view.findViewById(R.id.chooseGenre);
        mMusicalGenreText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!view.hasFocus()) {
                    mPresenter.checkGenreText(mMusicalGenreText.getText().toString());
                } else {
                    mMusicalGenreLayout.setErrorEnabled(false);
                }
            }
        });

        mConfirmButton = (Button) view.findViewById(R.id.confirm_button);
        mConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.checkAllParametersAndShowNextFragment(
                        getFragmentManager(),
                        getArguments(),
                        mNameText.getText().toString(),
                        mSurnameNameText.getText().toString(),
                        "",
                        mNicknameText.getText().toString(),
                        mCountryText.getText().toString(),
                        mCityText.getText().toString(),
                        mBirthdayText.getText().toString(),
                        mCreativity,
                        mMusicalInstrumentText.getText().toString(),
                        mMusicalGenreText.getText().toString()
                );
            }
        });

        if (mIsFirstScreen){
            Bundle bundle = getArguments();
            if (!TextUtils.isEmpty(bundle.getString(UserArtistBean.NAME_EXTRA))){
                mNameText.setText(bundle.getString(UserArtistBean.NAME_EXTRA));
            }
            if (!TextUtils.isEmpty(bundle.getString(UserArtistBean.SURNAME_EXTRA))){
                mSurnameNameText.setText(bundle.getString(UserArtistBean.SURNAME_EXTRA));
            }
            if (!TextUtils.isEmpty(bundle.getString(UserArtistBean.COUNTRY_EXTRA))){
                mCountryText.setText(bundle.getString(UserArtistBean.COUNTRY_EXTRA));
            }
            if (!TextUtils.isEmpty(bundle.getString(UserArtistBean.CITY_EXTRA))){
                mCityText.setText(bundle.getString(UserArtistBean.CITY_EXTRA));
            }
            if (!TextUtils.isEmpty(bundle.getString(UserArtistBean.BIRTHDAY_EXTRA))){
                mBirthdayText.setText(bundle.getString(UserArtistBean.BIRTHDAY_EXTRA));
            }
            mIsFirstScreen = false;
        }

        return view;
    }

    @Override
    public void showCheckNameLayoutException(int stringExceptionId) {
        mNameLayout.setErrorEnabled(true);
        mNameLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showCheckSurnameLayoutException(int stringExceptionId) {
        mSurnameNameLayout.setErrorEnabled(true);
        mSurnameNameLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showCheckNicknameLayoutException(int stringExceptionId) {
        mNicknameLayout.setErrorEnabled(true);
        mNicknameLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showCheckCountryLayoutException(int stringExceptionId) {
        mCountryLayout.setErrorEnabled(true);
        mCountryLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showCheckCityLayoutException(int stringExceptionId) {
        mCityLayout.setErrorEnabled(true);
        mCityLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showCheckBirthdayLayoutException(int stringExceptionId) {
        mBirthdayLayout.setErrorEnabled(true);
        mBirthdayLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showCheckMusicalInstrumentLayoutException(int stringExceptionId) {
        if (RegistrationArtistFragmentPresenterStep_1.MUSICIAN_CREATIVITY.equals(mCreativity)) {
            mMusicalInstrumentLayout.setErrorEnabled(true);
            mMusicalInstrumentLayout.setError(getString(stringExceptionId));
        }
    }

    @Override
    public void showCheckGenreLayoutException(int stringExceptionId) {
        mMusicalGenreLayout.setErrorEnabled(true);
        mMusicalGenreLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showBirthdayDate(String date) {
        mBirthdayText.setText(date);
    }
}
