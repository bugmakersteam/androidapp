package ru.music_boom_app.views.registration.listener_registration;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.music_boom_app.BuildConfig;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.models.beans.UserListenerBean;
import ru.music_boom_app.models.parsers.AuthorizationResponseUserListenerBeanParser;
import ru.music_boom_app.models.parsers.BaseResponseParser;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.base_requests.BasePostRequest;
import ru.music_boom_app.utils.request_observer.states.StateRequest;
import ru.music_boom_app.views.main_page.MainPageActivity;

/**
 * @author Markin Andrey on 12.01.2018.
 */
@InjectViewState
public class RegistrationListenerFragmentPresenterStep_1 extends BasePresenter<RegistrationListenerFragmentViewStep_1> {
    private static final String URL_REQUEST = "/registration?user_type=listener";
    private static final String LISTENER_REGISTRATION = "listener_registration";
    private static final String USER_BEAN = "userBean";

    @Override
    public void setResponse(String response, String requestId) {
        if (LISTENER_REGISTRATION.equals(requestId)) {
            if (BaseResponseParser.checkSuccessResponse(response)) {
                switch (BaseResponseParser.checkUserTypeResponse(response)) {
                    case BaseResponseParser.LISTENER_TYPE:
                        UserListenerBean userListenerBean = AuthorizationResponseUserListenerBeanParser.paresUserListener(response);
                        getViewState().openMainPage(userListenerBean);
                        break;
                }
            } else {
                getViewState().showFailDialog(R.string.request_fail_message);
            }
        }
    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        getViewState().showFailDialog(dialogErrorMessageId);
    }

    @Override
    public void showProgressBar() {
        getViewState().showProgress();
    }

    @Override
    public void hideProgressBar() {
        getViewState().hideProgress();
    }

    public void checkCountryText(String s) {
        if (checkText(s)) {
            getViewState().showCountryLayoutException(R.string.registration_invalid_country);
        }
    }

    public void checkCityText(String s) {
        if (checkText(s)) {
            getViewState().showCityLayoutException(R.string.registration_invalid_city);
        }
    }

    public void checkAllFieldsAndStartRequestRegistration(Activity activity, NetworkUtils networkUtils, Bundle bundle, boolean checked, String countryText, String cityText) {
        if (!checked) {
            getViewState().showCheckedPersonalDataContractLayoutException();
            return;
        }
        if (checkText(countryText)) {
            getViewState().showCountryLayoutException(R.string.registration_invalid_country);
            return;
        }
        if (checkText(cityText)) {
            getViewState().showCityLayoutException(R.string.registration_invalid_city);
            return;
        }

        if (networkUtils.checkConnection(activity)){
            sendRegistrationUserListenerRequest(bundle, countryText, cityText);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }

    }

    public void startOpenMainPage(Context context, UserBean bean) {
        Intent intent = new Intent(context, MainPageActivity.class);
        intent.putExtra(USER_BEAN, bean);
        context.startActivity(intent);
    }

    private boolean checkText(String text) {
        return TextUtils.isEmpty(text);
    }

    private void sendRegistrationUserListenerRequest(Bundle bundle, String countryText, String cityText) {
        ListenerRegistrationRequest request = new ListenerRegistrationRequest(this,
                R.string.request_fail_message);
        request.addObserver((o, arg) -> {
            BasePostRequest basePostRequest = (BasePostRequest) o;
            StateRequest stateRequest = basePostRequest.getStateRequest();
            stateRequest.shareData();
        });
        JSONObject mainJsonObject = new JSONObject();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", bundle.getString(UserBean.REGISTERED_ID));
            jsonObject.put("phoneNumber", bundle.getString(UserBean.PHONE_NUMBER_EXTRA));
            jsonObject.put("password", bundle.getString(UserBean.CONFIRM_PASSWORD_EXTRA));
            jsonObject.put("country", countryText);
            jsonObject.put("city", cityText);
            jsonObject.put("isAgreementOfPersonalData", "true");
            mainJsonObject.put("provider", bundle.getString(UserBean.PROVIDER));
            mainJsonObject.put("user", jsonObject);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = createRequestBodyText("registrationJson", mainJsonObject.toString());
        request.createAndSendRequest(BuildConfig.URL, requestBody, LISTENER_REGISTRATION);
    }

    private RequestBody createRequestBodyText(String key, String value) {
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), value);
    }
}
