package ru.music_boom_app.views.registration.artist_registration.step1;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.views.registration.artist_registration.step2.RegistrationArtistFragmentStep_2;

/**
 * @author Markin Andrey on 25.01.2018.
 */
@InjectViewState
public class RegistrationArtistFragmentPresenterStep_1 extends BasePresenter<RegistrationArtistFragmentViewStep_1> {
    public static final String MUSICIAN_CREATIVITY = "musician";
    public static final String ARTIST_CREATIVITY = "artist";

    private RegistrationArtistFragmentStep_2 mRegistrationArtistFragmentStep_2;

    @Override
    public void setResponse(String response, String requestId) {

    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {

    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

    public void checkNameText(String nameText) {
        if (checkText(nameText)) {
            getViewState().showCheckNameLayoutException(R.string.registration_invalid_name);
        }
    }

    public void checkSurnameText(String surnameText) {
        if (checkText(surnameText)) {
            getViewState().showCheckSurnameLayoutException(R.string.registration_invalid_surname);
        }
    }

    public void checkNicknameText(String nicknameText) {
        if (checkText(nicknameText)) {
            getViewState().showCheckNicknameLayoutException(R.string.registration_invalid_nickname);
        }
    }

    public void checkCountryText(String countryText) {
        if (checkText(countryText)) {
            getViewState().showCheckCountryLayoutException(R.string.registration_invalid_country);
        }
    }

    public void checkCityText(String cityText) {
        if (checkText(cityText)) {
            getViewState().showCheckCityLayoutException(R.string.registration_invalid_city);
        }
    }

    public void checkBirthdayText(String birthdayText) {
        if (checkText(birthdayText)) {
            getViewState().showCheckBirthdayLayoutException(R.string.registration_invalid_birthday);
        }
    }

    public void checkMusicalInstrumentText(String musicalInstrumentText) {
        if (checkText(musicalInstrumentText)) {
            getViewState().showCheckMusicalInstrumentLayoutException(R.string.registration_invalid_musical_instrument);
        }
    }

    public void checkGenreText(String genreText) {
        if (checkText(genreText)) {
            getViewState().showCheckGenreLayoutException(R.string.registration_invalid_genre);
        }
    }

    public void checkAllParametersAndShowNextFragment(FragmentManager fragmentManager,
                                                      Bundle bundle,
                                                      String nameText,
                                                      String surnameText,
                                                      String patronymicText,
                                                      String nicknameText,
                                                      String countryText,
                                                      String cityText,
                                                      String birthdayText,
                                                      String creativityText,
                                                      String musicalInstrumentText,
                                                      String genreText
    ) {
        if (checkText(nameText)) {
            getViewState().showCheckNameLayoutException(R.string.registration_invalid_name);
            return;
        }
        if (checkText(surnameText)) {
            getViewState().showCheckSurnameLayoutException(R.string.registration_invalid_surname);
            return;
        }
        if (checkText(patronymicText)) {
            patronymicText = "";
        }
        if (checkText(nicknameText)) {
            getViewState().showCheckNicknameLayoutException(R.string.registration_invalid_nickname);
            return;
        }
        if (checkText(countryText)) {
            getViewState().showCheckCountryLayoutException(R.string.registration_invalid_country);
            return;
        }
        if (checkText(cityText)) {
            getViewState().showCheckCountryLayoutException(R.string.registration_invalid_city);
            return;
        }
        if (checkText(birthdayText)) {
            getViewState().showCheckCountryLayoutException(R.string.registration_invalid_birthday);
            return;
        }
        if (MUSICIAN_CREATIVITY.equals(creativityText) && checkText(musicalInstrumentText)) {
            getViewState().showCheckCountryLayoutException(R.string.registration_invalid_musical_instrument);
            return;
        }
        if (checkText(genreText)) {
            getViewState().showCheckCountryLayoutException(R.string.registration_invalid_genre);
            return;
        }

        bundle.putString(UserArtistBean.NAME_EXTRA, nameText);
        bundle.putString(UserArtistBean.SURNAME_EXTRA, surnameText);
        bundle.putString(UserArtistBean.PATRONYMIC_EXTRA, patronymicText);
        bundle.putString(UserArtistBean.NICKNAME_EXTRA, nicknameText);
        bundle.putString(UserArtistBean.COUNTRY_EXTRA, countryText);
        bundle.putString(UserArtistBean.CITY_EXTRA, cityText);
        bundle.putString(UserArtistBean.BIRTHDAY_EXTRA, birthdayText);
        bundle.putString(UserArtistBean.CREATIVITY_EXTRA, creativityText);
        bundle.putString(UserArtistBean.MUSICAL_INSTRUMENT_EXTRA, musicalInstrumentText);
        bundle.putString(UserArtistBean.GENRE_EXTRA, genreText);
        showArtistRegistrationStep_2(fragmentManager, bundle);
    }

    public void showArtistRegistrationStep_2(FragmentManager fragmentManager,Bundle bundle){
        if (mRegistrationArtistFragmentStep_2 == null) {
            mRegistrationArtistFragmentStep_2 = RegistrationArtistFragmentStep_2.newInstance(bundle);
        } else {
            mRegistrationArtistFragmentStep_2.setArguments(bundle);
        }
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, mRegistrationArtistFragmentStep_2)
                .addToBackStack(null)
                .commit();
    }

    public void showDataPickerDialog(Context context) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialogFragment = new DatePickerDialog(context, R.style.AppCompatAlertDialogStyle, (datePicker, year1, month1, day1) -> {
            Calendar calendar = Calendar.getInstance(Locale.GERMANY);
            calendar.set(Calendar.YEAR, year1);
            calendar.set(Calendar.MONTH, month1);
            calendar.set(Calendar.DATE, day1);
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            String date = dateFormat.format(calendar.getTime());
            getViewState().showBirthdayDate(date);
        }, year, month, day);
        dialogFragment.show();
    }

    private boolean checkText(String text) {
        return TextUtils.isEmpty(text);
    }
}
