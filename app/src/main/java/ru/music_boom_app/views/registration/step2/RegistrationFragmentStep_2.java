package ru.music_boom_app.views.registration.step2;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.arellomobile.mvp.presenter.InjectPresenter;

import ru.music_boom_app.R;
import ru.music_boom_app.views.base.BaseApplicationFragment;

/**
 * @author Markin Andrey on 12.01.2018.
 */
public class RegistrationFragmentStep_2 extends BaseApplicationFragment implements RegistrationFragmentView_2 {

    private Button mUserListenerButton;
    private Button mUserArtistButton;

    @InjectPresenter
    RegistrationFragmentPresenter_2 mPresenter;

    public static RegistrationFragmentStep_2 newInstance(Bundle args) {
        RegistrationFragmentStep_2 fragment = new RegistrationFragmentStep_2();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.registration_title);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.registration_step_2_fragment, container, false);

        mUserListenerButton = view.findViewById(R.id.listener_button);
        mUserListenerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.showListenerRegistrationFragment(
                        getActivity().getSupportFragmentManager(),
                        getArguments()
                );
            }
        });
        mUserArtistButton = view.findViewById(R.id.artist_button);
        mUserArtistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.showArtistRegistrationFragment(
                        getActivity().getSupportFragmentManager(),
                        getArguments()
                );
            }
        });

        return view;
    }
}
