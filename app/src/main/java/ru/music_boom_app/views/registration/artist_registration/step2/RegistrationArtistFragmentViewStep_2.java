package ru.music_boom_app.views.registration.artist_registration.step2;

import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import ru.music_boom_app.models.beans.UserBean;

/**
 * @author Markin Andrey on 31.01.2018.
 */
@StateStrategyType(AddToEndStrategy.class)
public interface RegistrationArtistFragmentViewStep_2 extends MvpView {
    void showFailDialog(@StringRes int errorText);
    void showEmailLayoutException(@StringRes int stringExceptionId);
    void showSocialNetworkValueFields();
    void hideSocialNetworkValueFields(String currentSocialNetwork);
    void showProgress();
    void hideProgress();
    void showCheckedAgreementPersonalData();
    void showCheckedArtistContract();
    void openMainPage(UserBean bean);
}
