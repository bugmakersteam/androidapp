package ru.music_boom_app.views.registration.artist_registration.step2;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.arellomobile.mvp.presenter.InjectPresenter;

import javax.inject.Inject;

import ru.music_boom_app.MusicBoomApplication;
import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserArtistBean;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.utils.PermissionsUtils;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;
import ru.music_boom_app.views.base.BaseApplicationFragment;

/**
 * @author Markin Andrey on 31.01.2018.
 */
public class RegistrationArtistFragmentStep_2 extends BaseApplicationFragment
        implements RegistrationArtistFragmentViewStep_2 {

    private static final String CHECK_PERSONAL_DATA_DIALOG_TAG = "check_personal_data";
    private static final String CHECK_ARTIST_CONTRACT_DIALOG_TAG = "check_artist_contract";
    private static final String ERROR_OPERATION = "error_operation";

    public static RegistrationArtistFragmentStep_2 newInstance(Bundle args) {
        RegistrationArtistFragmentStep_2 fragment = new RegistrationArtistFragmentStep_2();
        fragment.setArguments(args);
        return fragment;
    }

    private final static String VK = "vk";
    private final static String FB = "fb";
    private final static String W_APP = "w_app";
    private final static String TLG = "tlg";

    private TextInputLayout mEmailLayout;
    private EditText mEmailEditText;
    private AppCompatImageView mVK_ImageButton;
    private AppCompatImageView mFB_ImageButton;
    private AppCompatImageView mWapp_ImageButton;
    private AppCompatImageView mTlg_ImageButton;
    private EditText mSocialNetworkValue;
    private Button mSocialNetworkAddValueButton;
    private ProgressBar mProgressBar;
    private CheckedTextView mAgreementOfPersonalDataView;
    private CheckedTextView mArtistContractView;
    private Button mEndRegistrationButton;

    private String mVK_link;
    private String mFb_link;
    private String mW_AppNumber;
    private String mTlgAlias;

    //Флаг означающий первичную загрузку экрана
    private boolean mIsFirstScreen = true;

    @Inject
    PermissionsUtils mPermissionsUtils;
    @Inject
    NetworkUtils mNetworkUtils;
    @InjectPresenter
    RegistrationArtistFragmentPresenterStep_2 mPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MusicBoomApplication.getComponent(getContext()).inject(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.contacts_information_title);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.registration_step_4_artist_fragment, container, false);
        mEmailLayout = (TextInputLayout) view.findViewById(R.id.email_layout);
        mEmailEditText = (EditText) view.findViewById(R.id.email);
        mEmailEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    mPresenter.checkEmailField(mEmailEditText.getText().toString());
                } else {
                    mEmailLayout.setErrorEnabled(false);
                }
            }
        });

        mVK_ImageButton = (AppCompatImageView) view.findViewById(R.id.address_vk_button);
        mVK_ImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.showSocialNetworkValueFields(VK);
            }
        });
        mFB_ImageButton = (AppCompatImageView) view.findViewById(R.id.address_fb_button);
        mFB_ImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.showSocialNetworkValueFields(FB);
            }
        });
        mWapp_ImageButton = (AppCompatImageView) view.findViewById(R.id.address_whats_app_button);
        mWapp_ImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.showSocialNetworkValueFields(W_APP);
            }
        });
        mTlg_ImageButton = (AppCompatImageView) view.findViewById(R.id.address_tlg_button);
        mTlg_ImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.showSocialNetworkValueFields(TLG);
            }
        });
        mSocialNetworkValue = (EditText) view.findViewById(R.id.social_net_value);
        mSocialNetworkAddValueButton = (Button) view.findViewById(R.id.set_address_value_button);
        mSocialNetworkAddValueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = mSocialNetworkValue.getText().toString();
                switch (mPresenter.getCurrentSocialNetwork()) {
                    case VK:
                        mVK_link = value;
                        break;
                    case FB:
                        mFb_link = value;
                        break;
                    case W_APP:
                        mW_AppNumber = value;
                        break;
                    case TLG:
                        mTlgAlias = value;
                        break;
                }
                mPresenter.hideSocialNetworkValueFields();
            }
        });
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mAgreementOfPersonalDataView = (CheckedTextView) view.findViewById(R.id.personalDataAgreement);
        mAgreementOfPersonalDataView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mAgreementOfPersonalDataView.isChecked()) {
                    mAgreementOfPersonalDataView.setChecked(true);
                } else {
                    mAgreementOfPersonalDataView.setChecked(false);
                }
            }
        });
        mArtistContractView = (CheckedTextView) view.findViewById(R.id.musicianContract);
        mArtistContractView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mArtistContractView.isChecked()) {
                    mArtistContractView.setChecked(true);
                } else {
                    mArtistContractView.setChecked(false);
                }
            }
        });
        mEndRegistrationButton = (Button) view.findViewById(R.id.confirm_button);
        mEndRegistrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.checkAllFieldsAndStartRequestRegistration(
                        getActivity(),
                        mNetworkUtils,
                        getArguments(),
                        mAgreementOfPersonalDataView.isChecked(),
                        mArtistContractView.isChecked(),
                        mEmailEditText.getText().toString(),
                        mVK_link,
                        mTlgAlias,
                        mW_AppNumber
                );
            }
        });

        if (mIsFirstScreen){
            Bundle bundle = getArguments();
            if (!TextUtils.isEmpty(bundle.getString(UserArtistBean.EMAIL_EXTRA))){
                mEmailEditText.setText(bundle.getString(UserArtistBean.EMAIL_EXTRA));
            }
            mIsFirstScreen = false;
        }

        return view;
    }

    @Override
    public void showFailDialog(int errorTextId) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(errorTextId),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), ERROR_OPERATION);
    }

    @Override
    public void showEmailLayoutException(int stringExceptionId) {
        mEmailLayout.setErrorEnabled(true);
        mEmailLayout.setError(getString(stringExceptionId));
    }

    @Override
    public void showSocialNetworkValueFields() {
        String currentSocialNetwork = mPresenter.getCurrentSocialNetwork();
        switch (currentSocialNetwork) {
            case VK:
                setSocialNetworkValue(mVK_link);
                break;
            case FB:
                setSocialNetworkValue(mFb_link);
                break;
            case W_APP:
                setSocialNetworkValue(mW_AppNumber);
                break;
            case TLG:
                setSocialNetworkValue(mTlgAlias);
                break;
        }
        mSocialNetworkValue.setVisibility(View.VISIBLE);
        mSocialNetworkAddValueButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideSocialNetworkValueFields(String currentSocialNetwork) {
        mSocialNetworkValue.setVisibility(View.GONE);
        mSocialNetworkAddValueButton.setVisibility(View.GONE);
        changeSocialNetworkImageButton(currentSocialNetwork);
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showCheckedAgreementPersonalData() {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.check_personal_data_dialog_title),
                getString(R.string.check_personal_data_dialog_message),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), CHECK_PERSONAL_DATA_DIALOG_TAG);
    }

    @Override
    public void showCheckedArtistContract() {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.check_artist_contract_dialog_title),
                getString(R.string.check_artist_contract_dialog_message),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getActivity().getSupportFragmentManager(), CHECK_ARTIST_CONTRACT_DIALOG_TAG);
    }

    @Override
    public void openMainPage(UserBean bean) {
        mPresenter.startOpenMainPage(getContext(), bean);
    }


    private void changeSocialNetworkImageButton(String currentSocialNetwork) {
        switch (currentSocialNetwork) {
            case VK:
                if (!TextUtils.isEmpty(mVK_link)) {
                    mVK_ImageButton.setImageDrawable(getResources()
                            .getDrawable(R.drawable.ic_registration_added_vk, getActivity().getTheme()));
                } else {
                    mVK_ImageButton.setImageDrawable(getResources()
                            .getDrawable(R.drawable.ic_registration_vk, getActivity().getTheme()));
                }
                break;
            case FB:
                if (!TextUtils.isEmpty(mFb_link)) {
                    mFB_ImageButton.setImageDrawable(getResources()
                            .getDrawable(R.drawable.ic_registration_added_facebook, getActivity().getTheme()));
                } else {
                    mFB_ImageButton.setImageDrawable(getResources()
                            .getDrawable(R.drawable.ic_registration_facebook, getActivity().getTheme()));
                }
                break;
            case W_APP:
                if (!TextUtils.isEmpty(mW_AppNumber)) {
                    mWapp_ImageButton.setImageDrawable(getResources()
                            .getDrawable(R.drawable.ic_registration_added_whatsapp, getActivity().getTheme()));
                } else {
                    mWapp_ImageButton.setImageDrawable(getResources()
                            .getDrawable(R.drawable.ic_registration_whatsapp, getActivity().getTheme()));
                }
                break;
            case TLG:
                if (!TextUtils.isEmpty(mTlgAlias)) {
                    mTlg_ImageButton.setImageDrawable(getResources()
                            .getDrawable(R.drawable.ic_registration_added_telegram, getActivity().getTheme()));
                } else {
                    mTlg_ImageButton.setImageDrawable(getResources()
                            .getDrawable(R.drawable.ic_registration_telegram, getActivity().getTheme()));
                }
                break;
        }
    }

    private void setSocialNetworkValue(String value) {
        if (!TextUtils.isEmpty(value)) {
            mSocialNetworkValue.setText(value);
        } else {
            mSocialNetworkValue.setText("");
        }
    }
}
