package ru.music_boom_app.views.registration.listener_registration;

import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import ru.music_boom_app.models.beans.UserBean;

/**
 * @author Markin Andrey on 12.01.2018.
 */
@StateStrategyType(SingleStateStrategy.class)
public interface RegistrationListenerFragmentViewStep_1 extends MvpView {
    void showCountryLayoutException(@StringRes int stringExceptionId);

    void showCityLayoutException(@StringRes int stringExceptionId);

    void showCheckedPersonalDataContractLayoutException();

    void showProgress();

    void hideProgress();

    void showFailDialog(int errorTextId);

    void openMainPage(UserBean bean);
}
