package ru.music_boom_app.views.registration.step1;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.google.firebase.auth.PhoneAuthProvider;

import net.rimoto.intlphoneinput.IntlPhoneInput;

import java.io.File;
import java.util.concurrent.TimeUnit;

import ru.music_boom_app.R;
import ru.music_boom_app.models.beans.UserBean;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.NetworkUtils;
import ru.music_boom_app.views.registration.step2.RegistrationFragmentStep_2;

/**
 * @author Markin Andrey on 09.01.2018.
 */
@InjectViewState
public class RegistrationFragmentPresenter_1 extends BasePresenter<RegistrationFragmentView_1> {

    private static int MINIMAL_PASSWORD_SIZE = 6;

    private RegistrationFragmentStep_2 mStep_2;
    private ResendSMSTimer mSmsTimer;
    private String mVerificationId;
    private String mVerificationPhoneNumber;

    @Override
    public void setResponse(String response, String requestId) {

    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {

    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

    public void checkPasswordField(String passwordText) {
        if (!checkPasswordText(passwordText)) {
            getViewState().showPasswordLayoutException(R.string.registration_invalid_password);
        }
    }

    public void checkConfirmPasswordField(String confirmPasswordText, String passwordText) {
        if (!checkConfirmPasswordText(confirmPasswordText, passwordText)) {
            getViewState().showConfirmPasswordLayoutException(R.string.registration_invalid_confirm_password);
        }
    }

    public void checkAllFieldsAndStartNewFragment(FragmentManager fragmentManager, Bundle bundle, String phoneNumberText, String passwordText, String confirmPasswordText) {

       if (TextUtils.isEmpty(phoneNumberText)){
           getViewState().showPhoneNumberException(R.string.registration_dont_validate_phone_number);
           return;
       }
        if (!checkPasswordText(passwordText)) {
            getViewState().showPasswordLayoutException(R.string.registration_invalid_password);
            return;
        }
        if (!checkConfirmPasswordText(confirmPasswordText, passwordText)) {
            getViewState().showConfirmPasswordLayoutException(R.string.registration_invalid_confirm_password);
            return;
        }

        bundle.putString(UserBean.PHONE_NUMBER_EXTRA, phoneNumberText);
        bundle.putString(UserBean.CONFIRM_PASSWORD_EXTRA, confirmPasswordText);

        if (mStep_2 == null) {
            mStep_2 = RegistrationFragmentStep_2.newInstance(bundle);
        } else {
            mStep_2.setArguments(bundle);
        }
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, mStep_2)
                .addToBackStack(null)
                .commit();
    }

    private boolean checkPasswordText(String passwordText) {
        if (passwordText.length() < MINIMAL_PASSWORD_SIZE) {
            return false;
        }
        return true;
    }

    private boolean checkConfirmPasswordText(String confirmPasswordText, String passwordText) {
        if (TextUtils.isEmpty(passwordText) ||
                !passwordText.equals(confirmPasswordText)) {
            return false;
        }
        return true;
    }

    public void sendSMSCode(Activity activity,
                            String phoneNumberText,
                            PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks,
                            NetworkUtils networkUtils) {

        if (networkUtils.checkConnection(activity)) {
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    phoneNumberText,
                    60,
                    TimeUnit.SECONDS,
                    activity,
                    callbacks
            );
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public void resendSMSCode(Activity activity,
                              String phoneNumberText,
                              PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks,
                              PhoneAuthProvider.ForceResendingToken token,
                              NetworkUtils networkUtils) {

        if (networkUtils.checkConnection(activity)) {
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    phoneNumberText,        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    activity,               // Activity (for callback binding)
                    callbacks,         // OnVerificationStateChangedCallbacks
                    token);
        } else {
            getViewState().showFailDialog(R.string.network_error);
        }
    }

    public boolean checkValidNumber(IntlPhoneInput phoneNumberText) {
        boolean isValid = phoneNumberText.isValid();
        if (!isValid) {
            getViewState().showPhoneNumberException(R.string.registration_invalid_phone_number);
        }
        return isValid;
    }

    public void setChronometerTimer() {
        mSmsTimer = new ResendSMSTimer(60000, 1000);
        mSmsTimer.start();
    }

    public void cancelChronometer() {
        if (mSmsTimer != null) {
            mSmsTimer.cancel();
        }
    }

    public void updateUIVerificationFailed() {
        getViewState().updateUIVerificationFailedFragment();
    }

    public void updateUICodeSent() {
        getViewState().updateUICodeSentFragment();
    }

    public void updateUIOnComplete() {
        getViewState().updateUIOnCompleteFragment();
    }

    public void setVerificationId(String verificationId) {
        mVerificationId = verificationId;
    }
    public String getVerificationId() {
        return mVerificationId;
    }

    public void setVerificationPhoneNumber(String verificationPhoneNumber) {
        mVerificationPhoneNumber = verificationPhoneNumber;
    }

    public String getVerificationPhoneNumber() {
        return mVerificationPhoneNumber;
    }

    private class ResendSMSTimer extends CountDownTimer {

        public ResendSMSTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long l) {
            getViewState().showResendSMSTimer(String.valueOf(l / 1000));
        }

        @Override
        public void onFinish() {
            cancelChronometer();
            getViewState().showResendSMSButton();
        }
    }
}
