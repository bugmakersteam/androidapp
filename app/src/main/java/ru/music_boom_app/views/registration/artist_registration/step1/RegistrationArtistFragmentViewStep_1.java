package ru.music_boom_app.views.registration.artist_registration.step1;

import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Markin Andrey on 25.01.2018.
 */
@StateStrategyType(SingleStateStrategy.class)
public interface RegistrationArtistFragmentViewStep_1 extends MvpView {
    void showCheckNameLayoutException(@StringRes int stringExceptionId);
    void showCheckSurnameLayoutException(@StringRes int stringExceptionId);
    void showCheckNicknameLayoutException(@StringRes int stringExceptionId);
    void showCheckCountryLayoutException(@StringRes int stringExceptionId);
    void showCheckCityLayoutException(@StringRes int stringExceptionId);
    void showCheckBirthdayLayoutException(@StringRes int stringExceptionId);
    void showCheckMusicalInstrumentLayoutException(@StringRes int stringExceptionId);
    void showCheckGenreLayoutException(@StringRes int stringExceptionId);
    void showBirthdayDate(String date);
}
