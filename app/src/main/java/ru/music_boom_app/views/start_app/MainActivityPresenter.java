package ru.music_boom_app.views.start_app;

import com.arellomobile.mvp.InjectViewState;

import java.io.File;

import ru.music_boom_app.presenters.BasePresenter;

/**
 * @author Markin Andrey on 27.11.2017.
 */
@InjectViewState
public class MainActivityPresenter extends BasePresenter<MainActivityView> {

    @Override
    public void setResponse(String response, String requestId) {
    }

    @Override
    public void setFileResponse(File fileResponse, String requestId) {

    }

    @Override
    public void showFailDialog(int dialogErrorMessageId) {
        getViewState().showfailDialog(dialogErrorMessageId);
    }

    @Override
    public void showProgressBar() {
        getViewState().showProgress();
    }

    @Override
    public void hideProgressBar() {
        getViewState().hideProgress();
    }
}
