package ru.music_boom_app.views.start_app;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.ProgressBar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import ru.music_boom_app.R;
import ru.music_boom_app.views.authorization.AuthorizationFragment;
import ru.music_boom_app.views.base.BaseApplicationActivity;
import ru.music_boom_app.views.base.BaseApplicationAlertDialogFragment;

public class StartActivity extends BaseApplicationActivity implements MainActivityView {

    private static final String AUTHORIZATION_FRAGMENT_TAG = "authorization_fragment";

    private ProgressBar mProgressBar;

    @InjectPresenter
    MainActivityPresenter mMainActivityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_layout);
        mProgressBar = findViewById(R.id.progressBar);
        if (savedInstanceState == null) {
            AuthorizationFragment authorizationFragment = AuthorizationFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, authorizationFragment, AUTHORIZATION_FRAGMENT_TAG)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                AuthorizationFragment fragment = (AuthorizationFragment) getSupportFragmentManager().findFragmentByTag(AUTHORIZATION_FRAGMENT_TAG);

                if (fragment != null) {
                    fragment.checkVkData(res.userId,
                            res.accessToken,
                            res.email);
                } else {
                    mMainActivityPresenter.showFailDialog(R.string.authorization_vk_fail_message);
                }
            }

            @Override
            public void onError(VKError error) {
                mMainActivityPresenter.showFailDialog(R.string.authorization_vk_fail_message);
            }
        }))
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showfailDialog(int dialogErrorMessageId) {
        BaseApplicationAlertDialogFragment dialogFragment = BaseApplicationAlertDialogFragment.newDialog(
                getString(R.string.request_fail_message),
                getString(dialogErrorMessageId),
                getString(R.string.string_OK)
        );
        dialogFragment.show(getSupportFragmentManager(), FAIL_DIALOG);
    }

    @Override
    public boolean onSupportNavigateUp() {
        getSupportFragmentManager().popBackStack();
        return true;
    }
}
