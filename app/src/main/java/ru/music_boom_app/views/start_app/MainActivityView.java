package ru.music_boom_app.views.start_app;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Markin Andrey on 27.11.2017.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface MainActivityView extends MvpView {
    void showProgress();
    void hideProgress();
    void showfailDialog(int dialogErrorMessageId);
}

