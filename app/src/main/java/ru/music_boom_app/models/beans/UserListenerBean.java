package ru.music_boom_app.models.beans;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Markin Andrey on 11.12.2017.
 */
public class UserListenerBean extends UserBean implements Parcelable {
    /**
     * Количество всех артистов, которым задонатил слушатель
     */
    private int mAllDonatedArtist;

    public UserListenerBean() {
    }

    protected UserListenerBean(Parcel in) {
        mAllDonatedArtist = in.readInt();
        mSessionId = in.readString();
        mId = in.readLong();
        mEmail = in.readString();
        mCountry = in.readString();
        mCity = in.readString();
        mUserType = in.readString();
        mIsAllowsOfPersonalData = in.readByte() != 0;
        mPhoneNumber = in.readString();
        mAvatar = in.readString();
    }

    public static final Creator<UserListenerBean> CREATOR = new Creator<UserListenerBean>() {
        @Override
        public UserListenerBean createFromParcel(Parcel in) {
            return new UserListenerBean(in);
        }

        @Override
        public UserListenerBean[] newArray(int size) {
            return new UserListenerBean[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mAllDonatedArtist);
        parcel.writeString(mSessionId);
        parcel.writeLong(mId);
        parcel.writeString(mEmail);
        parcel.writeString(mCountry);
        parcel.writeString(mCity);
        parcel.writeString(mUserType);
        parcel.writeByte((byte) (mIsAllowsOfPersonalData ? 1 : 0));
        parcel.writeString(mPhoneNumber);
        parcel.writeString(mAvatar);
    }

    public int getAllDonatedArtist() {
        return mAllDonatedArtist;
    }

    public void setAllDonatedArtist(int allDonatedArtist) {
        mAllDonatedArtist = allDonatedArtist;
    }
}
