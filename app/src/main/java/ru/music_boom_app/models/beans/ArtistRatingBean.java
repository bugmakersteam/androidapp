package ru.music_boom_app.models.beans;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Markin Andrey on 13.12.2017.
 */
public class ArtistRatingBean implements Parcelable {
    private double mCityRating;
    private double mCountryRating;

    public ArtistRatingBean() {
    }

    protected ArtistRatingBean(Parcel in) {
        mCityRating = in.readDouble();
        mCountryRating = in.readDouble();
    }

    public static final Creator<ArtistRatingBean> CREATOR = new Creator<ArtistRatingBean>() {
        @Override
        public ArtistRatingBean createFromParcel(Parcel in) {
            return new ArtistRatingBean(in);
        }

        @Override
        public ArtistRatingBean[] newArray(int size) {
            return new ArtistRatingBean[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(mCityRating);
        parcel.writeDouble(mCountryRating);
    }

    public double getCityRating() {
        return mCityRating;
    }

    public void setCityRating(double cityRating) {
        mCityRating = cityRating;
    }

    public double getCountryRating() {
        return mCountryRating;
    }

    public void setCountryRating(double countryRating) {
        mCountryRating = countryRating;
    }
}
