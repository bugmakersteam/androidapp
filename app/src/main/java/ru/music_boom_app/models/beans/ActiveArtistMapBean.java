package ru.music_boom_app.models.beans;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.Objects;

/**
 * @author Markin Andrey on 13.04.2018.
 */
public class ActiveArtistMapBean implements Parcelable {
    private long mArtistId;
    private String mAvatarUrl;
    private double mLongitude;
    private double mLatitude;
    private Bitmap mAvatar;

    public ActiveArtistMapBean() {

    }

    protected ActiveArtistMapBean(Parcel in) {
        mArtistId = in.readLong();
        mAvatarUrl = in.readString();
        mLongitude = in.readDouble();
        mLatitude = in.readDouble();
        mAvatar = in.readParcelable(Bitmap.class.getClassLoader());
    }

    public static final Creator<ActiveArtistMapBean> CREATOR = new Creator<ActiveArtistMapBean>() {
        @Override
        public ActiveArtistMapBean createFromParcel(Parcel in) {
            return new ActiveArtistMapBean(in);
        }

        @Override
        public ActiveArtistMapBean[] newArray(int size) {
            return new ActiveArtistMapBean[size];
        }
    };

    public long getArtistId() {
        return mArtistId;
    }

    public void setArtistId(long artistId) {
        mArtistId = artistId;
    }

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        mAvatarUrl = avatarUrl;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public Bitmap getAvatar() {
        return mAvatar;
    }

    public void setAvatar(Bitmap avatar) {
        mAvatar = avatar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActiveArtistMapBean that = (ActiveArtistMapBean) o;
        return mArtistId == that.mArtistId &&
                Double.compare(that.mLongitude, mLongitude) == 0 &&
                Double.compare(that.mLatitude, mLatitude) == 0 &&
                Objects.equal(mAvatarUrl, that.mAvatarUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(mArtistId, mAvatarUrl, mLongitude, mLatitude);
    }

    @Override
    public String toString() {
        return "ActiveArtistMapBean{" +
                "mArtistId=" + mArtistId +
                ", mAvatarUrl='" + mAvatarUrl + '\'' +
                ", mLongitude=" + mLongitude +
                ", mLatitude=" + mLatitude +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mArtistId);
        dest.writeString(mAvatarUrl);
        dest.writeDouble(mLongitude);
        dest.writeDouble(mLatitude);
        dest.writeParcelable(mAvatar, flags);
    }
}
