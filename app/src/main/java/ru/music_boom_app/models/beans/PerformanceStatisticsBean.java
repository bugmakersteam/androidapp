package ru.music_boom_app.models.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.Objects;

/**
 * @author Markin Andrey on 13.12.2017.
 */
public class PerformanceStatisticsBean implements Parcelable {

    /**
     * Всего выступлений
     */
    private int mAllPerformances;

    /**
     * Суммарное количество часов выступлений в текущем месяце
     */
    private int mThisMonthHours;

    /**
     * Суммарное количество заработанных средств в текущем месяце
     */
    private double mThisMonthMoney;

    /**
     * Средняя продолжитьтельность одного выступления в текущем месяце
     */
    private double mAveragePerformanceTime;

    public PerformanceStatisticsBean() {
    }

    protected PerformanceStatisticsBean(Parcel in) {
        mAllPerformances = in.readInt();
        mThisMonthHours = in.readInt();
        mThisMonthMoney = in.readDouble();
        mAveragePerformanceTime = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mAllPerformances);
        parcel.writeInt(mThisMonthHours);
        parcel.writeDouble(mThisMonthMoney);
        parcel.writeDouble(mAveragePerformanceTime);
    }

    public static final Creator<PerformanceStatisticsBean> CREATOR = new Creator<PerformanceStatisticsBean>() {
        @Override
        public PerformanceStatisticsBean createFromParcel(Parcel in) {
            return new PerformanceStatisticsBean(in);
        }

        @Override
        public PerformanceStatisticsBean[] newArray(int size) {
            return new PerformanceStatisticsBean[size];
        }
    };

    public int getAllPerformances() {
        return mAllPerformances;
    }

    public void setAllPerformances(int allPerformances) {
        mAllPerformances = allPerformances;
    }

    public int getThisMonthHours() {
        return mThisMonthHours;
    }

    public void setThisMonthHours(int thisMonthHours) {
        mThisMonthHours = thisMonthHours;
    }

    public double getThisMonthMoney() {
        return mThisMonthMoney;
    }

    public void setThisMonthMoney(double thisMonthMoney) {
        mThisMonthMoney = thisMonthMoney;
    }

    public double getAveragePerformanceTime() {
        return mAveragePerformanceTime;
    }

    public void setAveragePerformanceTime(double averagePerformanceTime) {
        mAveragePerformanceTime = averagePerformanceTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PerformanceStatisticsBean that = (PerformanceStatisticsBean) o;
        return mAllPerformances == that.mAllPerformances &&
                mThisMonthHours == that.mThisMonthHours &&
                Double.compare(that.mThisMonthMoney, mThisMonthMoney) == 0 &&
                Double.compare(that.mAveragePerformanceTime, mAveragePerformanceTime) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(mAllPerformances, mThisMonthHours, mThisMonthMoney, mAveragePerformanceTime);
    }

    @Override
    public String toString() {
        return "PerformanceStatisticsBean{" +
                "mAllPerformances=" + mAllPerformances +
                ", mThisMonthHours=" + mThisMonthHours +
                ", mThisMonthMoney=" + mThisMonthMoney +
                ", mAveragePerformanceTime=" + mAveragePerformanceTime +
                '}';
    }
}
