package ru.music_boom_app.models.beans;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * @author Markin Andrey on 11.12.2017.
 */
public class UserArtistBean extends UserBean implements Parcelable {
    public static final String NAME_EXTRA = "name";
    public static final String SURNAME_EXTRA = "surname";
    public static final String PATRONYMIC_EXTRA = "patronymic";
    public static final String NICKNAME_EXTRA = "nickname";
    public static final String BIRTHDAY_EXTRA = "birthday";
    public static final String CREATIVITY_EXTRA = "creativity";
    public static final String MUSICAL_INSTRUMENT_EXTRA = "musical_instrument";
    public static final String GENRE_EXTRA = "genre";
    public static final String EMAIL_EXTRA = "email";

    /**
     * Имя артиста
     */
    private String mName;

    /**
     * Фамилия артиста
     */
    private String mSurname;

    /**
     * Отчество артиста
     */
    private String mPatronymic;

    /**
     * Никнэйм/алиас артиста
     */
    private String mNickname;

    /**
     * Дата рождения артиста
     */
    private String mBirthday;

    /**
     * Дата регитсрации артиста
     */
    private String mDateOfRegistration;

    /**
     * Вид деятельности артиста
     */
    private String mCreativity;

    /**
     * Жанр артиста
     */
    private String mGenre;

    /**
     * Музыкальный инструмент артиста (если музыкант)
     */
    private String mInstrument;

    /**
     * Ссылка на профиль Вконтакте артиста
     */
    private String mVKLink;

    /**
     * Алиас в телешрамме артиста
     */
    private String mTLGAlias;

    /**
     * Номер телефона в ВатсАпп артиста
     */
    private String mWhatsAppNumber;

    /**
     * Параметр отображающий согласие артиста с договором сотрудничества. True - согласен, False - не согласен.
     */
    private boolean mIsArtistContract;

    /**
     * Параметр отображающий готовность артиста принимать заказы для выступлений. True - готов, False - не готов.
     */
    private boolean mIsOrdered;

    /**
     * Параметр отображающий наличие у артитста првязанного номера карты для вывода средств. True - привязан, False - не привязан.
     */
    private boolean mIsLinkedCard;

    /**
     * Список адресов фотографий польователя
     */
    private List<String> mPhotos;

    /**
     * Статистика выступлений
     */
    private PerformanceStatisticsBean mPerformanceStatisticsBean;

    /**
     * Финансовая статистика
     */
    private FinanceStatisticsBean mFinanceStatisticsBean;

    /**
     * Рейтинг
     */
    private ArtistRatingBean mArtistRatingBean;

    /**
     * Номер карты
     */
    private String mCardNumber;

    public UserArtistBean() {

    }

    protected UserArtistBean(Parcel in) {
        mName = in.readString();
        mSurname = in.readString();
        mPatronymic = in.readString();
        mNickname = in.readString();
        mBirthday = in.readString();
        mDateOfRegistration = in.readString();
        mCreativity = in.readString();
        mGenre = in.readString();
        mInstrument = in.readString();
        mVKLink = in.readString();
        mTLGAlias = in.readString();
        mWhatsAppNumber = in.readString();
        mIsArtistContract = in.readByte() != 0;
        mIsOrdered = in.readByte() != 0;
        mIsLinkedCard = in.readByte() != 0;
        mAvatar = in.readString();
        mPhotos = in.createStringArrayList();
        mPerformanceStatisticsBean = in.readParcelable(PerformanceStatisticsBean.class.getClassLoader());
        mFinanceStatisticsBean = in.readParcelable(FinanceStatisticsBean.class.getClassLoader());
        mArtistRatingBean = in.readParcelable(ArtistRatingBean.class.getClassLoader());
        mCardNumber = in.readString();
        mSessionId = in.readString();
        mId = in.readLong();
        mEmail = in.readString();
        mCountry = in.readString();
        mCity = in.readString();
        mUserType = in.readString();
        mIsAllowsOfPersonalData = in.readByte() != 0;
        mPhoneNumber = in.readString();
    }

    public static final Creator<UserArtistBean> CREATOR = new Creator<UserArtistBean>() {
        @Override
        public UserArtistBean createFromParcel(Parcel in) {
            return new UserArtistBean(in);
        }

        @Override
        public UserArtistBean[] newArray(int size) {
            return new UserArtistBean[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mName);
        parcel.writeString(mSurname);
        parcel.writeString(mPatronymic);
        parcel.writeString(mNickname);
        parcel.writeString(mBirthday);
        parcel.writeString(mDateOfRegistration);
        parcel.writeString(mCreativity);
        parcel.writeString(mGenre);
        parcel.writeString(mInstrument);
        parcel.writeString(mVKLink);
        parcel.writeString(mTLGAlias);
        parcel.writeString(mWhatsAppNumber);
        parcel.writeByte((byte) (mIsArtistContract ? 1 : 0));
        parcel.writeByte((byte) (mIsOrdered ? 1 : 0));
        parcel.writeByte((byte) (mIsLinkedCard ? 1 : 0));
        parcel.writeString(mAvatar);
        parcel.writeStringList(mPhotos);
        parcel.writeParcelable(mPerformanceStatisticsBean, i);
        parcel.writeParcelable(mFinanceStatisticsBean, i);
        parcel.writeParcelable(mArtistRatingBean, i);
        parcel.writeString(mCardNumber);
        parcel.writeString(mSessionId);
        parcel.writeLong(mId);
        parcel.writeString(mEmail);
        parcel.writeString(mCountry);
        parcel.writeString(mCity);
        parcel.writeString(mUserType);
        parcel.writeByte((byte) (mIsAllowsOfPersonalData ? 1 : 0));
        parcel.writeString(mPhoneNumber);
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getSurname() {
        return mSurname;
    }

    public void setSurname(String surname) {
        mSurname = surname;
    }

    public String getPatronymic() {
        return mPatronymic;
    }

    public void setPatronymic(String patronymic) {
        mPatronymic = patronymic;
    }

    public String getNickname() {
        return mNickname;
    }

    public void setNickname(String nickname) {
        mNickname = nickname;
    }

    public String getBirthday() {
        return mBirthday;
    }

    public void setBirthday(String birthday) {
        mBirthday = birthday;
    }

    public String getDateOfRegistration() {
        return mDateOfRegistration;
    }

    public void setDateOfRegistration(String dateOfRegistration) {
        mDateOfRegistration = dateOfRegistration;
    }

    public String getCreativity() {
        return mCreativity;
    }

    public void setCreativity(String creativity) {
        mCreativity = creativity;
    }

    public String getGenre() {
        return mGenre;
    }

    public void setGenre(String genre) {
        mGenre = genre;
    }

    public String getInstrument() {
        return mInstrument;
    }

    public void setInstrument(String instrument) {
        mInstrument = instrument;
    }

    public String getVKLink() {
        return mVKLink;
    }

    public void setVKLink(String VKLink) {
        mVKLink = VKLink;
    }

    public String getTLGAlias() {
        return mTLGAlias;
    }

    public void setTLGAlias(String TLGAlias) {
        mTLGAlias = TLGAlias;
    }

    public String getWhatsAppNumber() {
        return mWhatsAppNumber;
    }

    public void setWhatsAppNumber(String whatsAppNumber) {
        mWhatsAppNumber = whatsAppNumber;
    }

    public boolean isArtistContract() {
        return mIsArtistContract;
    }

    public void setArtistContract(boolean artistContract) {
        mIsArtistContract = artistContract;
    }

    public boolean isOrdered() {
        return mIsOrdered;
    }

    public void setOrdered(boolean ordered) {
        mIsOrdered = ordered;
    }

    public boolean isLinkedCard() {
        return mIsLinkedCard;
    }

    public void setLinkedCard(boolean linkedCard) {
        mIsLinkedCard = linkedCard;
    }

    public PerformanceStatisticsBean getPerformanceStatisticsBean() {
        return mPerformanceStatisticsBean;
    }

    public void setPerformanceStatisticsBean(PerformanceStatisticsBean performanceStatisticsBean) {
        mPerformanceStatisticsBean = performanceStatisticsBean;
    }

    public FinanceStatisticsBean getFinanceStatisticsBean() {
        return mFinanceStatisticsBean;
    }

    public void setFinanceStatisticsBean(FinanceStatisticsBean financeStatisticsBean) {
        mFinanceStatisticsBean = financeStatisticsBean;
    }

    public ArtistRatingBean getArtistRatingBean() {
        return mArtistRatingBean;
    }

    public void setArtistRatingBean(ArtistRatingBean artistRatingBean) {
        mArtistRatingBean = artistRatingBean;
    }

    public List<String> getPhotos() {
        return mPhotos;
    }

    public void setPhotos(List<String> photos) {
        mPhotos = photos;
    }

    public String getCardNumber() {
        return mCardNumber;
    }

    public void setCardNumber(String cardNumber) {
        mCardNumber = cardNumber;
    }
}
