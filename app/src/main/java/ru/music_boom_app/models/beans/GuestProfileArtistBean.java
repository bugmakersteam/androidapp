package ru.music_boom_app.models.beans;

import com.google.common.base.Objects;

import java.util.List;

/**
 * @author Markin Andrey on 17.04.2018.
 */
public class GuestProfileArtistBean {

    private String mArtistName;
    private String mCreativity;
    private String mInstrument;
    private String mGenre;
    private boolean mIsOrdered;
    private String mPhoneNumber;
    private String mVkLink;
    private String mWappNumber;
    private String mAvatarURL;
    private List<String> mPhotosURL;

    public String getArtistName() {
        return mArtistName;
    }

    public void setArtistName(String artistName) {
        mArtistName = artistName;
    }

    public String getCreativity() {
        return mCreativity;
    }

    public void setCreativity(String creativity) {
        mCreativity = creativity;
    }

    public String getInstrument() {
        return mInstrument;
    }

    public void setInstrument(String instrument) {
        mInstrument = instrument;
    }

    public String getGenre() {
        return mGenre;
    }

    public void setGenre(String genre) {
        mGenre = genre;
    }

    public boolean isOrdered() {
        return mIsOrdered;
    }

    public void setOrdered(boolean ordered) {
        mIsOrdered = ordered;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    public String getVkLink() {
        return mVkLink;
    }

    public void setVkLink(String vkLink) {
        mVkLink = vkLink;
    }

    public String getWappNumber() {
        return mWappNumber;
    }

    public void setWappNumber(String wappNumber) {
        mWappNumber = wappNumber;
    }

    public String getAvatarURL() {
        return mAvatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        mAvatarURL = avatarURL;
    }

    public List<String> getPhotosURL() {
        return mPhotosURL;
    }

    public void setPhotosURL(List<String> photosURL) {
        mPhotosURL = photosURL;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GuestProfileArtistBean that = (GuestProfileArtistBean) o;
        return mIsOrdered == that.mIsOrdered &&
                Objects.equal(mArtistName, that.mArtistName) &&
                Objects.equal(mCreativity, that.mCreativity) &&
                Objects.equal(mInstrument, that.mInstrument) &&
                Objects.equal(mGenre, that.mGenre) &&
                Objects.equal(mPhoneNumber, that.mPhoneNumber) &&
                Objects.equal(mVkLink, that.mVkLink) &&
                Objects.equal(mWappNumber, that.mWappNumber) &&
                Objects.equal(mAvatarURL, that.mAvatarURL) &&
                Objects.equal(mPhotosURL, that.mPhotosURL);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(mArtistName, mCreativity, mInstrument, mGenre, mIsOrdered, mPhoneNumber, mVkLink, mWappNumber, mAvatarURL, mPhotosURL);
    }

    @Override
    public String toString() {
        return "GuestProfileArtistBean{" +
                "mArtistName='" + mArtistName + '\'' +
                ", mCreativity='" + mCreativity + '\'' +
                ", mInstrument='" + mInstrument + '\'' +
                ", mGenre='" + mGenre + '\'' +
                ", mIsOrdered=" + mIsOrdered +
                ", mPhoneNumber='" + mPhoneNumber + '\'' +
                ", mVkLink='" + mVkLink + '\'' +
                ", mWappNumber='" + mWappNumber + '\'' +
                ", mAvatarURL='" + mAvatarURL + '\'' +
                ", mPhotosURL=" + mPhotosURL +
                '}';
    }
}
