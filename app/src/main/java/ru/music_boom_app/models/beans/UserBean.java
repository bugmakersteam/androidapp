package ru.music_boom_app.models.beans;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Markin Andrey on 11.12.2017.
 */
public class UserBean implements Parcelable {
    public static final String PROVIDER = "provider";
    public static final String REGISTERED_ID = "registered_id";

    public static final String PHONE_NUMBER_EXTRA = "phone_number";
    public static final String CONFIRM_PASSWORD_EXTRA = "confirm_password";
    public static final String COUNTRY_EXTRA = "country";
    public static final String CITY_EXTRA = "city";

    protected String mSessionId;
    protected long mId;
    protected String mEmail;
    protected String mCountry;
    protected String mCity;
    protected String mUserType;
    protected boolean mIsAllowsOfPersonalData;
    /**
     * Номер телефона артиста
     */
    protected String mPhoneNumber;
    /**
     * Адрес аватара
     */
    protected String mAvatar;

    public UserBean(){

    }

    protected UserBean(Parcel in) {
        mSessionId = in.readString();
        mId = in.readLong();
        mEmail = in.readString();
        mCountry = in.readString();
        mCity = in.readString();
        mUserType = in.readString();
        mIsAllowsOfPersonalData = in.readByte() != 0;
        mPhoneNumber = in.readString();
        mAvatar = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mSessionId);
        dest.writeLong(mId);
        dest.writeString(mEmail);
        dest.writeString(mCountry);
        dest.writeString(mCity);
        dest.writeString(mUserType);
        dest.writeByte((byte) (mIsAllowsOfPersonalData ? 1 : 0));
        dest.writeString(mPhoneNumber);
        dest.writeString(mAvatar);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserBean> CREATOR = new Creator<UserBean>() {
        @Override
        public UserBean createFromParcel(Parcel in) {
            return new UserBean(in);
        }

        @Override
        public UserBean[] newArray(int size) {
            return new UserBean[size];
        }
    };

    public String getSessionId() {
        return mSessionId;
    }

    public void setSessionId(String sessionId) {
        mSessionId = sessionId;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public boolean isAllowsOfPersonalData() {
        return mIsAllowsOfPersonalData;
    }

    public void setAllowsOfPersonalData(boolean allowsOfPersonalData) {
        mIsAllowsOfPersonalData = allowsOfPersonalData;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        mUserType = userType;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    public String getAvatar() {
        return mAvatar;
    }

    public void setAvatar(String avatar) {
        mAvatar = avatar;
    }
}
