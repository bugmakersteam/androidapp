package ru.music_boom_app.models.beans;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Markin Andrey on 11.12.2017.
 */
public class FinanceStatisticsBean implements Parcelable {
    private String mAllEarnedMoney;
    private String mAllDerivedMoney;
    private String mCurrentBalance;

    public FinanceStatisticsBean() {
    }

    protected FinanceStatisticsBean(Parcel in) {
        mAllEarnedMoney = in.readString();
        mAllDerivedMoney = in.readString();
        mCurrentBalance = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mAllEarnedMoney);
        parcel.writeString(mAllDerivedMoney);
        parcel.writeString(mCurrentBalance);
    }

    public static final Creator<FinanceStatisticsBean> CREATOR = new Creator<FinanceStatisticsBean>() {
        @Override
        public FinanceStatisticsBean createFromParcel(Parcel in) {
            return new FinanceStatisticsBean(in);
        }

        @Override
        public FinanceStatisticsBean[] newArray(int size) {
            return new FinanceStatisticsBean[size];
        }
    };

    public String getAllEarnedMoney() {
        return mAllEarnedMoney;
    }

    public void setAllEarnedMoney(String allEarnedMoney) {
        mAllEarnedMoney = allEarnedMoney;
    }

    public String getAllDerivedMoney() {
        return mAllDerivedMoney;
    }

    public void setAllDerivedMoney(String allDerivedMoney) {
        mAllDerivedMoney = allDerivedMoney;
    }

    public String getCurrentBalance() {
        return mCurrentBalance;
    }

    public void setCurrentBalance(String currentBalance) {
        mCurrentBalance = currentBalance;
    }
}
