package ru.music_boom_app.models.parsers;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Markin Andrey on 09.03.2018.
 */
public class FbDataResponseParser {


    public static String getName(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            String result = jsonObject.optString("first_name", "");
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getSurname(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            String result = jsonObject.optString("last_name", "");
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getBirthday(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            String result = jsonObject.optString("birthday", "");
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getCity(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            String result = jsonObject.optString("city", "");
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getCountry(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            String result = jsonObject.optString("country", "");
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getEmail (String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            String result = jsonObject.optString("email", "");
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getId (String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            String result = jsonObject.optString("id", "");
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}
