package ru.music_boom_app.models.parsers;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.music_boom_app.models.beans.ArtistRatingBean;
import ru.music_boom_app.models.beans.FinanceStatisticsBean;
import ru.music_boom_app.models.beans.PerformanceStatisticsBean;
import ru.music_boom_app.models.beans.UserArtistBean;


/**
 * @author Markin Andrey on 27.02.2018.
 */
public class AuthorizationResponseUserArtistBeanParser extends BaseResponseParser {
    private static final String SESSION_ID = "sessionId";
    private static final String EMPTY = "";
    private static final String USER_TYPE = "userType";
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String SURNAME = "surname";
    private static final String PATRONYMIC = "patronymic";
    private static final String BIRTHDAY = "birthday";
    private static final String NICKNAME = "nickname";
    private static final String COUNTRY = "country";
    private static final String CITY = "city";
    private static final String PHONE_NUMBER = "phoneNumber";
    private static final String EMAIL = "email";
    private static final String CREATIVITY = "creativity";
    private static final String INSTRUMENT = "instrument";
    private static final String GENRE = "genre";
    private static final String VK_LINK = "vk";
    private static final String TLG_ALIAS = "tlg";
    private static final String WAPP = "wapp";
    private static final String IS_ORDERED = "isOrdered";
    private static final String REG_DATE = "regDate";
    private static final String IS_LINKED_CARD = "linkedCard";
    private static final String AVATAR = "avatar";
    private static final String PHOTOS = "photos";
    private static final String ALL_EARNED_MONEY = "allEarnedMoney";
    private static final String ALL_DERIVED_MONEY = "allDerivedMoney";
    private static final String CURRENT_BALANCE = "currentBalance";
    private static final String COUNTRY_RATING = "countryRating";
    private static final String CITY_RATING = "cityRating";
    private static final String STAT_OF_PERFOMANCE = "statOfPerformance";
    private static final String ALL_PERFOMANCES = "allPerformances";
    private static final String HOURS_OF_MONTH = "hoursOfMonth";
    private static final String MONEY_OF_MONTH = "moneyOfMonth";
    private static final String AVERAGE_PERFOMANCE_TIME = "averagePerformanceTime";
    private static final String CARD_NUMBER = "cardNumber";



    public static UserArtistBean parseUserArtist(String json) {
        UserArtistBean userArtistBean = new UserArtistBean();
        try {
            JSONObject mainJsonObject = new JSONObject(json);
            userArtistBean.setSessionId(mainJsonObject.optString(SESSION_ID, EMPTY));

            JSONObject userJsonObject = mainJsonObject.getJSONObject(USER_OBJECT);
            userArtistBean.setUserType(userJsonObject.optString(USER_TYPE, EMPTY));
            userArtistBean.setId(userJsonObject.optLong(ID,0));
            userArtistBean.setName(userJsonObject.optString(NAME, EMPTY));
            userArtistBean.setSurname(userJsonObject.optString(SURNAME, EMPTY));
            userArtistBean.setPatronymic(userJsonObject.optString(PATRONYMIC, EMPTY));

            String date = userJsonObject.optString(BIRTHDAY, EMPTY);
            if (!TextUtils.isEmpty(date)){
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date formatDate = dateFormat.parse(date);
                    SimpleDateFormat androidFormat = new SimpleDateFormat("dd.MM.yyyy");
                    date = androidFormat.format(formatDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            userArtistBean.setBirthday(date);

            userArtistBean.setNickname(userJsonObject.optString(NICKNAME, EMPTY));
            userArtistBean.setCountry(userJsonObject.optString(COUNTRY, EMPTY));
            userArtistBean.setCity(userJsonObject.optString(CITY,EMPTY));
            userArtistBean.setPhoneNumber(userJsonObject.optString(PHONE_NUMBER, EMPTY));
            userArtistBean.setEmail(userJsonObject.optString(EMAIL, EMPTY));
            userArtistBean.setCreativity(userJsonObject.optString(CREATIVITY, EMPTY));
            userArtistBean.setInstrument(userJsonObject.optString(INSTRUMENT, EMPTY));
            userArtistBean.setGenre(userJsonObject.optString(GENRE, EMPTY));
            userArtistBean.setCardNumber(userJsonObject.optString(CARD_NUMBER, EMPTY));
            userArtistBean.setVKLink(userJsonObject.optString(VK_LINK, EMPTY));
            userArtistBean.setTLGAlias(userJsonObject.optString(TLG_ALIAS, EMPTY));
            userArtistBean.setWhatsAppNumber(userJsonObject.optString(WAPP, EMPTY));
            userArtistBean.setOrdered(userJsonObject.optBoolean(IS_ORDERED, false));
            userArtistBean.setDateOfRegistration(userJsonObject.optString(REG_DATE, EMPTY));
            userArtistBean.setLinkedCard(userJsonObject.optBoolean(IS_LINKED_CARD, false));
            userArtistBean.setAvatar(userJsonObject.optString(AVATAR, EMPTY));

            JSONArray jsonPhotosUrlArray = userJsonObject.getJSONArray(PHOTOS);
            List<String> photosUrl = new ArrayList<>();
            for (int i = 0; i < jsonPhotosUrlArray.length(); i++){
                photosUrl.add(jsonPhotosUrlArray.optString(i, EMPTY));
            }
            userArtistBean.setPhotos(photosUrl);

            FinanceStatisticsBean financeStatisticsBean = new FinanceStatisticsBean();
            financeStatisticsBean.setAllEarnedMoney(userJsonObject.optString(ALL_EARNED_MONEY, EMPTY));
            financeStatisticsBean.setAllDerivedMoney(userJsonObject.optString(ALL_DERIVED_MONEY, EMPTY));
            financeStatisticsBean.setCurrentBalance(userJsonObject.optString(CURRENT_BALANCE, EMPTY));
            userArtistBean.setFinanceStatisticsBean(financeStatisticsBean);

            ArtistRatingBean artistRatingBean = new ArtistRatingBean();
            artistRatingBean.setCountryRating(userJsonObject.optDouble(COUNTRY_RATING, 0));
            artistRatingBean.setCityRating(userJsonObject.optDouble(CITY_RATING, 0));
            userArtistBean.setArtistRatingBean(artistRatingBean);

            PerformanceStatisticsBean performanceStatisticsBean = new PerformanceStatisticsBean();
            JSONObject perfomanceStatiscticsJsonObject = userJsonObject.getJSONObject(STAT_OF_PERFOMANCE);
            performanceStatisticsBean.setAllPerformances(perfomanceStatiscticsJsonObject.optInt(ALL_PERFOMANCES, 0));
            performanceStatisticsBean.setThisMonthHours(perfomanceStatiscticsJsonObject.optInt(HOURS_OF_MONTH, 0));
            performanceStatisticsBean.setThisMonthMoney(perfomanceStatiscticsJsonObject.optDouble(MONEY_OF_MONTH, 0));
            performanceStatisticsBean.setAveragePerformanceTime(perfomanceStatiscticsJsonObject.optDouble(AVERAGE_PERFOMANCE_TIME, 0));
            userArtistBean.setPerformanceStatisticsBean(performanceStatisticsBean);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return userArtistBean;
    }
}
