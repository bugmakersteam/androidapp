package ru.music_boom_app.models.parsers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.CopyOnWriteArrayList;

import ru.music_boom_app.models.beans.ActiveArtistMapBean;

/**
 * @author Markin Andrey on 13.04.2018.
 */
public class ActiveUserMapBeanParser {
    private static final String ARRAY_ARTISTS = "artists";
    private static final String ARTIST_ID = "artist_id";
    private static final String ARTIST_AVATAR_URL = "icon_url";
    private static final String ARTIST_LONGITUDE = "longitude";
    private static final String ARTIST_LATITUDE = "latitude";

    public static CopyOnWriteArrayList<ActiveArtistMapBean> parseActiveUsersMapBean(String json) {
        CopyOnWriteArrayList<ActiveArtistMapBean> activeArtistMapBeans = new CopyOnWriteArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArrayArtists = jsonObject.getJSONArray(ARRAY_ARTISTS);
            for (int i = 0; i < jsonArrayArtists.length(); i++) {
                JSONObject activeArtistObject = jsonArrayArtists.getJSONObject(i);
                ActiveArtistMapBean bean = new ActiveArtistMapBean();
                bean.setArtistId(activeArtistObject.optLong(ARTIST_ID, 0));
                bean.setAvatarUrl(activeArtistObject.optString(ARTIST_AVATAR_URL, ""));
                bean.setLongitude(activeArtistObject.optDouble(ARTIST_LONGITUDE, 0));
                bean.setLatitude(activeArtistObject.optDouble(ARTIST_LATITUDE));
                activeArtistMapBeans.add(bean);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return activeArtistMapBeans;
    }
}
