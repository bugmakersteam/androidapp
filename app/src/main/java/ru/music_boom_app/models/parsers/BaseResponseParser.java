package ru.music_boom_app.models.parsers;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Markin Andrey on 27.02.2018.
 */
public class BaseResponseParser {
    public static final String LISTENER_TYPE = "LISTENER";
    public static final String ARTIST_TYPE = "ARTIST";

    private static final String STATUS_FIELD = "status";
    private static final String SUCCESS_STATUS = "SUCCESS";
    private static final String ERROR_STATUS = "ERROR";
    protected static final String USER_OBJECT = "user";
    private static final String USER_TYPE_FIELD = "userType";
    private static final String REGISTERED_FIELD = "registered";


    public static boolean checkSuccessResponse(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            String statusValue = jsonObject.getString(STATUS_FIELD);
            if (SUCCESS_STATUS.equals(statusValue)){
                return true;
            } else {
                return false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Проверка поля о необходимости регистрации юзера
     *
     * @param json
     * @return true - зарегитстрирован, false - не зарегистрирован
     */
    public static boolean checkRegisteredFieldResponse(String json){
        try {
            JSONObject mainJsonObject = new JSONObject(json);
            JSONObject userJsonObject = mainJsonObject.getJSONObject(USER_OBJECT);
            return userJsonObject.getBoolean(REGISTERED_FIELD);
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String checkUserTypeResponse(String json){
        try {
            JSONObject mainJsonObject = new JSONObject(json);
            JSONObject userJsonObject = mainJsonObject.getJSONObject(USER_OBJECT);
            return userJsonObject.getString(USER_TYPE_FIELD);
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String checkRegisteredIdResponse(String json){
        try {
            JSONObject mainJsonObject = new JSONObject(json);
            JSONObject userJsonObject = mainJsonObject.getJSONObject(USER_OBJECT);
            return userJsonObject.getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}
