package ru.music_boom_app.models.parsers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Markin Andrey on 28.02.2018.
 */
public class VkDataResponseParser {

    public static String getName(String json){
        try {
            JSONArray jsonArray = new JSONArray(json);
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            String value = jsonObject.optString("first_name", "");
            return value;
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getSurname(String json) {
        try {
            JSONArray jsonArray = new JSONArray(json);
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            String value = jsonObject.optString("last_name", "");
            return value;
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getBirthday(String json){
        try {
            JSONArray jsonArray = new JSONArray(json);
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            String value = jsonObject.optString("bdate", "");
            return value;
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getCity(String json){
        try {
            JSONArray jsonArray = new JSONArray(json);
            JSONObject mainJsonObject = jsonArray.getJSONObject(0);
            JSONObject object = mainJsonObject.getJSONObject("city");
            String value = object.optString("title", "");
            return value;
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getCountry(String json){
        try {
            JSONArray jsonArray = new JSONArray(json);
            JSONObject mainJsonObject = jsonArray.getJSONObject(0);
            JSONObject object = mainJsonObject.getJSONObject("country");
            String value = object.optString("title", "");
            return value;
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}
