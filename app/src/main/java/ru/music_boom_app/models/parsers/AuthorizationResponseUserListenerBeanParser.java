package ru.music_boom_app.models.parsers;

import org.json.JSONException;
import org.json.JSONObject;

import ru.music_boom_app.models.beans.UserListenerBean;

/**
 * @author Markin Andrey on 27.02.2018.
 */
public class AuthorizationResponseUserListenerBeanParser extends BaseResponseParser {
    private static final String SESSION_ID = "sessionId";
    private static final String EMPTY = "";
    private static final String USER_TYPE = "userType";
    private static final String ID = "id";
    private static final String COUNTRY = "country";
    private static final String CITY = "city";
    private static final String PHONE_NUMBER = "phoneNumber";
    private static final String EMAIL = "email";
    private static final String ALL_DONATED_ARTISTS = "allDonatedArtists";
    private static final String AVATAR = "avatar";

    public static UserListenerBean paresUserListener(String json){
        UserListenerBean userListenerBean = new UserListenerBean();
        try {
            JSONObject mainJsonObject = new JSONObject(json);
            userListenerBean.setSessionId(mainJsonObject.optString(SESSION_ID, EMPTY));
            JSONObject userJsonObject = mainJsonObject.getJSONObject(USER_OBJECT);
            userListenerBean.setUserType(userJsonObject.optString(USER_TYPE, EMPTY));
            userListenerBean.setId(userJsonObject.optLong(ID,0));
            userListenerBean.setCountry(userJsonObject.optString(COUNTRY, EMPTY));
            userListenerBean.setCity(userJsonObject.optString(CITY,EMPTY));
            userListenerBean.setPhoneNumber(userJsonObject.optString(PHONE_NUMBER, EMPTY));
            userListenerBean.setEmail(userJsonObject.optString(EMAIL, EMPTY));
            userListenerBean.setAllDonatedArtist(userJsonObject.optInt(ALL_DONATED_ARTISTS,0));
            userListenerBean.setAvatar(userJsonObject.optString(AVATAR, EMPTY));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return userListenerBean;
    }
}
