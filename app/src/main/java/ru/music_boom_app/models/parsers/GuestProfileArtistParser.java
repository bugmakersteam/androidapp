package ru.music_boom_app.models.parsers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ru.music_boom_app.models.beans.GuestProfileArtistBean;

/**
 * @author Markin Andrey on 17.04.2018.
 */
public class GuestProfileArtistParser {

    private static final String ARTIST_NAME = "publicName";
    private static final String CREATIVITY = "creativity";
    private static final String INSTRUMENT = "instrument";
    private static final String GENRE = "genre";
    private static final String ORDERED = "isOrdered";
    private static final String PHONE_NUMBER = "phoneNumber";
    private static final String VK = "vk";
    private static final String WAPP = "whatsApp";
    private static final String AVATAR = "avatar";
    private static final String PHOTOS = "photos";
    private static final String EMPTY = "";

    public static GuestProfileArtistBean parseArtistBean(String json) {
        GuestProfileArtistBean bean = new GuestProfileArtistBean();
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONObject userJsonObject = jsonObject.getJSONObject("user");
            bean.setArtistName(userJsonObject.optString(ARTIST_NAME, EMPTY));
            bean.setCreativity(userJsonObject.optString(CREATIVITY, EMPTY));
            bean.setInstrument(userJsonObject.optString(INSTRUMENT, EMPTY));
            bean.setGenre(userJsonObject.optString(GENRE, EMPTY));
            bean.setOrdered(userJsonObject.optBoolean(ORDERED, false));
            bean.setPhoneNumber(userJsonObject.optString(PHONE_NUMBER, EMPTY));
            bean.setVkLink(userJsonObject.optString(VK, EMPTY));
            bean.setWappNumber(userJsonObject.optString(WAPP, EMPTY));
            bean.setAvatarURL(userJsonObject.optString(AVATAR, EMPTY));

            List<String> photoUrls = new ArrayList<>();
            JSONArray jsonArrayPhotos = userJsonObject.getJSONArray(PHOTOS);
            for (int i = 0; i < jsonArrayPhotos.length(); i++) {
                String photoURL = jsonArrayPhotos.optString(i, EMPTY);
                photoUrls.add(photoURL);
            }
            bean.setPhotosURL(photoUrls);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return bean;
    }


}
