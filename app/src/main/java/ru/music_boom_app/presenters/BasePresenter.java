package ru.music_boom_app.presenters;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;

import java.io.File;

/**
 * @author Markin Andrey on 23.11.2017.
 */
public abstract class BasePresenter<View extends MvpView> extends MvpPresenter<View> {

    public abstract void setResponse(String response, String requestId);

    public abstract void setFileResponse (File fileResponse, String requestId);

    public abstract void showFailDialog(int dialogErrorMessageId);

    public abstract void showProgressBar();

    public abstract void hideProgressBar();
}
