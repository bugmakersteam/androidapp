package ru.music_boom_app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.vk.sdk.VKSdk;

import javax.inject.Inject;

import ru.music_boom_app.dagger.components.AppComponent;
import ru.music_boom_app.dagger.components.DaggerAppComponent;
import ru.music_boom_app.dagger.modules.AuthorizationServicesModule;
import ru.music_boom_app.dagger.modules.CommonModule;
import ru.music_boom_app.dagger.modules.NetworkUtilsModule;
import ru.music_boom_app.dagger.modules.PermissionUtilsModule;
import ru.music_boom_app.utils.VkAuthorizationUtils;

/**
 * @author Markin Andrey on 06.12.2017.
 */
public class MusicBoomApplication extends Application {
    @Inject
    VkAuthorizationUtils mVkAuthorizationUtils;

    AppComponent mComponent;
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        VKSdk.initialize(getApplicationContext());
        mComponent = DaggerAppComponent
                .builder()
                .networkUtilsModule(new NetworkUtilsModule())
                .permissionUtilsModule(new PermissionUtilsModule())
                .authorizationServicesModule(new AuthorizationServicesModule())
                .commonModule(new CommonModule(getApplicationContext()))
                .build();
    }

    public static AppComponent getComponent(Context context){
        return ((MusicBoomApplication) context.getApplicationContext()).mComponent;
    }
}
