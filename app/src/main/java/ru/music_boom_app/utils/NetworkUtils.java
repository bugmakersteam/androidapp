package ru.music_boom_app.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * @author Markin Andrey on 06.12.2017.
 */
public class NetworkUtils {
    /**
     * Проверить подключение к интернету
     * @param context
     * @return true - подключен, false - неподключен
     */
    public boolean checkConnection(Context context)
    {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
