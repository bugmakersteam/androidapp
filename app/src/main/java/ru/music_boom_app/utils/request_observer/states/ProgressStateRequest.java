package ru.music_boom_app.utils.request_observer.states;

import ru.music_boom_app.presenters.BasePresenter;

/**
 * @author Markin Andrey on 23.11.2017.
 */
public class ProgressStateRequest implements StateRequest {
    BasePresenter mBasePresenter;

    public ProgressStateRequest(BasePresenter presenter) {
        mBasePresenter = presenter;
    }

    @Override
    public void shareData() {
        mBasePresenter.showProgressBar();
    }

    @Override
    public void shareFileData() {
        mBasePresenter.showProgressBar();
    }
}
