package ru.music_boom_app.utils.request_observer.states;

/**
 * @author Markin Andrey on 22.11.2017.
 *
 * Интерфейс описывающий методы объекта-состояния запроса
 */
public interface StateRequest {
    void shareData();
    void shareFileData();
}
