package ru.music_boom_app.utils.request_observer.states;

import android.util.Log;

import ru.music_boom_app.presenters.BasePresenter;

/**
 * @author Markin Andrey on 22.11.2017.
 */
public class FailStateRequest implements StateRequest {
    private BasePresenter mBasePresenter;
    private int mStringErrorMessage;

    public FailStateRequest(BasePresenter presenter, int stringErrorMessageId, String errorRequest) {
        mBasePresenter = presenter;
        mStringErrorMessage = stringErrorMessageId;
        Log.v("ERROR REQUEST", errorRequest);
    }

    @Override
    public void shareData() {
        mBasePresenter.hideProgressBar();
        mBasePresenter.showFailDialog(mStringErrorMessage);
    }

    @Override
    public void shareFileData() {
        mBasePresenter.hideProgressBar();
        mBasePresenter.showFailDialog(mStringErrorMessage);
    }

}
