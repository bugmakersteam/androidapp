package ru.music_boom_app.utils.request_observer.states;

import java.io.File;

import ru.music_boom_app.presenters.BasePresenter;

/**
 * @author Markin Andrey on 22.11.2017.
 */
public class SuccessStateRequest implements StateRequest {
    private String mResponse;
    private BasePresenter mBasePresenter;
    private String mRequestId;
    private File mFileResponse;

    public SuccessStateRequest(String response, BasePresenter presenter, String requestId) {
        mResponse = response;
        mBasePresenter = presenter;
        mRequestId = requestId;
        //Log.v("RESPONSE", response);
    }

    public SuccessStateRequest(File response, BasePresenter presenter, String requestId) {
        mFileResponse = response;
        mBasePresenter = presenter;
        mRequestId = requestId;
        //Log.v("RESPONSE", response.toString());
    }

    @Override
    public void shareData() {
        mBasePresenter.hideProgressBar();
        mBasePresenter.setResponse(mResponse, mRequestId);
    }

    @Override
    public void shareFileData() {
        mBasePresenter.hideProgressBar();
        mBasePresenter.setFileResponse(mFileResponse, mRequestId);
    }
}
