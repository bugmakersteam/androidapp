package ru.music_boom_app.utils;

import android.app.Activity;
import android.content.Context;

import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;

/**
 * @author Markin Andrey on 26.02.2018.
 */
public class VkAuthorizationUtils {
    private VKSdk mVKSdk;

    public VkAuthorizationUtils(Context context) {
      //  mVKSdk = VKSdk.initialize(context);
    }

    public void getLogin(Activity activity) {
        String[] scopes = {
                VKScope.EMAIL
        };
        VKSdk.login(activity, scopes);
    }
}
