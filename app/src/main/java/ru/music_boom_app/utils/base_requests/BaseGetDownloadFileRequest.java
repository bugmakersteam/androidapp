package ru.music_boom_app.utils.base_requests;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Observable;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.request_observer.states.FailStateRequest;
import ru.music_boom_app.utils.request_observer.states.ProgressStateRequest;
import ru.music_boom_app.utils.request_observer.states.StateRequest;
import ru.music_boom_app.utils.request_observer.states.SuccessStateRequest;

/**
 * @author Markin Andrey on 28.03.2018.
 */
public class BaseGetDownloadFileRequest extends Observable {
    private BasePresenter mPresenter;
    private int mStringErrorMessageId;
    private StateRequest mStateRequest;
    private Context mContext;
    private String mFilenameWithExtension;

    public BaseGetDownloadFileRequest(Context context, BasePresenter presenter, int stringErrorMessageId, String filenameWithExtension) {
        mPresenter = presenter;
        mStringErrorMessageId = stringErrorMessageId;
        mContext = context;
        mFilenameWithExtension = filenameWithExtension;
    }

    public StateRequest getStateRequest() {
        return mStateRequest;
    }

    public void downloadFile(String url, String requestId) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://mock.ru/")
                .client(client)
                .build();

        IGetDownloadFileRequest downloadFileRequest = retrofit.create(IGetDownloadFileRequest.class);
        mStateRequest = new ProgressStateRequest(mPresenter);
        setChanged();
        notifyObservers();

        Call<ResponseBody> call = downloadFileRequest.downloadFileWithUrl(url);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    File downloadedFile = decodeResponseToFile(response.body());
                    mStateRequest = new SuccessStateRequest(
                            downloadedFile,
                            mPresenter,
                            requestId
                    );
                    setChanged();
                    notifyObservers();
                } else {
                    mStateRequest = new FailStateRequest(mPresenter, mStringErrorMessageId, url);
                    setChanged();
                    notifyObservers();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mStateRequest = new FailStateRequest(mPresenter, mStringErrorMessageId, url);
                setChanged();
                notifyObservers();
            }
        });
    }

    //ToDo оптимизировать алгоритм в будущем
    private File decodeResponseToFile(ResponseBody responseBody) {
        try {
            File file = new File(mContext.getCacheDir() + File.separator + mFilenameWithExtension);
            InputStream inputStream = null;
            OutputStream outputStream = null;
            try {
                byte[] buffer = new byte[4096];
                inputStream = responseBody.byteStream();
                outputStream = new FileOutputStream(file);
                /**Читаем и записываем данные */
                while (true) {
                    int readByte = inputStream.read(buffer);
                    if (readByte == -1) {
                        break;
                    }
                    outputStream.write(buffer, 0, readByte);
                }
                outputStream.flush();
                return file;

            } catch (java.io.IOException e) {
                e.printStackTrace();
                return null;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return null;
        }
    }
}
