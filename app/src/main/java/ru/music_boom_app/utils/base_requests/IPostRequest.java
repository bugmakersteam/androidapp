package ru.music_boom_app.utils.base_requests;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;

/**
 * @author Markin Andrey on 30.11.2017.
 */
public interface IPostRequest {
    @Multipart
    @POST(".")
    Call<ResponseBody> basePostRequest (@PartMap Map<String, RequestBody> params, @PartMap Map<String, RequestBody> files);
}
