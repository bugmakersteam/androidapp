package ru.music_boom_app.utils.base_requests;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * @author Markin Andrey on 23.11.2017.
 */
public interface IGetRequest {
    @GET(".")
    Call<ResponseBody> baseGetRequest (@QueryMap Map<String, String> options);
}
