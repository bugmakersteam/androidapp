package ru.music_boom_app.utils.base_requests;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.request_observer.states.FailStateRequest;
import ru.music_boom_app.utils.request_observer.states.ProgressStateRequest;
import ru.music_boom_app.utils.request_observer.states.StateRequest;
import ru.music_boom_app.utils.request_observer.states.SuccessStateRequest;

/**
 * @author Markin Andrey on 23.11.2017.
 */
public abstract class BaseGetRequest extends Observable {
    private StateRequest mStateRequest;
    private BasePresenter mPresenter;
    private int mStringErrorMessage;
    private String mRequestId;

    public BaseGetRequest(BasePresenter presenter, int stringErrorMessage) {
        mPresenter = presenter;
        mStringErrorMessage = stringErrorMessage;
    }

    public StateRequest getStateRequest() {
        return mStateRequest;
    }

    public void createAndSendRequest(String url, Map<String, String> params, String requestId) {
        mRequestId = requestId;
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .build();
        if (params == null) {
            params = new HashMap<>();
        }
        IGetRequest request = retrofit.create(IGetRequest.class);
        mStateRequest = new ProgressStateRequest(mPresenter);
        setChanged();
        notifyObservers();
        createCustomRequest(retrofit, params);
    }

    protected void setSuccessState(Response<ResponseBody> response) {
        try {
            mStateRequest = new SuccessStateRequest(response.body().string(), mPresenter, mRequestId);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void setFailState() {
        mStateRequest = new FailStateRequest(
                mPresenter,
                mStringErrorMessage,
                "Ошибка запроса");
    }

    protected void setFailState(String value) {
        mStateRequest = new FailStateRequest(
                mPresenter,
                mStringErrorMessage,
                value);
    }

    protected abstract void createCustomRequest(Retrofit retrofit, Map<String, String> params);
}
