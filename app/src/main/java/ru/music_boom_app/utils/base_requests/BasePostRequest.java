package ru.music_boom_app.utils.base_requests;

import java.io.IOException;
import java.util.Observable;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.BufferedSink;
import retrofit2.Response;
import retrofit2.Retrofit;
import ru.music_boom_app.presenters.BasePresenter;
import ru.music_boom_app.utils.request_observer.states.FailStateRequest;
import ru.music_boom_app.utils.request_observer.states.ProgressStateRequest;
import ru.music_boom_app.utils.request_observer.states.StateRequest;
import ru.music_boom_app.utils.request_observer.states.SuccessStateRequest;

/**
 * @author Markin Andrey on 30.11.2017.
 */
public abstract class BasePostRequest extends Observable {
    private StateRequest mStateRequest;
    private BasePresenter mPresenter;
    private String mRequestId;
    private int mStringErrorMessage;

    public BasePostRequest(BasePresenter presenter, int stringErrorMessage) {
        mPresenter = presenter;
        mStringErrorMessage = stringErrorMessage;
    }

    public StateRequest getStateRequest() {
        return mStateRequest;
    }

    public void createAndSendRequest(String url, RequestBody params, String requestId) {
        mRequestId = requestId;
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .build();

        if (params == null) {
            params = new RequestBody() {
                @Nullable
                @Override
                public MediaType contentType() {
                    return null;
                }

                @Override
                public void writeTo(BufferedSink sink) throws IOException {

                }
            };
        }
        mStateRequest = new ProgressStateRequest(mPresenter);
        setChanged();
        notifyObservers();
        createCustomRequest(retrofit, params);
    }

    protected abstract void createCustomRequest(Retrofit retrofit, RequestBody params);

    protected void setSuccessState(Response<ResponseBody> response) {
        try {
            mStateRequest = new SuccessStateRequest(response.body().string(), mPresenter, mRequestId);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void setFailState() {
        mStateRequest = new FailStateRequest(
                mPresenter,
                mStringErrorMessage,
                "Ошибка запроса");
    }

    protected void setFailState(String value) {
        mStateRequest = new FailStateRequest(
                mPresenter,
                mStringErrorMessage,
                value);
    }
}
