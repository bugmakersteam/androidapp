package ru.music_boom_app.utils.base_requests;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * @author Markin Andrey on 28.03.2018.
 */
public interface IGetDownloadFileRequest {
    @GET
    Call<ResponseBody> downloadFileWithUrl(@Url String fileUrl);
}
