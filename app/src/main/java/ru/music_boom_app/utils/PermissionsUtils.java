package ru.music_boom_app.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * @author Markin Andrey on 07.12.2017.
 */
public class PermissionsUtils {

    public void requestPermission(Activity activity, String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(activity, permission)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(
                    activity,
                    new String[]{permission},
                    requestCode);
        }
    }

    public void requestPermissions(Activity activity, String[] permissions, int requestCode) {
        boolean hasPermission = true;

        for (int i = 0; i < permissions.length; i++) {
            if (ContextCompat.checkSelfPermission(activity, permissions[i])
                    != PackageManager.PERMISSION_GRANTED) {
                hasPermission = false;
                break;
            }
        }
        if (!hasPermission) {
            ActivityCompat.requestPermissions(
                    activity,
                    permissions,
                    requestCode);
        }
    }

    public boolean checkPermissions(Activity activity, String[] permissions) {
        for (int i = 0; i < permissions.length; i++) {
            String permission = permissions[i];
            boolean isPermission = checkPermission(activity, permission);
            if (!isPermission) {
                return false;
            }
        }
        return true;
    }

    public boolean checkPermissions(Context context, String[] permissions) {
        for (int i = 0; i < permissions.length; i++) {
            String permission = permissions[i];
            boolean isPermission = checkPermission(context, permission);
            if (!isPermission) {
                return false;
            }
        }
        return true;
    }

    public boolean checkPermission(Activity activity, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    public boolean checkPermission(Context context, String permission) {
        if (ContextCompat.checkSelfPermission(context, permission)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }
}
